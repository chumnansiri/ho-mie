<?php

include "utilities_settings.php";

cron_logs(__FILE__,__FUNCTION__,__LINE__,'renew membership start');


$today_date = date('Y-m-d H:i:s');

$sql = "SELECT 	member_id,
	   			mmt.membership_type_id,
	   			start_date,
	   			stop_date,
	   			mt.num_of_properties,
	   			mt.num_of_email_per_month,
	   			used_num_of_email_per_month,
	   			mt.membership_type_cost,
	   			membership_type_period
		FROM member_monthly_tokens mmt
		LEFT OUTER JOIN membership_types mt on mmt.membership_type_id = mt.membership_type_id
		WHERE date(stop_date) = date( now() )";

$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
	cron_logs(__FILE__,__FUNCTION__,__LINE__,'there are '.$result->num_rows.' expired tokens');
	
    while($row = $result->fetch_assoc()) {
    
    	$current_plan_sql = "SELECT membership_type_cost,membership_type_period,num_of_properties,num_of_email_per_month
							 FROM membership_types
							 WHERE membership_type_id =".$row['membership_type_id'];
    	
    	$current_plan_result = $conn->query( $current_plan_sql );
       	$current_plan = $current_plan_result->fetch_assoc();
    	    	
		if( $row["membership_type_cost"] == 0 )
		{
			// this is freemuim, so we continue the same plan
			
			$date = new DateTime(date('Y-m-d H:i:s'));
			$start_day = $date->format('j');
			$date->modify( "+".$current_plan['membership_type_period'] );
			$end_day = $date->format('j');
			
			if ($start_day != $end_day){
				$date->modify('last day of last month');
			}
			
			$next_month = $date->format('Y-m-d H:i:s');
			
						
			$insert ="INSERT INTO member_monthly_tokens
					  SET member_id 					= ".$row['member_id'].",
						  membership_type_id			= ".$row['membership_type_id'].",
						  start_date 					= now(),
						  stop_date 					= '".$next_month."',
						  num_of_properties       		= ".$current_plan['num_of_properties'].",
						  num_of_email_per_month  		= ".$current_plan['num_of_email_per_month'].",
						  used_num_of_email_per_month 	= 0,
						  sort_by                     	= 0,
						  created_date					= '".$today_date."',
						  last_modified               	= '".$today_date."'";
			
			if( $conn->query( $insert ) ){
				echo $conn->insert_id;
			}else{
				cron_logs(__FILE__,__FUNCTION__,__LINE__,$insert);
			}		
			
		}else{
			
			// we look for next plan
			$sql = "select next_downgrade_membership_types
					from members
					where member_id = ".$row['member_id'];
			
			$member_result = $conn->query($sql);
			
			if ($member_result->num_rows > 0) {
				
				$member = $member_result->fetch_assoc();
				
				if( !is_null( $member['next_downgrade_membership_types'] ) ){
					// there's future plan
					
					$future_plan_sql = "SELECT membership_type_cost,membership_type_period,num_of_properties,num_of_email_per_month
							 			FROM membership_types
							 			WHERE membership_type_id =".$member['next_downgrade_membership_types'];
					 
					$future_plan_result = $conn->query( $future_plan_sql );
					$future_plan        = $future_plan_result->fetch_assoc();
					
					if( $future_plan['membership_type_cost'] == 0){
						// if the future plan cost 0
						
						$date = new DateTime(date('Y-m-d H:i:s'));
						$start_day = $date->format('j');
						$date->modify( "+".$future_plan['membership_type_period'] );
						$end_day = $date->format('j');
							
						if ($start_day != $end_day){
							$date->modify('last day of last month');
						}
							
						$next_month = $date->format('Y-m-d H:i:s');
							
							
							
						$insert ="INSERT INTO member_monthly_tokens
					  		  	  SET 	member_id 					= ".$row['member_id'].",
						  				membership_type_id			= ".$member['next_downgrade_membership_types'].",
						 	 			start_date 					= now(),
						  				stop_date 					= '".$next_month."',
						  				num_of_properties       	= ".$future_plan['num_of_properties'].",
						  				num_of_email_per_month  	= ".$future_plan['num_of_email_per_month'].",
						  				used_num_of_email_per_month = 0,
						  				sort_by                     = 0,
						  				created_date				= '".$today_date."',
						  				last_modified               = '".$today_date."'";
						
						if( $conn->query( $insert ) ){
							echo $conn->insert_id;
						
							// future plan to null
							$update = "UPDATE members
								   	   SET next_downgrade_membership_types = null
								   	   WHERE member_id = ".$row['member_id'];
						
							$conn->query( $update );
						
						
						
						}else{
							cron_logs(__FILE__,__FUNCTION__,__LINE__,$insert);
						}
						
					}else{
						// if the future plan cost mooney
						
						$charge_success = true; //--> this's supposed to call function
						
						if( $charge_success ){
							
							$date = new DateTime(date('Y-m-d H:i:s'));
							$start_day = $date->format('j');
							$date->modify( "+".$future_plan['membership_type_period'] );
							$end_day = $date->format('j');
								
							if ($start_day != $end_day){
								$date->modify('last day of last month');
							}
								
							$next_month = $date->format('Y-m-d H:i:s');
								
								
								
							$insert ="INSERT INTO member_monthly_tokens
					  		  		  SET 	member_id 					= ".$row['member_id'].",
						  					membership_type_id			= ".$member['next_downgrade_membership_types'].",
						 	 				start_date 					= now(),
						  					stop_date 					= '".$next_month."',
						  					num_of_properties       	= ".$future_plan['num_of_properties'].",
						  					num_of_email_per_month  	= ".$future_plan['num_of_email_per_month'].",
						  					used_num_of_email_per_month = 0,
						  					sort_by                     = 0,
						  					created_date				= '".$today_date."',
						  					last_modified               = '".$today_date."'";
							
							if( $conn->query( $insert ) ){
								echo $conn->insert_id;
							
								// future plan to null
								$update = "UPDATE members
								   		   SET next_downgrade_membership_types = null
								   		   WHERE member_id = ".$row['member_id'];
							
								$conn->query( $update );
														
							
							}else{
								cron_logs(__FILE__,__FUNCTION__,__LINE__,$insert);
							}
							
						}else{			
							cron_logs(__FILE__,__FUNCTION__,__LINE__,'can not charge credit card');
						}
						
						
						
					}
					
					
					
			
				}else{
					// there's no future plan
					
					$date = new DateTime(date('Y-m-d H:i:s'));
					$start_day = $date->format('j');
					$date->modify( "+".$current_plan['membership_type_period'] );
					$end_day = $date->format('j');
					
					if ($start_day != $end_day){
						$date->modify('last day of last month');
					}
					
					$next_month = $date->format('Y-m-d H:i:s');
					
					$insert ="INSERT INTO member_monthly_tokens
					  		  SET 	member_id 					= ".$row['member_id'].",
						  			membership_type_id			= ".$row['membership_type_id'].",
						  			start_date 					= now(),
						  			stop_date 					= '".$next_month."',
						  			num_of_properties       	= ".$current_plan['num_of_properties'].",
						  			num_of_email_per_month  	= ".$current_plan['num_of_email_per_month'].",
						  			used_num_of_email_per_month = 0,
						  			sort_by                     = 0,
						  			created_date				= '".$today_date."',
						  			last_modified               = '".$today_date."'";
						
					if( $conn->query( $insert ) ){
						echo $conn->insert_id;
					}else{
						cron_logs(__FILE__,__FUNCTION__,__LINE__,$insert);
					}
					
				}
				
			}			
			
		}
    }
} 
cron_logs(__FILE__,__FUNCTION__,__LINE__,'renew membership stop');


?>
