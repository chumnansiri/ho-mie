<?php
namespace Index\Controller;


use Index\Model\Feedback;

use Index\Model\Memberbuyingcostitem;
use Index\Model\Membersellingcostitem;

use Index\Model\Memberexpenseitem;

use Index\Model\Memberincomeitem;

use Index\Model\Fullpagephoto;
use Index\Model\Propertyvideo;
use Index\Model\Member;



use Zend\View\View;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime;
use Zend\Mime\Part as MimePart;
use Zend\Mime\Message as MimeMessage;


//use Index\Model\Property;

use \TCPDF;
use \MPDF;
use Index\Model\Membermonthlytoken;
use Index\Model\Surway;


class MemberController extends AbstractActionController
{

	private $properties;


	public function __construct(){
	
		
	}

	public function onDispatch( \Zend\Mvc\MvcEvent $e )
	{
		
		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
	
		
		//////////////////////
		
			$trafficlog = $this->getServiceLocator()->get('Trafficlog');
			$trafficlog->log(
					array(	'remote_addr' 		=> $_SERVER['REMOTE_ADDR'],
							'http_user_agent'	=> $_SERVER['HTTP_USER_AGENT']
						 )
			);
			
			//  session checking
			
			$this->itemTable = $sm->get('Index\Model\ItemTable');

			$layout = $this->layout();
			$layout->setTemplate('layout/member');

			

		    $timeout = $this->itemTable->get_Item_value('session','timeout'); // In seconds
		    $this->layout()->timeout = $timeout;
		    
		    $json_model = $this->getmembercurrenttokenajaxAction();		    
		    $json_string =  $json_model->serialize();
		    $member_token = json_decode($json_string,true);
		   
		    
		    $json_model = $this->getmembercurrentpropertiesajaxAction();
		    $json_string = $json_model->serialize();
		    $num_of_current_properties = json_decode($json_string,true);
		    
		    
		    $this->layout()->sent_email 	= $member_token['used_num_of_email_per_month'];
		    $this->layout()->max_email  	= $member_token['num_of_email_per_month'];
		    
		    $this->layout()->used_properties 	= $num_of_current_properties['num_of_current_properties'];
		    $this->layout()->max_properties 	= $member_token['num_of_properties'];

		    $fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
		    
			
			
		
			
				if (
						   ( isset( $user_session->last_active ) && $user_session->last_active < (time()-$timeout) )
						|| ( isset( $user_session->fingerprint ) && $user_session->fingerprint != $fingerprint)   // protect hi-jack
						|| ( $user_session->isloggedin != true)
					)
				{
					//setcookie(session_name(), '', time()-3600, '/');  unset cookie if need
				
					
					
					unset($user_session);
					session_destroy();
					return $this->redirect()->toRoute('index');
				}
			

			$user_session->last_active = time();
			$user_session->fingerprint = $fingerprint;
			$this->layout()->fingerprint = $fingerprint;
 
			// end session checking

			$propertyTable = $sm->get('Index\Model\PropertyTable');
								
			$rowset    = $propertyTable->getMemberProperties( $user_session->id );
				
			$i=0;
			foreach ($rowset as $property) {
				$this->properties[$i]['property_name'] 		= $property->get_property_name();
				$this->properties[$i++]['property_id']   	= $property->get_property_id();
			}
			
		
			$this->layout()->properties =  $this->properties;
			

			return parent::onDispatch( $e );
	}


	public function propertyAction(){
		
		$user_session = new Container('user');
		
		 $this->properties;
      
		$view = new ViewModel(
				array(
						'fingerprint' => $user_session->fingerprint,
						'properties'  => $this->properties
				)
		);
		return $view;
	}

	public function unitsAction(){
		$user_session = new Container('user');

		$view = new ViewModel(
				array(
						'fingerprint' => $user_session->fingerprint,
				)
		);
		return $view;
	}
	
	public function photosAction(){
	
		$user_session = new Container('user');
		$property_id = $user_session->property_id;
		
		$up_id = uniqid();

		if( $property_id ){

			$sm = $this->getServiceLocator();
			$PropertyTable = $sm->get('Index\Model\PropertyTable');
			$property = $PropertyTable->getProperty($property_id);

			$FullpagephotoTable = $sm->get('Index\Model\FullpagephotoTable');

			$request = $this->getRequest();

			if (isset($_POST['cover_photo'])) {  // for cover phpto
								
					
					$temp = explode(".", $_FILES["cover_photo"]["name"]);
					$extension = end($temp);
					$file_name = 'cover_photo_id_'.$property_id.'.'.$extension;
					$file_path = "/img/members/member_id_".$user_session->id."/".$file_name;
				
					// to remove old file incase different file extension
					system('rm -f '.getcwd().'/public/img/members/member_id_'.$user_session->id.'/cover_photo_id_'.$property_id.'.*');

					$property->set_property_cover_photo_path($file_path);
					move_uploaded_file( $_FILES["cover_photo"]["tmp_name"],getcwd()."/public".$file_path);
					$PropertyTable->saveProperty( $property );
			
				
			}else if( isset($_POST['full_page_photo']) ){  // for full page phpto
				
					
				
					$temp = explode(".", $_FILES["full_page_photo"]["name"]);
					$extension = end($temp);


					$Fullpagephoto = new Fullpagephoto();
					$Fullpagephoto->set_property_id($property_id);
					$Fullpagephoto->set_fullpage_photo_path('');
					$fullpage_photo_id = $FullpagephotoTable->saveFullpagephoto($Fullpagephoto);


					$file_name = "fullpage_photo_id_".$fullpage_photo_id.'.'.$extension;
					$fullpage_photo_path = "/img/members/member_id_".$user_session->id."/fullpage_photos/".$file_name;


					move_uploaded_file( $_FILES["full_page_photo"]["tmp_name"],getcwd()."/public".$fullpage_photo_path);

					$Fullpagephoto = $FullpagephotoTable->getFullpagephoto($fullpage_photo_id);
					$Fullpagephoto->set_fullpage_photo_path($fullpage_photo_path);
					$FullpagephotoTable->saveFullpagephoto($Fullpagephoto);



			

			}
			
			$view = new ViewModel(
									array(
											'user_id' => $user_session->id,
											'cover_photo_path' => $property->get_property_cover_photo_path(),
											'fingerprint' => $user_session->fingerprint,
											
									)
								 );
			return $view;
			
		}
	}

	public function uploadprogressAction()
	{
	
	
	

// 		return new JsonModel(
// 				array('total' =>  3 )
// 		);
		session_start();
		$id = $this->params()->fromQuery('id', null);
		$progress = new \Zend\ProgressBar\Upload\SessionProgress();
		return new \Zend\View\Model\JsonModel( $progress->getProgress($id) );
/*	
		try {
		session_start();
		$id = $this->params()->fromQuery('id', null);
		//$progress = new \Zend\ProgressBar\Upload\SessionProgress();
		//return new JsonModel( $progress->getProgress($id) );
		} catch (Exception $e) {
		return new JsonModel(
				array('total' =>  $e->getMessage() )
		);
			
		}
		*/
	
	
	}
	
	
	public function videosAction(){

		$user_session = new Container('user');
		$property_id = $user_session->property_id;

		if( $property_id ){


			$sm = $this->getServiceLocator();
			$PropertyTable = $sm->get('Index\Model\PropertyTable');
			$property = $PropertyTable->getProperty($property_id);

			$PropertyvideoTable = $sm->get('Index\Model\PropertyvideoTable');

			if( isset($_POST['video']) ){  // for video
				if ($_FILES["video"]["error"] > 0){
					//echo "Error: " . $_FILES["video"]["error"] . "<br>";
					
				}else{
					$temp = explode(".", $_FILES["video"]["name"]);
					$extension = end($temp);


					$Propertyvideo = new Propertyvideo();
					$Propertyvideo->set_property_id($property_id);
					$Propertyvideo->set_video_path('');
					$property_video_id = $PropertyvideoTable->savePropertyvideo($Propertyvideo);


					$file_name = "property_video_id_".$property_video_id.'.'.$extension;
					$video_path = "/videos/members/member_id_".$user_session->id."/".$file_name;

					move_uploaded_file( $_FILES["video"]["tmp_name"],getcwd()."/public".$video_path);

					$Propertyvideo = $PropertyvideoTable->getPropertyvideo($property_video_id);
					$Propertyvideo->set_video_path($video_path);
					$PropertyvideoTable->savePropertyvideo($Propertyvideo);

				}


			}

			$view = new ViewModel(
					array(
							'user_id' => $user_session->id,
							'fingerprint' => $user_session->fingerprint,
					)
			);
			return $view;
		}
	}

	public function amortizationAction(){
		$user_session = new Container('user');

		$view = new ViewModel(
				array(
						'username'    => $user_session->email,
						'fingerprint' => $user_session->fingerprint,
				)
		);

		return $view;
	}

	public function reportsAction(){

		$user_session = new Container('user');
		
		$view = new ViewModel(
								array(
									'username' => $user_session->email,
									'fingerprint' => $user_session->fingerprint,
								)
							 );

	    return $view;

	}

	public function compareAction(){

		$user_session = new Container('user');

		$view = new ViewModel(
				array(
						'username' => $user_session->email,
						'fingerprint' => $user_session->fingerprint,
				)
		);

		return $view;

	}

	public function emailpropertypdfcompareAction(){
		$user_session = new Container('user');
		$first_page 		= $_POST['first_page'];
		$second_page 		= $_POST['second_page'];

		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
		$property_id = $user_session->property_id;
		$PropertyTable = $sm->get('Index\Model\PropertyTable');
		$Property = $PropertyTable->getProperty($property_id);
		$property_name = $Property->get_property_name();

		ob_end_clean();

		$mpdf=new mPDF();

		$mpdf->SetHeader('www.ho-mie.com|Comparision Report|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->WriteHTML($first_page);

		$mpdf->SetHeader('www.ho-mie.com|Comparision Report|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($second_page);

		$mpdf->Output(getcwd().'/public/pdf/property'.$property_id.'.pdf', 'F');

		$content  = new MimeMessage();
		$htmlPart = new MimePart("<html><body><p>This is your pdf compare report</p></body></html>");
		$htmlPart->type = 'text/html';
		$textPart = new MimePart("");
		$textPart->type = 'text/plain';
		$content->setParts(array($textPart, $htmlPart));

		$contentPart = new MimePart($content->generateMessage());
		$contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';


		$attachment = new MimePart(fopen(getcwd().'/public/pdf/property'.$property_id.'.pdf', 'r'));
		$attachment->type = 'application/pdf';
		$attachment->filename ='Compare_report.pdf';
		$attachment->encoding    = Mime\Mime::ENCODING_BASE64;
		$attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;

		$body = new MimeMessage();
		$body->setParts(array($contentPart, $attachment));

		$message = new Message();
		$message->addTo($user_session->email)
		->addFrom('non@ho-mie.com')
		->setSubject('Compare report')
		->setBody($body);


		$config = $sm->get('config');
		$smtp   = $config['smtp'];

		$transport = new SmtpTransport();

		$options   = new SmtpOptions(array(
						
						'name'              => $smtp['name'],
						'host'              => $smtp['name'],
						'connection_class'  => 'login',
						'connection_config' => array(
								'ssl'       => $smtp['ssl'],
								'username'  => $smtp['username'],
								'password'  => $smtp['password'],
								
						),
						'port' 				=> $smtp['port'],
				));
	


		$transport->setOptions($options);
		$transport->send($message);
		system('rm '.getcwd().'/public/pdf/property'.$property_id.'.pdf');

		return $this->response;
	}

	public function emailpropertypdfreportAction(){
		

		$user_session = new Container('user');
		$cover_page 		= $_POST['cover_page'];
		$overview_page 		= $_POST['overview_page'];
		$projection1_page 	= $_POST['projection1_page'];
		$projection2_page   = $_POST['projection2_page'];
		$projection3_page   = $_POST['projection3_page'];
		$projection4_page   = $_POST['projection4_page'];
		$projection5_page   = $_POST['projection5_page'];
		$rent_page          = $_POST['rent_page'];
		$closingcost_page   = $_POST['closingcost_page'];


		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
		$property_id = $user_session->property_id;
		$PropertyTable = $sm->get('Index\Model\PropertyTable');
		$Property = $PropertyTable->getProperty($property_id);
		$property_name = $Property->get_property_name();

		ob_end_clean();
		$trafficlog = $this->getServiceLocator()->get('Trafficlog');
		
		
		
		$mpdf=new mPDF();

		
		
		$mpdf->SetHeader('www.ho-mie.com|Property Report|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->WriteHTML($cover_page);

		$mpdf->SetHeader('www.ho-mie.com|Overview|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($overview_page);

		$mpdf->SetHeader('www.ho-mie.com|Buy and Hold Projection year 1-6|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($projection1_page);

		$mpdf->SetHeader('www.ho-mie.com|Buy and Hold Projection year 7-12|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($projection2_page);

		$mpdf->SetHeader('www.ho-mie.com|Buy and Hold Projection year 13-18|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($projection3_page);

		$mpdf->SetHeader('www.ho-mie.com|Buy and Hold Projection year 19-24|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($projection4_page);

		$mpdf->SetHeader('www.ho-mie.com|Buy and Hold Projection year 25-30|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($projection5_page);

		$mpdf->SetHeader('www.ho-mie.com|Rent summary|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($rent_page);

		$mpdf->SetHeader('www.ho-mie.com|Itemized Closing Costs|{PAGENO}');
		$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
		$mpdf->AddPage();
		$mpdf->WriteHTML($closingcost_page);

		foreach( $_POST['fullpage_photos'] as $fullpage_photo){


			$mpdf->SetHeader('www.ho-mie.com|Full Page Photo|{PAGENO}');
			$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
			$mpdf->AddPage();
			$mpdf->WriteHTML($fullpage_photo);

		}
	
		
		
		$mpdf->Output(getcwd().'/public/pdf/property'.$property_id.'.pdf', 'F');
		
		

		$content  = new MimeMessage();
        $htmlPart = new MimePart("<html><body><p>This is your pdf report</p></body></html>");
        $htmlPart->type = 'text/html';
        $textPart = new MimePart("");
        $textPart->type = 'text/plain';
        $content->setParts(array($textPart, $htmlPart));

        $contentPart = new MimePart($content->generateMessage());
        $contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';


        $attachment = new MimePart(fopen(getcwd().'/public/pdf/property'.$property_id.'.pdf', 'r'));
        $attachment->type = 'application/pdf';
        $attachment->filename =$property_name.'_analysis_report.pdf';
        $attachment->encoding    = Mime\Mime::ENCODING_BASE64;
        $attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;

        $body = new MimeMessage();
        $body->setParts(array($contentPart, $attachment));

		$message = new Message();
		$message->addTo($user_session->email)
		->addFrom('non@ho-mie.com')
		->setSubject($property_name.' analysis report')
		->setBody($body);
		
		

		$config = $sm->get('config');
		$smtp   = $config['smtp'];

		$transport = new SmtpTransport();

		$options   = new SmtpOptions(array(
						
						'name'              => $smtp['name'],
						'host'              => $smtp['name'],
						'connection_class'  => 'login',
						'connection_config' => array(
								'ssl'       => $smtp['ssl'],
								'username'  => $smtp['username'],
								'password'  => $smtp['password'],
								
						),
						'port' 				=> $smtp['port'],
				));



	    $transport->setOptions($options);
		$transport->send($message);
		system('rm '.getcwd().'/public/pdf/property'.$property_id.'.pdf');

		return $this->response;
	}

	public function emailmortgagereportAction(){

		$user_session 		= new Container('user');
		$pages		= $_POST['pages'];

		
		
		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
		$property_id = $user_session->property_id;
		$PropertyTable = $sm->get('Index\Model\PropertyTable');
		$Property = $PropertyTable->getProperty($property_id);
		$property_name = $Property->get_property_name();

		ob_end_clean();

		$mpdf=new mPDF();


		$i = 0;
		foreach( $_POST['pages'] as $page){


			if($i == 0){
				$mpdf->SetHeader('www.ho-mie.com|Amortization|{PAGENO}');
				$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
				$mpdf->WriteHTML($page);
			}else{
				$mpdf->SetHeader('www.ho-mie.com|Amortization|{PAGENO}');
				$mpdf->SetFooter('www.ho-mie.com|'.$user_session->email.'|{PAGENO}');
				$mpdf->AddPage();
				$mpdf->WriteHTML($page);
			}



			$i++;
		}

		
		
		$mpdf->Output(getcwd().'/public/pdf/property'.$property_id.'.pdf', 'F');

		$content  = new MimeMessage();
		$htmlPart = new MimePart("<html><body><p>This is your pdf amortization report</p></body></html>");
		$htmlPart->type = 'text/html';
		$textPart = new MimePart("");
		$textPart->type = 'text/plain';
		$content->setParts(array($textPart, $htmlPart));

		$contentPart = new MimePart($content->generateMessage());
		$contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';


		$attachment = new MimePart(fopen(getcwd().'/public/pdf/property'.$property_id.'.pdf', 'r'));
		$attachment->type = 'application/pdf';
		$attachment->filename =$property_name.'_amortization_report.pdf';
		$attachment->encoding    = Mime\Mime::ENCODING_BASE64;
		$attachment->disposition = Mime\Mime::DISPOSITION_ATTACHMENT;

		$body = new MimeMessage();
		$body->setParts(array($contentPart, $attachment));

		$message = new Message();
		$message->addTo($user_session->email)
		->addFrom('Admin@ho-mie.com')
		->setSubject($property_name.' amortization report')
		->setBody($body);


		$config = $sm->get('config');
		$smtp   = $config['smtp'];

		$transport = new SmtpTransport();
		
	

		$options   = new SmtpOptions(array(
						
						'name'              => $smtp['name'],
						'host'              => $smtp['name'],
						'connection_class'  => 'login',
						'connection_config' => array(
								'ssl'       => $smtp['ssl'],
								'username'  => $smtp['username'],
								'password'  => $smtp['password'],
								
						),
						'port' 				=> $smtp['port'],
				));

		$transport->setOptions($options);
		$transport->send($message);
		system('rm '.getcwd().'/public/pdf/property'.$property_id.'.pdf');


		return $this->response;

	}

	public function accountAction(){
		$user_session = new Container('user');

		$view = new ViewModel(
				array(
						'fingerprint' => $user_session->fingerprint,
				)
		);

		return $view;
	}
	
	public function updatedefaultmlsajaxAction(){
	
		$mls = $_POST['mls'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
				
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_mls( $mls );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}

	public function updatedefaultstyleajaxAction(){
	
		$style = $_POST['style'];
		$user_session = new Container('user');
		
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_style( $style );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultsquarefeetajaxAction(){
	
		$squarefeet = $_POST['squarefeet'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_squarefeet( $squarefeet );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultlotsizeajaxAction(){
	
		$lotsize = $_POST['lotsize'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_lotsize( $lotsize );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultyearbuiltajaxAction(){
	
		$yearbuilt = $_POST['yearbuilt'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_yearbuilt( $yearbuilt );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultlastremodelajaxAction(){
	
		$lastremodel = $_POST['lastremodel'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_lastremodel( $lastremodel );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultparkingajaxAction(){
	
		$parking = $_POST['parking'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_parking( $parking );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultlistingpriceajaxAction(){
	
		$listingprice = $_POST['listingprice'];
		$user_session = new Container('user');
	
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_listingprice( $listingprice );
	
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultpurchasepriceajaxAction(){
		
		$purchaseprice = $_POST['purchaseprice'];
		$user_session = new Container('user');
		
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_purchaseprice( $purchaseprice );
		$member->set_property_default_purchaseprice( $purchaseprice );
		$this->memberTable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultinitimproveajaxAction(){
		
		$initimprove = $_POST['initimprove'];
		$user_session = new Container('user');
		
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_initimprove( $initimprove );
		$member->set_property_default_initimprove( $initimprove );
		$this->memberTable->saveMember($member);
		return $this->response;
		
	}
	
	public function updatedefaultfirstmortgateajaxAction(){
		
		$loanamount 		= $_POST['percent'];
		$loantype   		= $_POST['loan_type'];
		$interesterate 		= $_POST['interested_rate'];
		$payment_frequency  = $_POST['payment_mode'];
		$term               = $_POST['loan_term'];
		$interestonly_term  = $_POST['int_only_term'];
		
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
		
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
				
		$member->set_property_default_first_loanamount( $loanamount );
		$member->set_property_default_first_loantype( $loantype );
		$member->set_property_default_first_interestrate( $interesterate );
		$member->set_property_default_first_payment_frequency( $payment_frequency );
		$member->set_property_default_first_term( $term );
		$member->set_property_default_first_interestonly_term( $interestonly_term );
		
		$membertable->saveMember($member);
		
		return $this->response;
		
	}
	
	public function updatedefaultsecondmortgateajaxAction(){
	
		$loanamount 		= $_POST['percent'];
		$loantype   		= $_POST['loan_type'];
		$interesterate 		= $_POST['interested_rate'];
		$payment_frequency  = $_POST['payment_mode'];
		$term               = $_POST['loan_term'];
		$interestonly_term  = $_POST['int_only_term'];
	
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
	
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
	
		$member->set_property_default_second_loanamount( $loanamount );
		$member->set_property_default_second_loantype( $loantype );
		$member->set_property_default_second_interestrate( $interesterate );
		$member->set_property_default_second_payment_frequency( $payment_frequency );
		$member->set_property_default_second_term( $term );
		$member->set_property_default_second_interestonly_term( $interestonly_term );
	
		$membertable->saveMember($member);
	
		return $this->response;
	
	}
	
	public function updaterentmodeajaxAction(){
		$property_default_rent_mode 		= $_POST['property_default_rent_mode'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
	
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_rent_mode( $property_default_rent_mode );
		$membertable->saveMember($member);
		return $this->response;
	}
	
	public function updatedefaultfixamountajaxAction(){
		
		$fix_amount 		= $_POST['fix_amount'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
		
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_singlevalue_rent( $fix_amount );
		$membertable->saveMember( $member );
		return $this->response;
		
	}
	
	public function updatedefaultsinglevaluefreqajaxAction(){
		
		$singlevalue_freq 		= $_POST['singlevalue_freq'];
		$user_session 			= new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
		
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_singlevalue_rent_frequency($singlevalue_freq);
		$membertable->saveMember( $member );
		return $this->response;
		
	}
	
	public function updatedefaultvacancyrateajaxAction(){
		
		$vacancy_rate 		= $_POST['vacancyrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
	
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_vacancy_rate($vacancy_rate);
		$membertable->saveMember( $member );
		return $this->response;
	
	}
	
	public function updatedefaultappreciationrateajaxAction(){
		$appreciationrate 		= $_POST['appreciationrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
	
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_appreciation_rate($appreciationrate);
		$membertable->saveMember( $member );
		return $this->response;
	}
	
	public function updatedefaultincomeinflationrateajaxAction(){
		
		$incomeinflationrate 		= $_POST['incomeinflationrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
		
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_incomeinflation_rate( $incomeinflationrate );
		$membertable->saveMember( $member );
		return $this->response;
		
	}
	
	public function updatedefaultexpenseinflationrateajaxAction(){
		
		$expenseinflationrate 		= $_POST['expenseinflationrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
		
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_expenseinflation_rate( $expenseinflationrate );
	
		$membertable->saveMember( $member );
		return $this->response;
		
	}
	
	public function updatedefaultlvtforrefinanceajaxAction(){
		
		$lvtforrefinance 		= $_POST['lvtforrefinance'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
		
		$member = $membertable->getMember( $user_session->id );
		$member = new Member( $member );
		$member->set_property_default_lvt_for_refinance($lvtforrefinance);
		
		$membertable->saveMember( $member );
		return $this->response;
	}
	
	public function updatedefaultcompoundingperiodajaxAction(){
		
		$property_default_compounding_period 		= $_POST['property_default_compounding_period'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$membertable = $sm->get('Index\Model\MemberTable');
	
		$member = $membertable->getMember($user_session->id);
		$member = new Member( $member );
		$member->set_property_default_compounding_period($property_default_compounding_period);
		
		$membertable->saveMember( $member );
		return $this->response;
	
	}
	
	public function getmembersettingajaxAction(){

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$MemberTable = $sm->get('Index\Model\MemberTable');

		$member = $MemberTable->getMember($user_session->id);

		$member = new Member($member);

		$incomeitems = $member->get_income_items();

		$incomeitemarray= array();
		foreach( $incomeitems as $incomeitem){


			$incomeitemarray[] = array(
										'income_id' 	=>   $incomeitem->get_income_id(),
										'income_name'   =>   $incomeitem->get_income_name()
									  );

		}

		$expenseitems = $member->get_expense_items();

		$expenseitemarray= array();
		foreach( $expenseitems as $expenseitem){

			$expenseitemarray[] = array(
										'expense_id'    =>  $expenseitem->get_expense_id(),
										'expense_name'  =>  $expenseitem->get_expense_name()
									   );

		}

		$buyingcostitems = $member->get_buyingcost_items();

		$buyingcostitemarray= array();
		foreach( $buyingcostitems as $buyingcostitem){
			$buyingcostitemarray[] = array(
											'buyingcost_id'  => $buyingcostitem->get_buyingcost_id(),
											'buyingcost_name'=> $buyingcostitem->get_buyingcost_name()
										  );
		}

		$sellingcostitems = $member->get_sellingcost_items();

		$sellingcostitemarray= array();
		foreach( $sellingcostitems as $sellingcostitem){
			$sellingcostitemarray[] = array(
												'sellingcost_id'  => $sellingcostitem->get_sellingcost_id(),
												'sellingcost_name'=> $sellingcostitem->get_sellingcost_name()
										   );
		}

		
		$default_value_items_array = array(
												'property_default_mls'							=> $member->get_property_default_mls(),
												'property_default_style'						=> $member->get_property_default_style(),
												'property_default_squarefeet'					=> $member->get_property_default_squarefeet(),
												'property_default_lotsize'						=> $member->get_property_default_lotsize(),
												'property_default_yearbuilt'					=> $member->get_property_default_yearbuilt(),
												'property_default_lastremodel'					=> $member->get_property_default_lastremodel(),
												'property_default_parking'						=> $member->get_property_default_parking(),
				
												'property_default_listingprice' 				=> $member->get_property_default_listingprice(),
												'property_default_initimprove'					=> $member->get_property_default_initimprove(),
												'property_default_purchaseprice'				=> $member->get_property_default_purchaseprice(),
												'property_default_first_loanamount'				=> $member->get_property_default_firstmortgage_amount(),
												
												'property_default_second_loanamount'			=> $member->get_property_default_secondmortgage_amount(),
												'property_default_downpayment'					=> $member->get_default_downpayment(),
												'property_default_buyingcost'					=> $member->get_property_default_buyingcost(),
												'property_default_initialcost'					=> $member->get_property_default_initialcost(),
												'property_default_init_cash_invested'           => $member->get_property_default_initialcash_invested(),
												'property_default_vacancy_rate'					=> $member->get_property_default_vacancy_rate(),
												'property_default_rent_mode'					=> $member->get_property_default_rent_mode(),
												'property_default_singlevalue_rent'				=> $member->get_property_default_singlevalue_rent(),
															
												'property_default_singlevalue_rent_frequency'	=> $member->get_property_default_singlevalue_rent_frequency(),
												'property_default_appreciation_rate'			=> $member->get_property_default_appreciation_rate(),
												'property_default_incomeinflation_rate'			=> $member->get_property_default_incomeinflation_rate(),
												'property_default_expenseinflation_rate'		=> $member->get_property_default_expenseinflation_rate(),
												'property_default_sellingcost'					=> $member->get_property_default_sellingcost(),
												'property_default_lvt_for_refinance'			=> $member->get_property_default_lvt_for_refinance(),
												'property_default_compounding_period'			=> $member->get_property_default_compounding_period()
											);
		
		

		$JsonObj = new JsonModel(array(
										'incomeitemarray' 				=>$incomeitemarray,
										'expenseitemarray'  			=>$expenseitemarray,
										'buyingcostitemarray'			=>$buyingcostitemarray,
										'sellingcostitemarray'  		=>$sellingcostitemarray,
										'default_value_items_array'		=>$default_value_items_array
									  )
								);

		return $JsonObj;

	}

	
	
	public function sendfeedbackajaxAction(){

		$user_session = new Container('user');

		$message = $_POST['message'];
		$sm = $this->getServiceLocator();
		$feedbackTable = $sm->get('Index\Model\FeedbackTable');

		$feedback = new Feedback();

		$feedback->set_member_id($user_session->id);
		$feedback->set_message($message);

		$feedbackTable->saveFeedback($feedback);

		$JsonObj = new JsonModel(array(

				'message' 				 	=> $message,

		));



		return $JsonObj;
	}

	public function updatepasswordajaxAction(){

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$MemberTable = $sm->get('Index\Model\MemberTable');

        $member = $MemberTable->getMember($user_session->id);

        $member = new Member($member);

		$current_password 		=	$_POST['current_password'];
		$new_password 			=   $_POST['new_password'];
		$retype_new_password    =   $_POST['retype_new_password'];

		$md5_password = md5( $current_password );

		$status  = 0;
		$message = '';

		if( $member->get_password() == $md5_password )
		{

			if( $new_password == '' ){

				$message = 'You cannot use a blank password';

			}else if( strlen($new_password) < 8 ){

				$message = 'New password is too short';

			}else if( $new_password == $current_password){

				$message = 'New password must be different from old password';

			}else if( $new_password != $retype_new_password){

				$message = 'Passwords do not match';

			}else if( $new_password == $retype_new_password ){

				$member->set_password( md5($new_password) );
				$MemberTable->saveMember( $member );

				$status = 1;
				$message = 'The password is updated';

			}


		}else if( $member->get_password() != $md5_password ){
			$message = 'The current password is incorrect';
		}else{

		}



		$JsonObj = new JsonModel(array(

				'status' 				 	=> $status,
				'message' 					=> $message

		));



		return $JsonObj;

	}

	public function addincomeitemajaxAction(){

		$user_session = new Container('user');
		$member_income_item = new Memberincomeitem();
		$income_item_name = $_POST['income_item_name'];
		$member_income_item->set_income_name($income_item_name);
		$member_income_item->set_member_id($user_session->id);
		$sm = $this->getServiceLocator();
		$member_income_itemTable = $sm->get('Index\Model\Member_income_itemTable');
		$income_id = $member_income_itemTable->saveMemberincomeitem($member_income_item);
		$JsonObj = new JsonModel(array(
				'income_id' 				 	=> $income_id,
		));
		return $JsonObj;
	}

	public function deleteincomeitemajaxAction(){

		$income_id = $_POST['income_id'];
		$sm = $this->getServiceLocator();
		
		$member_income_itemTable = $sm->get('Index\Model\Member_income_itemTable');
		
		$property_member_income_itemTable = $sm->get('Index\Model\Property_member_income_itemTable');
		
		$isUsed = $property_member_income_itemTable->isUsed($income_id);
		
		if( $isUsed ){
			
			$JsonObj = new JsonModel(array(
				'error' 				 	=> 1,
				'message'					=> 'This Income Item is being used, can not be deleted'
			));
		
		}else{
			
			$member_income_itemTable->deleteMemberincomeitem($income_id);
			$JsonObj = new JsonModel(array(
				'error' 				 	=> 0,
			));
		}
		
		return $JsonObj;

		
	}

	public function addexpenseitemajaxAction(){
		$user_session = new Container('user');
		$member_expense_item = new Memberexpenseitem();
		$expense_item_name = $_POST['expense_item_name'];
		$member_expense_item->set_expense_name($expense_item_name);
		$member_expense_item->set_member_id($user_session->id);
		$sm = $this->getServiceLocator();
		$member_expense_itemTable = $sm->get('Index\Model\Member_expense_itemTable');
		$expense_id = $member_expense_itemTable->saveMemberexpenseitem($member_expense_item);
		$JsonObj = new JsonModel(array(
				'expense_id' 				 	=> $expense_id,
		));
		return $JsonObj;
	}

	public function deleteexpenseitemajaxAction(){
		$expense_id = $_POST['expense_id'];
		$sm = $this->getServiceLocator();
		
		$property_member_expense_itemTable = $sm->get('Index\Model\Property_member_expense_itemTable');
		
		$member_expense_itemTable = $sm->get('Index\Model\Member_expense_itemTable');
		
		$isUsed = $property_member_expense_itemTable->isUsed( $expense_id );
		
		if( $isUsed ){
			
			$JsonObj = new JsonModel(array(
				'error' 				 	=> 1,
				'message'					=> 'This Expense Item is being used, can not be deleted'
			));
		
		}else{
			
			$member_expense_itemTable->deleteMemberexpenseitem($expense_id);
			$JsonObj = new JsonModel(array(
				'error' 				 	=> 0,
			));
		}
		
		return $JsonObj;
		
	}

	public function addbuyingcostitemajaxAction(){
		$user_session = new Container('user');
		$member_buyingcost_item = new Memberbuyingcostitem();
		$buyingcost_item_name = $_POST['buyingcost_item_name'];
		$member_buyingcost_item->set_buyingcost_name($buyingcost_item_name);
		$member_buyingcost_item->set_member_id($user_session->id);
		$sm = $this->getServiceLocator();
		$member_buyingcost_itemTable = $sm->get('Index\Model\Member_buyingcost_itemTable');
		$buyingcost_id = $member_buyingcost_itemTable->saveMemberbuyingcostitem($member_buyingcost_item);
		$JsonObj = new JsonModel(array(
				'buyingcost_id' 				 	=> $buyingcost_id,
		));
		return $JsonObj;
	}

	public function deletebuyingcostitemajaxAction(){
		$buyingcost_id = $_POST['buyingcost_id'];
		$sm = $this->getServiceLocator();
		
		$property_buyingcost_itemTable = $sm->get('Index\Model\Property_buyingcost_itemTable');
		
		$member_buyingcost_itemTable = $sm->get('Index\Model\Member_buyingcost_itemTable');
		
		$member_default_buyingcost_itemTable = $sm->get('Index\Model\Member_default_buyingcost_itemTable');

		$isUsed = $property_buyingcost_itemTable->isUsed( $buyingcost_id );
		
		if( $isUsed ){
				
			$JsonObj = new JsonModel(array(
					'error' 				 	=> 1,
					'message'					=> 'This Buyingcost Item is being used, can not be deleted'
			));
		
		}else{
				
			$member_buyingcost_itemTable->deleteMemberbuyingcostitem($buyingcost_id);
			$member_default_buyingcost_itemTable->deleteMember_default_buyingcost_item_by_buyingcost_id( $buyingcost_id );
			$JsonObj = new JsonModel(array(
					'error' 				 	=> 0,
			));
		}
		
		return $JsonObj;
		
		

		return 0;
	}

	public function addsellingcostitemajaxAction(){
		$user_session = new Container('user');
		$member_sellingcost_item = new Membersellingcostitem();
		$sellingcost_item_name = $_POST['sellingcost_item_name'];
		$member_sellingcost_item->set_sellingcost_name($sellingcost_item_name);
		$member_sellingcost_item->set_member_id($user_session->id);
		$sm = $this->getServiceLocator();
		$member_sellingcost_itemTable = $sm->get('Index\Model\Member_sellingcost_itemTable');
		$sellingcost_id = $member_sellingcost_itemTable->saveMembersellingcostitem($member_sellingcost_item);
		$JsonObj = new JsonModel(array(
				'sellingcost_id' 				 	=> $sellingcost_id,
		));
		return $JsonObj;
	}

	public function deletesellingcostitemajaxAction(){
		$sellingcost_id = $_POST['sellingcost_id'];
		$sm = $this->getServiceLocator();
		
		$property_sellingcost_itemTable = $sm->get('Index\Model\Property_sellingcost_itemTable');
		
		$member_sellingcost_itemTable = $sm->get('Index\Model\Member_sellingcost_itemTable');
		
		$member_default_sellingcost_itemTable = $sm->get('Index\Model\Member_default_sellingcost_itemTable');
		
		$isUsed = $property_sellingcost_itemTable->isUsed( $sellingcost_id );
		
		if( $isUsed ){
		
			$JsonObj = new JsonModel(array(
					'error' 				 	=> 1,
					'message'					=> 'This Buyingcost Item is being used, can not be deleted'
			));
		
		}else{
		
			$member_sellingcost_itemTable->deleteMembersellingcostitem( $sellingcost_id );
			$member_default_sellingcost_itemTable->deleteMember_default_sellingcost_item_by_sellingcost_id( $sellingcost_id );
			
			$JsonObj = new JsonModel(array(
					'error' 				 	=> 0,
			));
		}
		
		return $JsonObj;
	}


	public function logoutAction(){

		unset($user_session);
		session_destroy();
		return $this->redirect()->toRoute('index');
	}

	public function updatesessionajaxAction(){
		
		$property_id = $_POST['property_id'];
		$tab         = $_POST['tab'];
		$user_session = new Container('user');
		$user_session->tab         = $tab;
		$user_session->property_id = $property_id;
		
		$JsonObj = new JsonModel(array(
				'tab'					 	=> $user_session->tab,
				'property_id'			 	=> $user_session->property_id,		
		));
		
		return $JsonObj; 
	}

	public function getsessionajaxAction(){

		$user_session = new Container('user');

		if( isset($_REQUEST['salt']) && ($_REQUEST['salt'] == $user_session->fingerprint ) ){


				$JsonObj = new JsonModel(array(
						'email' 				 	=> $user_session->email,
						'id'					 	=> $user_session->id,
						'last_active'   		 	=> $user_session->last_active,
						'fingerpring'   		 	=> $user_session->fingerprint,
						'isloggedin'    		 	=> $user_session->isloggedin,
						'tab'					 	=> $user_session->tab,
						'property_id'			 	=> $user_session->property_id,
						'first_column_property_id'  => $user_session->first_column_property_id,
						'second_column_property_id' => $user_session->second_column_property_id,
						'third_column_property_id'  => $user_session->third_column_property_id,
						'fourth_column_property_id' => $user_session->fourth_column_property_id,

				));
				return $JsonObj;

		}else{

			return new JsonModel();
		}

	}
	
	public function getmembercurrenttokenajaxAction(){
		
		$sm = $this->getServiceLocator();
		
		$user_session = new Container('user');
		$member_monthly_tokenTable = $sm->get('Index\Model\Member_monthly_tokenTable');
		
		$member_monthly_token = $member_monthly_tokenTable->getMembercurrenttoken( $user_session->id );
		
		$member_monthly_token = new Membermonthlytoken( $member_monthly_token );
		
		$JsonObj = new JsonModel(array(
				'start_date'					=>    $member_monthly_token->get_start_date(),
				'stop_date'						=>    $member_monthly_token->get_stop_date(),
				'num_of_properties'				=>	  $member_monthly_token->get_num_of_properties(),
				'num_of_email_per_month'		=>	  $member_monthly_token->get_num_of_email_per_month(),
				'used_num_of_email_per_month'	=>    $member_monthly_token->get_used_num_of_email_per_month()
		));
		
		return $JsonObj;
	}
	
	
	public function getmembercurrentpropertiesajaxAction(){
		
		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
		
		$propertyTable = $sm->get('Index\Model\PropertyTable');
		
		$rowset    = $propertyTable->getMemberProperties( $user_session->id );
		
		$JsonObj = new JsonModel(array(
					'num_of_current_properties' 	=> count( $rowset ),
				));
			
		
		return $JsonObj;
	}
	
	public function increasenumofemailajaxAction(){
		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
		
		$member_monthly_tokenTable = $sm->get('Index\Model\Member_monthly_tokenTable');
		
		$member_monthly_token = $member_monthly_tokenTable->getMembercurrenttoken( $user_session->id );
		
		
		$member_monthly_token = new Membermonthlytoken( $member_monthly_token );
		
		$member_monthly_token->set_used_num_of_email_per_month( $member_monthly_token->get_used_num_of_email_per_month() + 1 );
		
		$member_monthly_tokenTable->saveMembermonthlytoken($member_monthly_token);
		
		$JsonObj = new JsonModel(array(
				'sucess' 	=> true,
		));
			
		
		return $JsonObj;
	}
	
	public function surwayajaxAction(){
		
		$sm = $this->getServiceLocator();
		$user_session = new Container('user');
		
		$surway_Table = $sm->get('Index\Model\SurwayTable');
		
		$surway = new Surway();
		$surway->set_member_id($user_session->id );
		
		$surway_Table->saveSurway($surway);
		
		$JsonObj = new JsonModel(array(
				'sucess' 	=> true,
		));
			
		
		return $JsonObj;
	}

}


/*
 *
 * 'name' 		=> 'server.digidev.com',
				'host' 		=> 'server.digidev.com',
				'username'	=> 'service+digidev.com',
				'password'  => 'ServiceIsC00l!!!'
 */





