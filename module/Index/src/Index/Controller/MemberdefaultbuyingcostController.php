<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

use Index\Model\Memberdefaultbuyingcostitem;
	 
class MemberdefaultbuyingcostController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	
	public function indexajaxAction(){

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		
		$member_buyingcost_Table 		 = $sm->get('Index\Model\Member_buyingcost_itemTable');	
		$memberTable 					 = $sm->get('Index\Model\MemberTable');
		
		$member = $memberTable->getMember( $user_session->id );

		$buyingcost_items 				 = $member_buyingcost_Table->getMemberbuyingcostitems($user_session->id);
		$member_default_buyingcost_items = $member->get_default_buyingcost_items();
		
		$view = new ViewModel(array(
								'buyingcost_items' 					=> $buyingcost_items,
								'member_default_buyingcost_items' 	=> $member_default_buyingcost_items
							));
			
		$view->setTerminal(true);
		return $view;

		
	}
	
	public function addbuyingcostitemajaxAction(){
		
		$user_session = new Container('user');

		$buyingcost_item_amount = $_POST['buyingcost_item_amount'];
		$buyingcost_item_id     = $_POST['buyingcost_item_id'];

		$sm = $this->getServiceLocator();
		$this->Member_default_buyingcost_itemTable = $sm->get('Index\Model\Member_default_buyingcost_itemTable');

		$member_default_buyingcost_item = new Memberdefaultbuyingcostitem();

		$member_default_buyingcost_item->set_member_id($user_session->id);
		$member_default_buyingcost_item->set_buyingcost_id($buyingcost_item_id);
		$member_default_buyingcost_item->set_member_default_buyingcost_item_amount($buyingcost_item_amount);
		
		$member_default_buyingcost_item_id = $this->Member_default_buyingcost_itemTable->saveMember_default_buyingcost_item( $member_default_buyingcost_item );
		
		$this->MemberTable = $sm->get('Index\Model\MemberTable');
		$member = $this->MemberTable->getMember($user_session->id);
		$member_default_buyingcost_items = $member->get_default_buyingcost_items();
		
		$view = new ViewModel(array(
				'member_default_buyingcost_items' => $member_default_buyingcost_items
		));
		$view->setTerminal(true);
		
		return $view;
				
	}
	
	public function deletebuyingcostitemajaxAction(){

		$user_session 						= new Container('user');
		$member_default_buyingcost_item_id  = $_POST['member_default_buyingcost_item_id'];

		$sm = $this->getServiceLocator();
		$this->Member_default_buyingcost_itemTable = $sm->get('Index\Model\Member_default_buyingcost_itemTable');

		$this->Member_default_buyingcost_itemTable->deleteMember_default_buyingcost_item($member_default_buyingcost_item_id);

		$this->MemberTable = $sm->get('Index\Model\MemberTable');
		$member = $this->MemberTable->getMember( $user_session->id );
		$member_default_buyingcost_items = $member->get_default_buyingcost_items();
		
		$view = new ViewModel(array(
				'member_default_buyingcost_items' => $member_default_buyingcost_items
		));
		$view->setTerminal(true);
		
		return $view;

	}



}
