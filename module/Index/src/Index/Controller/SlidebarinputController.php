<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Index\Model\Property;
use Index\Model\Member;

class SlidebarinputController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	public function vacancyrateAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property_vacancy_rate = $property->get_property_vacancy_rate();

		$view = new ViewModel(
				array(
						'property_vacancy_rate' => $property_vacancy_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	}

	public function appreciationrateAction() {

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property_appreciation_rate = $property->get_property_appreciation_rate();

		$view = new ViewModel(
								array(
										'property_appreciation_rate' => $property_appreciation_rate
									 )
							 );
		$view->setTerminal(true);
		return $view;
	}


	public function incomeinflationrateAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property_incomeinflation_rate = $property->get_property_incomeinflation_rate();

		$view = new ViewModel(
				array(
						'property_incomeinflation_rate' => $property_incomeinflation_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	}

	public function expenseinflationrateAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property_expenseinflation_rate = $property->get_property_expenseinflation_rate();

		$view = new ViewModel(
				array(
						'property_expenseinflation_rate' => $property_expenseinflation_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	}


	public function lvtforrefinanceAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property_lvt_for_refinance = $property->get_property_lvt_for_refinance();

		$view = new ViewModel(
				array(
						'property_lvt_for_refinance' => $property_lvt_for_refinance
				)
		);
		$view->setTerminal(true);
		return $view;
	}

   /////////////// from now on, it's for default value //////////
   
	public function defaultvacancyrateAction(){
		
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$member = $this->memberTable->getMember($user_session->id);
		$member = new Member( $member );
		
		$property_default_vacancy_rate = $member->get_property_default_vacancy_rate();
		
		$view = new ViewModel(
				array(
						'property_default_vacancy_rate' => $property_default_vacancy_rate
				)
		);
		$view->setTerminal(true);
		return $view;
		
	}
   
	public function defaultappreciationrateAction() {
	
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember($user_session->id);
		$member = new Member( $member );
	
		$property_default_appreciation_rate = $member->get_property_default_appreciation_rate();
		
		$view = new ViewModel(
				array(
						'property_default_appreciation_rate' => $property_default_appreciation_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	}
	
	
	public function defaultincomeinflationrateAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
	
		$member = $this->memberTable->getMember($user_session->id);
		$member = new Member( $member );
	
		$property_default_incomeinflation_rate = $member->get_property_default_incomeinflation_rate();
	
		$view = new ViewModel(
				array(
						'property_default_incomeinflation_rate' => $property_default_incomeinflation_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	}
	
	public function defaultexpenseinflationrateAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$member = $this->memberTable->getMember($user_session->id);
		$member = new Member( $member );
		
		$property_default_expenseinflation_rate = $member->get_property_default_expenseinflation_rate();
		
		
		$view = new ViewModel(
				array(
						'property_default_expenseinflation_rate' => $property_default_expenseinflation_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	}
	
	public function defaultlvtforrefinanceAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$member = $this->memberTable->getMember($user_session->id);
		$member = new Member( $member );
		
		$property_default_lvt_for_refinance = $member->get_property_default_lvt_for_refinance();
		
		$view = new ViewModel(
				array(
						'property_default_lvt_for_refinance' => $property_default_lvt_for_refinance
				)
		);
		$view->setTerminal(true);
		return $view;
	}
   



}
