<?php

namespace Index\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;



class PropertymortgagecostController extends AbstractActionController{

	public function indexAction() {
		// TODO Auto-generated PropertymortgagecostController::indexAction()
	// default action
	}

	public function firstmortgageajaxAction() {

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$this->itemTable = $sm->get('Index\Model\ItemTable');

		$payment_modes   = $this->itemTable->get_Items('payment_mode');
		$loan_types      = $this->itemTable->get_Items('loantype');

		$property = $this->propertyTable->getProperty($user_session->property_id);

		//$property = new Property();

		$downpayment 		 = $property->get_property_first_loanamount();
		$purchase_price      = $property->get_property_purchaseprice();
		$interested_rate     = $property->get_property_first_interestrate();
		$loan_term           = $property->get_property_first_term();
		$payment_frequency   = $property->get_property_first_payment_frequency();
		$loan_type_value     = $property->get_property_first_loantype();
		$interestedonly_rate = $property->get_property_first_interestonly_term();


		$view = new ViewModel(
						array(
								'downpayment'		 =>  $downpayment,
								'purchase_price'     =>  $purchase_price,
								'interested_rate'	 =>  $interested_rate,
								'loan_term'			 =>  $loan_term,
								'payment_modes'      =>  $payment_modes,
								'payment_frequency'  =>  $payment_frequency,
								'loan_types'         =>  $loan_types,
								'loan_type_value'	 =>  $loan_type_value,
								'interestedonly_rate'=>  $interestedonly_rate
							 )
							 );
		$view->setTerminal(true);
		return $view;

	}
	
	public function secondmortgageajaxAction() {
	
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
	
		$this->itemTable = $sm->get('Index\Model\ItemTable');
	
		$payment_modes   = $this->itemTable->get_Items('payment_mode');
		$loan_types      = $this->itemTable->get_Items('loantype');
	
		$property = $this->propertyTable->getProperty($user_session->property_id);
	
		//$property = new Property();
	
		$downpayment 		 = $property->get_property_second_loanamount();
		$purchase_price      = $property->get_property_purchaseprice();
		$interested_rate     = $property->get_property_second_interestrate();
		$loan_term           = $property->get_property_second_term();
		$payment_frequency   = $property->get_property_second_payment_frequency();
		$loan_type_value     = $property->get_property_second_loantype();
		$interestedonly_rate = $property->get_property_second_interestonly_term();
	
	
		$view = new ViewModel(
				array(
						'downpayment'		 =>  $downpayment,
						'purchase_price'     =>  $purchase_price,
						'interested_rate'	 =>  $interested_rate,
						'loan_term'			 =>  $loan_term,
						'payment_modes'      =>  $payment_modes,
						'payment_frequency'  =>  $payment_frequency,
						'loan_types'         =>  $loan_types,
						'loan_type_value'	 =>  $loan_type_value,
						'interestedonly_rate'=>  $interestedonly_rate
				)
		);
		$view->setTerminal(true);
		return $view;
	
	}
	
	public function defaultfirstmortgageajaxAction() {
		
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$this->itemTable = $sm->get('Index\Model\ItemTable');
		
		$payment_modes   = $this->itemTable->get_Items('payment_mode');
		$loan_types      = $this->itemTable->get_Items('loantype');
		
		$member = $this->memberTable->getMember($user_session->id);
		
		//$member = new Member();
				
		$downpayment		 = $member->get_property_default_first_loanamount();
		$purchase_price      = $member->get_property_default_purchaseprice();
		$interested_rate	 = $member->get_property_default_first_interestrate();
		$loan_term			 = $member->get_property_default_first_term();
		$payment_frequency	 = $member->get_property_default_first_payment_frequency();
		$loan_type_value     = $member->get_property_default_first_loantype();
		$interestedonly_rate = $member->get_property_default_first_interestonly_term();
				
		$view = new ViewModel(
				array(
						'downpayment'		 =>  $downpayment,
						'purchase_price'     =>  $purchase_price,
						'interested_rate'	 =>  $interested_rate,
						'loan_term'			 =>  $loan_term,
						'payment_modes'      =>  $payment_modes,
						'payment_frequency'  =>  $payment_frequency,
						'loan_types'         =>  $loan_types,
						'loan_type_value'	 =>  $loan_type_value,
						'interestedonly_rate'=>  $interestedonly_rate
				)
		);
		$view->setTerminal(true);
		return $view;
		
	}
	
	public function defaultsecondmortgageajaxAction(){
		
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->memberTable = $sm->get('Index\Model\MemberTable');
		
		$this->itemTable = $sm->get('Index\Model\ItemTable');
		
		$payment_modes   = $this->itemTable->get_Items('payment_mode');
		$loan_types      = $this->itemTable->get_Items('loantype');
		
		$member = $this->memberTable->getMember($user_session->id);
		
		//$member = new Member();
		
		$downpayment		 = $member->get_property_default_second_loanamount();
		$purchase_price      = $member->get_property_default_purchaseprice();
		$interested_rate	 = $member->get_property_default_second_interestrate();
		$loan_term			 = $member->get_property_default_second_term();
		$payment_frequency	 = $member->get_property_default_second_payment_frequency();
		$loan_type_value     = $member->get_property_default_second_loantype();
		$interestedonly_rate = $member->get_property_default_second_interestonly_term();
		
		$view = new ViewModel(
				array(
						'downpayment'		 =>  $downpayment,
						'purchase_price'     =>  $purchase_price,
						'interested_rate'	 =>  $interested_rate,
						'loan_term'			 =>  $loan_term,
						'payment_modes'      =>  $payment_modes,
						'payment_frequency'  =>  $payment_frequency,
						'loan_types'         =>  $loan_types,
						'loan_type_value'	 =>  $loan_type_value,
						'interestedonly_rate'=>  $interestedonly_rate
				)
		);
		$view->setTerminal(true);
		return $view;
				
	}

	public function getpaymenttableajaxAction(){
		
		$percent 			= $_GET['percent'];
		$interested_rate    = $_GET['interested_rate'];
		$loan_term          = $_GET['loan_term'];
		$payment_mode       = $_GET['payment_mode'];
		$loan_type          = $_GET['loan_type'];
		$int_only_term      = $_GET['int_only_term'];

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();

		$itemtable 		= $sm->get('Index\Model\ItemTable');
		$propertytable 	= $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);

		$purchase_price = $property->get_property_purchaseprice();

		$loan_amount = $purchase_price * ( 1-( $percent/100 ) );

		

		if( $payment_mode == $itemtable->get_Item_id('payment_mode','biweekly') ){
			$n = $loan_term * 26;

			$n1 = $int_only_term * 26;
			$n2 = ( $loan_term - $int_only_term ) * 26;
			$int_only_paymentterm=26;
		}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','monthly') ){
			$n = $loan_term * 12;

			$n1 = $int_only_term * 12;
			$n2 = ( $loan_term - $int_only_term ) * 12;
			$int_only_paymentterm=12;
		}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','yearly') ){
			$n = $loan_term;

			$n1 = $int_only_term;
			$n2 = $loan_term - $int_only_term ;
			$int_only_paymentterm=1;
		}else{
			//error
		}

		$c = ( ( $interested_rate/100 ) / 12);


		if( $loan_type == $itemtable->get_Item_id('loantype','amortizing') ){
			$payments='';
			$payment_amount = ($loan_amount * ( $c * pow( (1+$c), $n ) ) )  / ( ( pow( (1+$c), $n ) ) - 1 );
			$previous_principal=0;

			for($p=1; $p<=$n; $p++){

 				$balance     = ( $loan_amount * ( pow( (1+$c), $n ) - pow( (1+$c), $p ) ) )/( ( pow( (1+$c), $n ) - 1 ) );
 				$principal   = $loan_amount - $balance - $previous_principal;
 				$interest    = $payment_amount - $principal;
 				$previous_principal += $principal;

				if( $payment_mode == $itemtable->get_Item_id('payment_mode','biweekly') ){
					$year = intval( ( $p-1 )/26) + 1;
				}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','monthly') ){
					$year = intval( ( $p-1 ) /12) + 1;
				}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','yearly') ){
					$year = $p;
				}else{
					//error
				}


				$payments[$p-1] = array(
										'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
										'year'			 =>  $year,
										'number'		 =>  $p,
										'interest'		 =>  number_format($interest, 2, '.', ''),
										'principal'		 =>  number_format($principal, 2, '.', ''),
										'balance'		 =>  number_format($balance, 2, '.', ''),
									   );


			}
		}else if( $loan_type == $itemtable->get_Item_id('loantype','interested only') ){
			$payment_amount = ( $loan_amount*( $interested_rate/100 ) ) / $int_only_paymentterm;


			for($p=1; $p<=$n1; $p++){
				$balance 	= $loan_amount;
				$principal  = 0;
				$interest   = $payment_amount;

				if( $payment_mode == $itemtable->get_Item_id('payment_mode','biweekly') ){
					$year = intval( ( $p-1 )/26) + 1;
				}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','monthly') ){
					$year = intval( ( $p-1 ) /12) + 1;
				}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','yearly') ){
					$year = $p;
				}else{
					//error
				}

				$payments[$p-1] = array(
										'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
										'year'			 =>  $year,
										'number'		 =>  $p,
										'interest'		 =>  number_format($interest, 2, '.', ''),
										'principal'		 =>  number_format($principal, 2, '.', ''),
										'balance'		 =>  number_format($balance, 2, '.', ''),
									   );
			}

			$payment_amount2 = ($loan_amount * ( $c * pow( (1+$c), $n2 ) ) )  / ( ( pow( (1+$c), $n2 ) ) - 1 );
			$previous_principal=0;

			$index = $n1;
			for($p=1; $p<=$n2; $p++){
				$balance     = ( $loan_amount * ( pow( (1+$c), $n2 ) - pow( (1+$c), $p ) ) )/( ( pow( (1+$c), $n2 ) - 1 ) );
				$principal   = $loan_amount - $balance - $previous_principal;
				$interest    = $payment_amount2 - $principal;
				$previous_principal += $principal;

				if( $payment_mode == $itemtable->get_Item_id('payment_mode','biweekly') ){
					$year = intval( ( $p-1 )/26) + 1 + $int_only_term;
				}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','monthly') ){
					$year = intval( ( $p-1 ) /12) + 1 + $int_only_term;
				}else if( $payment_mode == $itemtable->get_Item_id('payment_mode','yearly') ){
					$year = $p + $int_only_term;
				}else{
					//error
				}

				$payments[$index] = array(
						'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
						'year'			 =>  $year,
						'number'		 =>  $index+1,
						'interest'		 =>  number_format($interest, 2, '.', ''),
						'principal'		 =>  number_format($principal, 2, '.', ''),
						'balance'		 =>  number_format($balance, 2, '.', ''),
				);

				$index++;
			}


		}else{
			//error
			$payments[0] = array(
					'onetime_payment'=>  '1',
					'year'			 =>  $itemtable->get_Item_id('loantype','interested only'),
					'number'		 =>  $loan_type,
					'interest'		 =>  '1',
					'principal'		 =>  '1',
					'balance'		 =>  '1',
			);
		}

		$JsonObj = new JsonModel($payments);

        return $JsonObj;
	}



	
}
