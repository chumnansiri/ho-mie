<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

/**
 * UtilityController
 *
 * @author
 *
 * @version
 *
 */
class UtilityController extends AbstractActionController {
	/**
	 * The default action - show the home page
	 */
	public function indexAction() {
		// TODO Auto-generated UtilityController::indexAction() default action
		return new ViewModel ();
	}
	
	public function tooltipajaxAction(){
	
		$tooltip_name = $_POST['tooltip_name'];
		
		$sm = $this->getServiceLocator();
		$itemTable = $sm->get('Index\Model\ItemTable');
		
		$html = $itemTable->get_Item_text('tooltip',$tooltip_name);
		$text = $itemTable->get_Item_value('tooltip',$tooltip_name);
							
		$JsonObj = new JsonModel(array(
				'html' 				 	=> $html,
				'text'					=> $text,
		));
		return $JsonObj;
	}
}