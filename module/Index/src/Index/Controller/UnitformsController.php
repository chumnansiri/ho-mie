<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Index\Model\Unit;


class UnitformsController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	public function addunitajaxAction(){

		$view = new ViewModel();
		$view->setTerminal(true);
		return $view;

	}

	public function editunitajaxAction(){

		$unit_id = $_POST['unit_id'];

		$sm = $this->getServiceLocator();
		$unitTable = $sm->get('Index\Model\UnitTable');

		$unit = $unitTable->getUnit($unit_id);
		$unit = new Unit( $unit );

		$unit_rents = $unit->get_unit_rents();

		$unit_rent_array =  array();
		foreach( $unit_rents as $unit_rent){

			$unit_rent_array[] = array(
										'year'       =>    $unit_rent->get_year(),
					                    'rent'       =>    $unit_rent->get_unit_rent_rent(),
					                    'frequency'  =>    $unit_rent->get_unit_rent_frequency(),
									  );

		}


		$view = new ViewModel(
								array(
										'unit_id'            =>      $unit_id,
										'unit_type'  		 => 	 $unit->get_unit_type(),
										'squar_feet'         =>      $unit->get_unit_squarfeet(),
										'furnished'          =>      $unit->is_furnished(),
										'quality'            =>      $unit->get_number_of_units(),
										'bed'                =>      $unit->get_number_of_bedrooms(),
										'bath'               =>      $unit->get_number_of_bathrooms(),
										'desc'               =>      $unit->get_unit_description(),
										'unit_rent_schedules'=>      $unit_rent_array
									 )
							 );
		$view->setTerminal(true);
		return $view;

	}




}
/*
$this->unit_id  		   =        $unit->get_unit_id();
$this->property_id         = 	    $unit->get_property_id();
$this->unit_type	       =        $unit->get_unit_type();
$this->number_of_units     =		$unit->get_number_of_units();
$this->number_of_bedrooms  =        $unit->get_number_of_bedrooms();
$this->number_of_bathrooms =        $unit->get_number_of_bathrooms();
$this->furnished           =        $unit->is_furnished();
$this->unit_squarfeet      =        $unit->get_unit_squarfeet();
$this->unit_description    =        $unit->get_unit_description();
$this->unit_rents          =        $unit->get_unit_rents();
$this->sort_by             =        $unit->get_sort_by();
$this->created_date        =        $unit->get_created_date();
$this->last_modified       =        $unit->get_last_modified();
*/