<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

use Index\Model\Propertysellingcostitem;

class PropertysellingcostController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	public function indexajaxAction() {

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$member_sellingcost_Table = $sm->get('Index\Model\Member_sellingcost_itemTable');

		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );

		$property_sellingcost_items = $property->get_sellingcost_items();


		$sellingcost_items 		= $member_sellingcost_Table->getMembersellingcostitems($user_session->id);

		$view = new ViewModel(array(
								'sellingcost_items' 			=> $sellingcost_items,
								'property_sellingcost_items' 	=> $property_sellingcost_items
							));
		$view->setTerminal(true);
		return $view;

	}

	public function addsellingcostitemajaxAction(){
		$user_session = new Container('user');

		$sellingcost_item_amount = $_POST['sellingcost_item_amount'];
		$sellingcost_item_id     = $_POST['sellingcost_item_id'];

		$sm = $this->getServiceLocator();
		$this->Property_sellingcost_itemTable = $sm->get('Index\Model\Property_sellingcost_itemTable');

		$property_sellingcost_item = new Propertysellingcostitem();

		$property_sellingcost_item->set_property_id($user_session->property_id);
		$property_sellingcost_item->set_sellingcost_id($sellingcost_item_id);
		$property_sellingcost_item->set_property_sellingcost_item_amount($sellingcost_item_amount);

		$property_sellingcost_item_id = $this->Property_sellingcost_itemTable->saveProperty_sellingcost_item($property_sellingcost_item);

		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );
		$property_sellingcost_items = $property->get_sellingcost_items();

		$view = new ViewModel(array(
				'property_sellingcost_items' => $property_sellingcost_items
		));
		$view->setTerminal(true);

		return $view;


	}

	public function deletesellingcostitemajaxAction(){

		$user_session 			= new Container('user');
		$property_sellingcost_item_id     = $_POST['property_sellingcost_item_id'];

		$sm = $this->getServiceLocator();
		$this->Property_sellingcost_itemTable = $sm->get('Index\Model\Property_sellingcost_itemTable');

		$this->Property_sellingcost_itemTable->deleteProperty_sellingcost_item($property_sellingcost_item_id);

		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );
		$property_sellingcost_items = $property->get_sellingcost_items();

		$view = new ViewModel(array(
				'property_sellingcost_items' => $property_sellingcost_items
		));
		$view->setTerminal(true);

		return $view;


	}


}
