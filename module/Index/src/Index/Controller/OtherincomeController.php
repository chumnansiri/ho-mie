<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Index\Model\Propertymemberincomeitem;


class OtherincomeController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	public function indexajaxAction() {

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->memberincomeitemTable = $sm->get('Index\Model\Member_income_itemTable');


		$memberincomeitems 		= $this->memberincomeitemTable->getMemberincomeitems($user_session->id);


		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );
		$property_otherincome_items = $property->get_other_income_items();

		$view = new ViewModel(array(
								'memberincomeitems' 			=> $memberincomeitems,
								'propertyotherincomeitems'		=> $property_otherincome_items

							));
		$view->setTerminal(true);
		return $view;

	}

	public function addotherincomeitemajaxAction(){

		$user_session = new Container('user');

		$otherincome_item_id 		= $_POST['otherincome_item_id'];
		$otherincome_item_amount    = $_POST['otherincome_item_amount'];
		$otherincome_intem_frequency= $_POST['otherincome_item_frequency'];

		$sm = $this->getServiceLocator();

		$this->Property_buyingcost_itemTable = $sm->get('Index\Model\Property_member_income_itemTable');

		$property_member_income_item = new Propertymemberincomeitem();

		$property_member_income_item->set_property_id($user_session->property_id);
		$property_member_income_item->set_income_id($otherincome_item_id);
		$property_member_income_item->set_amount($otherincome_item_amount);
		$property_member_income_item->set_frequency($otherincome_intem_frequency);

		$property_member_income_item_id = $this->Property_buyingcost_itemTable->saveProperty_member_income_item($property_member_income_item);


		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );
		$property_other_income_items = $property->get_other_income_items();

		$view = new ViewModel(array(
				'property_other_income_items' => $property_other_income_items
		));
		$view->setTerminal(true);

		return $view;

	}

	public function deleteotherincomeitemajaxAction(){
		$user_session 				 = new Container('user');
		$property_income_item_id     = $_POST['property_income_item_id'];

		$sm = $this->getServiceLocator();
		$this->Property_otherincomet_itemTable = $sm->get('Index\Model\Property_member_income_itemTable');

		$this->Property_otherincomet_itemTable->deleteProperty_member_income_item($property_income_item_id);

		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );
		$property_other_income_items = $property->get_other_income_items();

		$view = new ViewModel(array(
				'property_other_income_items' => $property_other_income_items
		));
		$view->setTerminal(true);

		return $view;

	}




}
