<?php
namespace Index\Controller;


use Index\Model\Unitrent;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;

use Index\Model\Property;
use Index\Model\Unit;



class UnitController extends AbstractActionController
{

	private $units;

	public function __construct(){


	}

	public function onDispatch( \Zend\Mvc\MvcEvent $e )
	{


			return parent::onDispatch( $e );
	}


	public function getunittabajaxAction(){

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);


		$units = $property->get_units();

		$unit_array = array();

		foreach( $units as $unit){

			$unit_rents = $unit->get_unit_rents();
			$unit_rent_array = array();
			foreach( $unit_rents as $unit_rent){
				$unit_rent_array[] = array(
											'unit_rent_id'		  => $unit_rent->get_unit_rent_id(),
						                    'unit_rent_rent'      => $unit_rent->get_unit_rent_rent(),
						                    'unit_rent_frequency' => $unit_rent->get_unit_rent_frequency(),
						                    'year'                => $unit_rent->get_year(),
											'sort_by'			  => $unit_rent->get_sort_by(),
											'created_date'		  => $unit_rent->get_created_date(),
											'last_modified'       => $unit_rent->get_last_modified()

										);
			}

			$unit_array[] = array(
										'unit_id'				=> $unit->get_unit_id(),
										'property_id'       	=> $unit->get_property_id(),
										'unit_type'				=> $unit->get_unit_type(),
										'number_of_units'		=> $unit->get_number_of_units(),
										'number_of_bedrooms'	=> $unit->get_number_of_bedrooms(),
										'number_of_bathrooms'	=> $unit->get_number_of_bathrooms(),
										'furnished'				=> $unit->is_furnished(),
										'unit_squarfeet'		=> $unit->get_unit_squarfeet(),
										'unit_description'		=> $unit->get_unit_description(),
										'unit_rents'            => $unit_rent_array,
										'sort_by'				=> $unit->get_sort_by(),
										'created_date'			=> $unit->get_created_date(),
										'last_modified' 		=> $unit->get_last_modified()

								);

		}

		$JsonObj = new JsonModel(array(
				'property_name'      => $property->get_property_name(),
				'units'	 			 => $unit_array,
		));

		return $JsonObj;
	}

	public function addingunitajaxAction(){

		$type_name 	 	= $_POST['type_name'];
		$squar_feet  	= $_POST['squar_feet'];
		$furnished   	= $_POST['furnished'];
		$quality     	= $_POST['quality'];
		$bed         	= $_POST['bed'];
		$bath        	= $_POST['bath'];
		$description 	= $_POST['description'];

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
	    $unitTable = $sm->get('Index\Model\UnitTable');

		$unit = new Unit();
		$unit->set_unit_type($type_name);
		$unit->set_unit_squarfeet($squar_feet);
		$unit->set_furnished($furnished);
		$unit->set_number_of_units($quality);
		$unit->set_number_of_bedrooms($bed);
		$unit->set_number_of_bathrooms($bath);
		$unit->set_unit_description($description);
		$unit->set_property_id($user_session->property_id);
		$unit_id = $unitTable->saveUnit($unit);

		if ( isset( $_POST['rent_schedule'] ) ){

			$unitTable = $sm->get('Index\Model\UnitrentTable');


			$rent_schedule  = $_POST['rent_schedule'];
			foreach($rent_schedule as $row){

				$unitrent = new Unitrent();
				$unitrent->set_unit_id($unit_id);

				$unitrent->set_year($row['year']);
				$unitrent->set_unit_rent_rent($row['rent']);
				$unitrent->set_unit_rent_frequency($row['frequency']);
				$unitTable->saveUnitrent($unitrent);

			}

		}
		return $this->response;
	}
	public function editingunitajaxAction(){
		$unit_id        = $_POST['unit_id'];
		$type_name 	 	= $_POST['type_name'];
		$squar_feet  	= $_POST['squar_feet'];
		$furnished   	= $_POST['furnished'];
		$quality     	= $_POST['quality'];
		$bed         	= $_POST['bed'];
		$bath        	= $_POST['bath'];
		$description 	= $_POST['description'];

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$unitTable = $sm->get('Index\Model\UnitTable');

		$unit = $unitTable->getUnit( $unit_id );
		$unit = new Unit($unit);
		$unit->set_unit_type($type_name);
		$unit->set_unit_squarfeet($squar_feet);
		$unit->set_furnished($furnished);
		$unit->set_number_of_units($quality);
		$unit->set_number_of_bedrooms($bed);
		$unit->set_number_of_bathrooms($bath);
		$unit->set_unit_description($description);
		$unit->set_property_id($user_session->property_id);
		$unit_id = $unitTable->saveUnit($unit);

		// if there rent schedule
		$unitrentTable = $sm->get('Index\Model\UnitrentTable');

		$years = array();
		if ( isset( $_POST['rent_schedule'] ) ){ // there's rent schedule

			foreach($_POST['rent_schedule'] as $rent_schedule){
				$years[] = $rent_schedule['year'];

				if( $unitrentTable->isexist($unit_id, $rent_schedule['year'] ) ){
					// update if the year already exist
					$unitrentTable->update_unit_by_year($unit_id,$rent_schedule['year'],$rent_schedule['rent'],$rent_schedule['frequency']);

				}else{
					// update if it's new year
					$new_unitrent = new Unitrent();
					$new_unitrent->set_unit_id($unit_id);
					$new_unitrent->set_unit_rent_rent($rent_schedule['rent']);
					$new_unitrent->set_unit_rent_frequency($rent_schedule['frequency']);
					$new_unitrent->set_year($rent_schedule['year']);
					$unitrentTable->saveUnitrent($new_unitrent);

				}

			}
			/// remove the rest rents
			$unitrentTable->deleteUnitrents($unit_id,$years);

		}else{// there's no rent schedule, remove all rents
			$years[] = 0;
			$unitrentTable->deleteUnitrents($unit_id,$years);
		}


		return $this->response;
	}

	public function deletingunitajaxAction(){
		$unit_id = $_POST['unit_id'];

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$unitTable = $sm->get('Index\Model\UnitTable');
		$unitTable->deleteUnit($unit_id);


		echo 'done';
		return $this->response;
	}

}
