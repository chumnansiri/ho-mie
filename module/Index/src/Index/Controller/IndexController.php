<?php
namespace Index\Controller;



//use Index\Model\MyPaydateCalculator;

use Index\Model\Member;
use Index\Model\Memberbuyingcostitem;
use Index\Model\Membersellingcostitem;
use Index\Model\Memberincomeitem;
use Index\Model\Memberexpenseitem;
use Index\Model\Membersubscription;



use Zend\View\Model\ViewModel;

use Zend\Session\Container;


use Zend\Mvc\Controller\AbstractActionController;

use Index\Form\SignupForm;

use Index\Form\SigninForm;
use Index\Form\ForgotpasswordForm;

use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;


use Zend\View\Model\JsonModel;
use \TCPDF;
use Index\Model\Membershiptype;
use Index\Model\Membermonthlytoken;


//require_once('/var/www/html/zf2/module/Index/libraries/MPDF/mpdf.php');



class IndexController extends AbstractActionController
{
	

	public function __construct(){


	}
	
	
	
	public function testAction(){
		
		$request = $this->getRequest();
		if ($request->isPost()) {
			// Make certain to merge the files info!
			$post = array_merge_recursive(
					$request->getPost()->toArray(),
					$request->getFiles()->toArray()
			);
		
			 
			if ($_FILES["image-file"]["error"] == 0){
				//return 'voranon';
				// Form is valid, save the form!
				 
				$temp = explode(".", $_FILES["image-file"]["name"]);
				$extension = end($temp);
				$file_name = 'test.'.$extension;
				$file_path = "/img/members/".$file_name;
				 
				// to remove old file incase different file extension
				//	system('rm -f '.getcwd().'/public/img/members/member_id_'.$user_session->id.'/cover_photo_id_'.$property_id.'.*');
				 
				// 	$property->set_property_cover_photo_path($file_path);
				move_uploaded_file( $_FILES["image-file"]["tmp_name"],getcwd()."/public".$file_path);
		
				 
				if (!empty($post['isAjax'])) {
					return new JsonModel(array(
							'status'   => true,
							'redirect' => 'test',
							//  'formData' => $data,
					));
				} else {
					// Fallback for non-JS clients
					return $this->redirect()->toRoute('index/test');
				}
				 
			} else {
				 
				 
				if (!empty($post['isAjax'])) {
					// Send back failure information via JSON
					return new JsonModel(array(
							'status'     => false,
							// 'formErrors' => 'can not upload',
							//    'formData'   => $form->getData(),
					));
				}
		
			}
		}
		
		
		
	}
	
	
	public function test1Action(){
		
		$user_session = new Container('user');
		$request = $this->getRequest();
			
		$post = array_merge_recursive(
				$request->getPost()->toArray(),
				$request->getFiles()->toArray()
		);
		
		if ($_FILES["cover_photo"]["error"] > 0){
			///////////////////////// new code
		
			if (!empty($post['isAjax'])) {
				// Send back failure information via JSON
				return new JsonModel(array(
						'status'     => false,
						'formErrors' => 'can not upload',
		
				));
			}
			//////////////////////// new code
		}else{
				
			$sm = $this->getServiceLocator();
			$PropertyTable = $sm->get('Index\Model\PropertyTable');
		
				
		
			$property_id = $post['property_id'];
			$user_id = $post['user_id'];
		
			$property = $PropertyTable->getProperty($property_id);
		
			$temp = explode(".", $_FILES["cover_photo"]["name"]);
		
			$extension = end($temp);
		
			$file_name = 'cover_photo_id_'.$property_id.'.'.$extension;
		
			$file_path = "/img/members/member_id_".$user_id."/".$file_name;
		
			// to remove old file incase different file extension
			system('rm -f '.getcwd().'/public/img/members/member_id_'.$user_id.'/cover_photo_id_'.$property_id.'.*');
		
			$property->set_property_cover_photo_path($file_path);
			move_uploaded_file( $_FILES["cover_photo"]["tmp_name"],getcwd()."/public".$file_path);
			$PropertyTable->saveProperty( $property );
		
				
				
				
				
				
			///////////////////////// new code
			if ( !empty( $post['isAjax'] ) ) {
				
				return new JsonModel(array(
						'status'   => true,
						'redirect' => '/member/photos',
						'property_id'     => $user_session->property_id,
						'user_id'         => $user_session->id 
				
				));
			} else {
		
				// Fallback for non-JS clients
		
				return $this->redirect()->toRoute('member/photos');
					
			}
		
		
			return new JsonModel(array(
					'status'   => true,
					'redirect' => 'photos',
		
			));
			//////////////////////// new code
		}
		
		
	}
	
	public function uploadprogressAction()
	{
		//session_start();
		
		//$user_session = new Container('user');
		
		$id = $this->params()->fromQuery('id', null);
		$progress = new \Zend\ProgressBar\Upload\SessionProgress();		
		return new \Zend\View\Model\JsonModel( $progress->getProgress($id) );	
	}
	
	

	public function onDispatch( \Zend\Mvc\MvcEvent $e )
    {
   
    	$trafficlog = $this->getServiceLocator()->get('Trafficlog');
    	$trafficlog->log(
    			array('remote_addr' 	=> $_SERVER['REMOTE_ADDR'],
    				  'http_user_agent'	=> $_SERVER['HTTP_USER_AGENT']
    			)
    	);

    	return parent::onDispatch( $e );
    }

    public function forgotpasswordAction(){
    	$db = $this->getServiceLocator()->get('my_index_db_adapter');
    	$form = new ForgotpasswordForm('Forgotpassword',$db);
    	$request = $this->getRequest();

    	if( $request->isPost($request->getPost()) ){

    		$form->setData($request->getPost());

    		if($form->isValid($request->getPost())){

    			$email       = $request->getPost()->email;
				$newpassword = rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9).rand(1,9);

				$sm = $this->getServiceLocator();

				$this->memberTable = $sm->get('Index\Model\MemberTable');
				$member_id = $this->memberTable->getId( $email );
				$member    = $this->memberTable->getMember($member_id);
				$md5_password = md5( $newpassword );
				$member->set_password( $md5_password );
				$this->memberTable->saveMember($member);


				$this->itemTable = $sm->get('Index\Model\ItemTable');

				$from 		= $this->itemTable->get_Item_text('forgotpassword_email','from');
				$subject 	= $this->itemTable->get_Item_text('forgotpassword_email','subject');
				$content 	= $this->itemTable->get_Item_text('forgotpassword_email','content');
				$content    = $content.$newpassword;

				$message = new Message();
				$message->addTo( $email )
				->addFrom($from)
				->setSubject($subject)
				->setBody($content);

				// Setup SMTP transport using LOGIN authentication

				$config = $sm->get('config');
				$smtp   = $config['smtp'];

				$transport = new SmtpTransport();
				$options   = new SmtpOptions(array(
						
						'name'              => $smtp['name'],
						'host'              => $smtp['name'],
						'connection_class'  => 'login',
						'connection_config' => array(
								'ssl'       => $smtp['ssl'],
								'username'  => $smtp['username'],
								'password'  => $smtp['password'],
								
						),
						'port' 				=> $smtp['port'],
				));
				$transport->setOptions($options);
				$transport->send($message);

				return $this->redirect()->toRoute('index',array(
						'controller'=> 'index',
						'action'  	=> 'newpasswordsent',
				));

    		}

    	}

    	return array('form' => $form);
    }

    public function newpasswordsentAction(){

    }

	public function indexAction(){
		
		
		
		
			
					
		$db = $this->getServiceLocator()->get('my_index_db_adapter');
		$form = new SigninForm('Signin',$db);

		$request = $this->getRequest();
		if( $request->isPost($request->getPost()) ){

			$form->setData($request->getPost());

			if($form->isValid($request->getPost())){

				$sm = $this->getServiceLocator();
				$this->memberTable = $sm->get('Index\Model\MemberTable');

				$this->itemTable = $sm->get('Index\Model\ItemTable');

				$timeout = $this->itemTable->get_Item_value('session','timeout'); // In seconds

				$fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);

				$user_session = new Container('user');

				$user_session->email = $request->getPost()->email;
				$user_session->id    = $this->memberTable->getId( $request->getPost()->email );		
				$user_session->last_active = time();
				$user_session->fingerprint = $fingerprint;
				$user_session->isloggedin = true;
		
				$member = $this->memberTable->getMember($user_session->id);				
				$member = new Member($member);
				
				$expired = strtotime( $member->get_expired_date() );
				$now = strtotime("now");
								
				if ( $now - $expired ){
					$user_session->expired = 1;
				}else{
					$user_session->expired = 0;					
				} 

				$user_session->tab 			='property_tab'; // always property_tab for first version

				$propertyTable = $sm->get('Index\Model\PropertyTable');
				$rowset    = $propertyTable->getMemberProperties( $user_session->id );
				if( count($rowset) !=0 ){
					$property = $rowset->current();
					$user_session->property_id 	= $property->get_property_id();
					
				}else{
					$user_session->property_id 	= 0;
				
				}
				
				return $this->redirect()->toRoute('member');


			}

		}

		return array('form' => $form);
	}

	public function aboutusAction(){
		
	}

	public function contactusAction(){

	}

	public function privacypolicyAction(){

	}

	public function termsAction(){

	}


	public function signupAction(){

		$db = $this->getServiceLocator()->get('my_index_db_adapter');
		$form = new SignupForm('Signup',$db);

		$member = new Member();

		$request = $this->getRequest();
		if( $request->isPost() ){


			//$member = new Member();


			$form->setData( $request->getPost() );

			if($form->isValid( $request->getPost() )){

				// pull data from form to member object

				$sm = $this->getServiceLocator();

				$member = new Member();
				$member->exchangeArray($form->getData());
				$md5_password = md5( $member->get_password() );
				$member->set_password( $md5_password );
				$hash = md5( rand(0,1000) );
				$member->set_hash($hash);
				$member->set_active(0);
				
				// set default value
				
				$member->set_property_default_squarefeet('1000');
				$member->set_property_default_lotsize('2000');
				$member->set_property_default_yearbuilt('1980');
				$member->set_property_default_lastremodel('2000');
				$member->set_property_default_parking('1');
				$member->set_property_default_listingprice('100000');
				$member->set_property_default_initimprove('0');
				$member->set_property_default_purchaseprice('0');
				
				$member->set_property_default_first_loanamount('5');
				$member->set_property_default_first_loantype('25');
				$member->set_property_default_first_interestrate('5');
				$member->set_property_default_first_payment_frequency('23');
				$member->set_property_default_first_term('30');
				$member->set_property_default_first_interestonly_term('1');
				
				$member->set_property_default_second_loanamount('100');
				$member->set_property_default_second_loantype('25');
				$member->set_property_default_second_interestrate('5');
				$member->set_property_default_second_payment_frequency('23');
				$member->set_property_default_second_term('30');
				$member->set_property_default_second_interestonly_term('1');
				
				$member->set_property_default_vacancy_rate('8');
				$member->set_property_default_rent_mode('28');
				$member->set_property_default_singlevalue_rent('500');
				$member->set_property_default_singlevalue_rent_frequency('31');
				$member->set_property_default_appreciation_rate('5');
				$member->set_property_default_incomeinflation_rate('3');
				$member->set_property_default_expenseinflation_rate('3');
				$member->set_property_default_lvt_for_refinance('3');
				$member->set_property_default_compounding_period('32');
				
				// save member
				$this->memberTable = $sm->get('Index\Model\MemberTable');
				$member_id = $this->memberTable->saveMember($member);



				$this->itemTable = $sm->get('Index\Model\ItemTable');

				$from 		= $this->itemTable->get_Item_text('confirm_email','from');
				$subject 	= $this->itemTable->get_Item_text('confirm_email','subject');
				$content 	= $this->itemTable->get_Item_text('confirm_email','content');

				$content    = $content.'http://'.$_SERVER['HTTP_HOST'].'/index/confirmmember?email='.$member->get_email().'&hash='.$hash;
				$message = new Message();
				$message->addTo($member->get_email())
						->addFrom($from)
						->setSubject($subject)
						->setBody($content);

				// Setup SMTP transport using LOGIN authentication

				$config = $sm->get('config');
				$smtp   = $config['smtp'];

				
				
				
				
				$transport = new SmtpTransport();
				
				
				$options   = new SmtpOptions(array(
						
						'name'              => $smtp['name'],
						'host'              => $smtp['name'],
						'connection_class'  => 'login',
						'connection_config' => array(
								'ssl'       => $smtp['ssl'],
								'username'  => $smtp['username'],
								'password'  => $smtp['password'],
								
						),
						'port' 				=> $smtp['port'],
				));
				
				/*
				
				$options   = new SmtpOptions(array(
						'name'              => $smtp['name'],
						'host'              => $smtp['host'],
						'connection_class'  => 'login',
						'connection_config' => array(
								'username'  => $smtp['username'],
								'password'  => $smtp['password'],
						),
				));
				*/
				$transport->setOptions($options);
				$transport->send($message);


				/// set session
				$this->itemTable = $sm->get('Index\Model\ItemTable');

				$timeout = $this->itemTable->get_Item_value('session','timeout'); // In seconds

				$fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);

				$user_session = new Container('user');

				$user_session->email = $request->getPost()->email;
				$user_session->id    = $this->memberTable->getId( $request->getPost()->email );
				$user_session->last_active = time();
				$user_session->fingerprint = $fingerprint;
				$user_session->isloggedin = true;
				$user_session->expired = 0;



				return $this->redirect()->toRoute('index',array(
										'controller'=> 'index',
										'action'  	=> 'welcome',
										));

			}
		}
		return array('form' => $form);
	}

	public function welcomeAction(){
		//$id = $this->getEvent()->getRouteMatch()->getParam('id');
	}



	public function confirmmemberAction(){

		if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])){ 
			// Verify data
			$email = addslashes($_GET['email']); // Set email variable
			$hash = addslashes($_GET['hash']); // Set hash variable
			$sm = $this->getServiceLocator();
			$this->memberTable = $sm->get('Index\Model\MemberTable');
			//echo $email;
			//echo $hash;
			if( $this->memberTable->isMemberExist($email,$hash) ){

				$member_id = $this->memberTable->getId($email);
				$member    = $this->memberTable->getMember($member_id);


				if(!$member->is_active()){ // not yet active
					$member->set_active(1);
					$member_id = $this->memberTable->saveMember($member);
					
					// add member subscriptions
					
					$member_subscription = new Membersubscription();
					$member_subscription->set_member_id($member_id);
															
					$membershiptype = $sm->get('Index\Model\Membership_typeTable')->getMembershiptype('1'); // set default to free membership which is '1'
					$membershiptype = new Membershiptype( $membershiptype );
										
					$member_subscription->set_membership_type_id( $membershiptype->get_membership_type_id() );
					$member_subscription->set_purchased_date( date('Y-m-d H:i:s') );
					$member_subscription->set_start_date( date('Y-m-d H:i:s') );
					$member_subscription->set_stop_date('2100-01-01 00:00:00');
					$member_subscription->set_num_of_properties( $membershiptype->get_num_of_properties() );
					$member_subscription->set_num_of_email_per_month( $membershiptype->get_num_of_email_per_month() );
					$member_subscription->set_next_membership_type_id( 1 ); // for free membership
					$member_subscription_id = $sm->get('Index\Model\Member_subscriptionTable')->saveMembersubscription( $member_subscription );
					
					// add member monthly tokens
					
					$member_monthly_token = new Membermonthlytoken();
					$member_monthly_token->set_member_id( $member_subscription->get_member_id() );
					$member_monthly_token->set_membership_type_id( $member_subscription->get_membership_type_id() );
					$member_monthly_token->set_start_date( date('Y-m-d H:i:s') );
					
					// adding one month that does not exceed the end of next month
					$date = new \DateTime(date('Y-m-d H:i:s'));
					$start_day = $date->format('j');					
					$date->modify("+1 month");
					$end_day = $date->format('j');										
					if ($start_day != $end_day){
						$date->modify('last day of last month');
					}
					
					$next_month = $date->format('Y-m-d H:i:s');					
					$member_monthly_token->set_stop_date( $next_month );
					$member_monthly_token->set_num_of_properties( $member_subscription->get_num_of_properties() );
					$member_monthly_token->set_num_of_email_per_month( $member_subscription->get_num_of_email_per_month() );
					$member_monthly_token->set_used_num_of_email_per_month( '0' );
					
					$member_monthly_token_id = $sm->get('Index\Model\Member_monthly_tokenTable')->saveMembermonthlytoken( $member_monthly_token );
					
					

					// add default member buyingcost items
					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Loan Fee');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Recording Fee');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Inspection/Survey');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Title Insurance');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Taxes');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Escrow/Attorney');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					$memberbuyingcostitem = new Memberbuyingcostitem();
					$memberbuyingcostitem->set_buyingcost_name('Appraisal');
					$memberbuyingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_buyingcost_itemTable')->saveMemberbuyingcostitem($memberbuyingcostitem);

					// add default member sellingcost items

					$membersellingcostitem = new Membersellingcostitem();
					$membersellingcostitem->set_sellingcost_name('Settlement Fee');
					$membersellingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_sellingcost_itemTable')->saveMembersellingcostitem($membersellingcostitem);

					$membersellingcostitem = new Membersellingcostitem();
					$membersellingcostitem->set_sellingcost_name('Commission');
					$membersellingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_sellingcost_itemTable')->saveMembersellingcostitem($membersellingcostitem);

					$membersellingcostitem = new Membersellingcostitem();
					$membersellingcostitem->set_sellingcost_name('Taxes');
					$membersellingcostitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_sellingcost_itemTable')->saveMembersellingcostitem($membersellingcostitem);

					// add default member other income items

					$memberincomeitem = new Memberincomeitem();
					$memberincomeitem->set_income_name('Laundry');
					$memberincomeitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_income_itemTable')->saveMemberincomeitem($memberincomeitem);

					$memberincomeitem = new Memberincomeitem();
					$memberincomeitem->set_income_name('Parking');
					$memberincomeitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_income_itemTable')->saveMemberincomeitem($memberincomeitem);

					$memberincomeitem = new Memberincomeitem();
					$memberincomeitem->set_income_name('Storage');
					$memberincomeitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_income_itemTable')->saveMemberincomeitem($memberincomeitem);

					$memberincomeitem = new Memberincomeitem();
					$memberincomeitem->set_income_name('Others');
					$memberincomeitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_income_itemTable')->saveMemberincomeitem($memberincomeitem);

					// add default member other expense items

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Advertising');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Auto & Travel');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Cleaning&Maintenance');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Commissions');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Insurance');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Legal&Professional Fees');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Management Fees');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Repairs');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Supplies');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Taxes');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('Utilities');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);


					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('HOA');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);

					$memberexpenseitem = new Memberexpenseitem();
					$memberexpenseitem->set_expense_name('PMI');
					$memberexpenseitem->set_member_id($member_id);
					$sm->get('Index\Model\Member_expense_itemTable')->saveMemberexpenseitem($memberexpenseitem);
					
					


					// create folders

					//folder for cover photo
					system('mkdir ./public/img/members/member_id_'.$member_id);
					//folder for full page photos
					system('mkdir ./public/img/members/member_id_'.$member_id.'/fullpage_photos');
					//folder for videos
					system('mkdir ./public/videos/members/member_id_'.$member_id);




					$message = 'Your account is activated, we value your feedbacks, feel free to let us know for anything we can do to make this product better together';
					$result  = 1;
				}else if( $member->is_active()){ // already active
					$message = 'Invalid url or account has already been activated';
					$result  = 0;
				}

			}else{
				$message = 'Your email is not autorized user';
				$result  = 0;
			}
		}else{ 
			// Invalid approach
			$message = 'Invalid url';
			$result  = 0;
		}

		return array('result'  => $result,
				     'message' => $message);
	}








































}
