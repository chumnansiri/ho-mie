<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Index\Model\Propertymemberexpenseitem;

use Zend\View\Model\JsonModel;


class OtherexpenseController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	public function indexajaxAction() {

		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->memberexpenseitemTable = $sm->get('Index\Model\Member_expense_itemTable');


		$memberexpenseitems 		= $this->memberexpenseitemTable->getMemberexpenseitems($user_session->id);


		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->PropertyTable->getProperty( $user_session->property_id );
		$property_otherexpense_items = $property->get_other_expense_items();

		$view = new ViewModel(array(
								'memberexpenseitems' 			=> $memberexpenseitems,
								'propertyotherexpenseitems'		=> $property_otherexpense_items

							));
		$view->setTerminal(true);
		return $view;

	}

	public function addotherexpenseitemajaxAction(){

		$user_session = new Container('user');

		$otherexpense_item_id 		= $_POST['otherexpense_item_id'];
		$otherexpense_item_amount    = $_POST['otherexpense_item_amount'];
		$otherexpense_intem_frequency= $_POST['otherexpense_item_frequency'];

		$sm = $this->getServiceLocator();

		$this->Property_buyingcost_itemTable = $sm->get('Index\Model\Property_member_expense_itemTable');

		$property_member_expense_item = new Propertymemberexpenseitem();

		$property_member_expense_item->set_property_id($user_session->property_id);
		$property_member_expense_item->set_expense_id($otherexpense_item_id);
		$property_member_expense_item->set_amount($otherexpense_item_amount);
		$property_member_expense_item->set_frequency($otherexpense_intem_frequency);

		$property_member_expense_item_id = $this->Property_buyingcost_itemTable->saveProperty_member_expense_item($property_member_expense_item);


		$this->PropertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->PropertyTable->getProperty($user_session->property_id);

		$expense_items = $property->get_other_expense_items();

		$i=0;
		foreach($expense_items as $expense_item){
			foreach($expense_item as $column => $value){
				$output[$i][$column] = $value;
			}
			$i++;
		}
		$JsonObj = new JsonModel(
									$output
								);
		return $JsonObj;

	}

	public function deleteotherexpenseitemajaxAction(){
		$user_session 				 = new Container('user');
		$property_expense_id     	 = $_POST['property_expense_id'];

		$sm = $this->getServiceLocator();
		$this->Property_otherexpense_itemTable = $sm->get('Index\Model\Property_member_expense_itemTable');

		$this->Property_otherexpense_itemTable->deleteProperty_member_expense_item($property_expense_id);

		return $this->response;
	}




}
