<?php

namespace Index\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

use Index\Model\Memberdefaultsellingcostitem;

class MemberdefaultsellingcostController extends AbstractActionController
{
	/**
	 * The default action - show the home page
	 */

	public function indexajaxAction() {
		
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		
		$member_sellingcost_Table 		= $sm->get('Index\Model\Member_sellingcost_itemTable');
		$memberTable 					= $sm->get('Index\Model\MemberTable');
		
		$member 						= $memberTable->getMember( $user_session->id );

		$sellingcost_items 				= $member_sellingcost_Table->getMembersellingcostitems($user_session->id);
		$member_default_sellingcost_items=$member->get_default_sellingcost_items();

		

		$view = new ViewModel(array(
				'sellingcost_items' 				=> $sellingcost_items,
				'member_default_sellingcost_items' 	=> $member_default_sellingcost_items
		));
		$view->setTerminal(true);
		return $view;

	}

	public function addsellingcostitemajaxAction(){
		
		$user_session = new Container('user');
		
		$sellingcost_item_amount = $_POST['sellingcost_item_amount'];
		$sellingcost_item_id     = $_POST['sellingcost_item_id'];
		
		$sm = $this->getServiceLocator();
		$this->Member_default_sellingcost_itemTable = $sm->get('Index\Model\Member_default_sellingcost_itemTable');
		
		$member_default_sellingcost_item = new Memberdefaultsellingcostitem();
		
		$member_default_sellingcost_item->set_member_id($user_session->id);
		$member_default_sellingcost_item->set_sellingcost_id($sellingcost_item_id);
		$member_default_sellingcost_item->set_member_default_sellingcost_item_amount($sellingcost_item_amount);
		
		$member_default_sellingcost_item_id = $this->Member_default_sellingcost_itemTable->saveMember_default_sellingcost_item( $member_default_sellingcost_item );
		
		$this->MemberTable = $sm->get('Index\Model\MemberTable');
		$member = $this->MemberTable->getMember($user_session->id);
		$member_default_sellingcost_items = $member->get_default_sellingcost_items();
		
		$view = new ViewModel(array(
				'member_default_sellingcost_items' => $member_default_sellingcost_items
		));
		$view->setTerminal(true);
		
		return $view;
		

	}

	public function deletesellingcostitemajaxAction(){

		$user_session 						= new Container('user');
		$member_default_sellingcost_item_id  = $_POST['member_default_sellingcost_item_id'];
		
		$sm = $this->getServiceLocator();
		$this->Member_default_sellingcost_itemTable = $sm->get('Index\Model\Member_default_sellingcost_itemTable');
		
		$this->Member_default_sellingcost_itemTable->deleteMember_default_sellingcost_item($member_default_sellingcost_item_id);
		
		$this->MemberTable = $sm->get('Index\Model\MemberTable');
		$member = $this->MemberTable->getMember( $user_session->id );
		$member_default_sellingcost_items = $member->get_default_sellingcost_items();
		
		$view = new ViewModel(array(
				'member_default_sellingcost_items' => $member_default_sellingcost_items
		));
		$view->setTerminal(true);
		
		return $view;
		

	}



}
