<?php
namespace Index\Controller;


use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

use Index\Model\Property;
use Index\Model\Member;

use Index\Model\Propertybuyingcostitem;
use Index\Model\Propertysellingcostitem;


class PropertyController extends AbstractActionController
{

	private $properties;

	public function __construct(){


	}

	public function onDispatch( \Zend\Mvc\MvcEvent $e )
	{


			return parent::onDispatch( $e );
	}



	public function testAction(){



		$view = new ViewModel();
		$view->setTerminal(true);
		return $view;

	}

	function objectToArray( $object ) {
		if( !is_object( $object ) && !is_array( $object ) ) {
			return $object;
		}
		if( is_object( $object ) ) {
			$object = (array) $object;
		}
		return array_map( 'objectToArray', $object );
	}


	public function getpropertytabajaxAction(){

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);


		$JsonObj = new JsonModel(array(
				'property_name' 	=> $property->get_property_name(),
				'address' 			 => array(
						'street'	 		=>	$property->get_property_street(),
						'city'		 		=>  $property->get_property_city(),
						'state'		 		=>  $property->get_property_state(),
						'zip'		 		=>  $property->get_property_zip(),
						'country'	 		=>  $property->get_property_country(),
				),
				'basic_info'   		 => array(
						'mls'		 		=>  $property->get_property_mls(),
						'style'		 		=>  $property->get_property_style(),
						'squarefeet' 		=>  $property->get_property_squarefeet(),
						'lotsize'	 		=>  $property->get_property_lotsize(),
						'yearbuilt'	 		=>  $property->get_property_yearbuilt(),
						'lastremodel'		=>  $property->get_property_lastremodel(),
						'parking'	 		=>  $property->get_property_parking(),
						'hoa'	     		=>  $property->get_property_hoa(),
				),

				'purchase_info' 	 => array(
						'listingprice'		=>  $property->get_property_listingprice(),
						'buyingcost'    	=>  $property->get_buyingcost(),
						'init_improve'  	=>  $property->get_property_initimprove(),
						'init_costs'    	=>  $property->get_initialcost(),
						'purchase_price'	=>  $property->get_property_purchaseprice(),
						'firstmortgage' 	=>  $property->get_firstmortgage_amount(),
						'secondmortgage'	=>  $property->get_secondmortgage_amount(),
						'down_payment'  	=>  $property->get_downpayment(),
						'init_cash_invested'=>  $property->get_initialcash_invested(),
				),

				'gross_rent_info'	 => array(
						'vacancy_rate'      		=>  $property->get_property_vacancy_rate(),
						'rent_mode'         		=>  $property->get_property_rent_mode(),
						'singlevalue_rent'  		=>  $property->get_property_singlevalue_rent(),
						'sum_of_units_permonth'     =>  $property->get_property_sum_of_units_permonth(),
						'singlevalue_rent_frequency'=>  $property->get_property_singlevalue_rent_frequency(),
						'otherincome'               =>  $property->get_property_otherincome()
				),

				'expense_info'   	 => array(
						'expense_table'				=>  $property->get_other_expense_items()
				),

				'projection_info'    => array(
						'appreciation_rate'			=>  $property->get_property_appreciation_rate(),
						'incomeinflation_rate'		=>  $property->get_property_incomeinflation_rate(),
						'expenseinflation_rate'     =>  $property->get_property_expenseinflation_rate(),
						'sellingcost'               =>  $property->get_sellingcost(),
						'lvtforrefinance'           =>  $property->get_property_lvt_for_refinance(),
						'compounding_period'        =>  $property->get_property_compounding_period()
				),

		));

		return $JsonObj;
	}

	public function getpropertyphotoajaxAction(){
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$fullpage_photos = $property->get_fullpage_photos();
		$fullpage_photos_array = array();
		foreach($fullpage_photos as $fullpage_photo){
			$fullpage_photos_array[] = array(
												'fullpage_photo_id'   => $fullpage_photo->get_fullpage_photo_id(),
												'fullpage_photo_path' => $fullpage_photo->get_fullpage_photo_path(),

											);
		}

		$JsonObj = new JsonModel(
									array(
											'property_name'     => $property->get_property_name(),
											'property_id'       => $user_session->property_id,
											'cover_photo' 		=> $property->get_property_cover_photo_path(),
											'fullpage_photos'	=> $fullpage_photos_array,
									)
							    );

		return $JsonObj;
	}

	public function getpropertyvideoajaxAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property_videos = $property->get_property_videos();
		$property_videos_array = array();
		foreach($property_videos as $property_video){
			$property_videos_array[] = array(
												'property_video_id'		=> $property_video->get_property_video_id(),
												'video_path'			=> $property_video->get_video_path(),
											);
		}

		$JsonObj = new JsonModel(
									array(
											'property_name'     => $property->get_property_name(),
											'property_videos'	=> $property_videos_array
										 )
								);

		return $JsonObj;

	}

	public function getpropertyamortizationajaxAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$JsonObj = new JsonModel(
				array(
						'property_name'     => $property->get_property_name(),
						'property_address'  => $property->get_property_street().' '.$property->get_property_city().' '.$property->get_property_state().' '.$property->get_property_zip(),
						'first_loan_report' => $property->get_mortgage_amortization_report(1), // first mortgage
						'second_loan_report'=> $property->get_mortgage_amortization_report(2), // second mortgage


				)
		);

		return $JsonObj;
	}

	public function getpropertyreportajaxAction(){
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);


		$fullpage_photos = $property->get_fullpage_photos();
		$fullpage_photos_array = array();
		foreach($fullpage_photos as $fullpage_photo){
			$fullpage_photos_array[] = array(
												'fullpage_photo_id'   => $fullpage_photo->get_fullpage_photo_id(),
												'fullpage_photo_path' => $fullpage_photo->get_fullpage_photo_path(),

											);
		}

		$JsonObj = new JsonModel(array(

				'cover_page' 			 => array(
						'property_name'     =>  $property->get_property_name(),
						'street'	 		=>	$property->get_property_street(),
						'city'		 		=>  $property->get_property_city(),
						'state'		 		=>  $property->get_property_state(),
						'zip'		 		=>  $property->get_property_zip(),
						'country'	 		=>  $property->get_property_country(),
						'cover_photo'       =>  $property->get_property_cover_photo_path()
				),

				'overview_page'			 => array(
						'basic_info'    	=> array(
								'mls'    		=>	$property->get_property_mls(),
								'style'			=>  $property->get_property_style(),
								'square_feet'	=>  $property->get_property_squarefeet(),
								'lot_size'      =>  $property->get_property_lotsize(),
								'year_built'    =>  $property->get_property_yearbuilt(),
								'last_remodel'  =>  $property->get_property_lastremodel(),
								'parking'		=>  $property->get_property_parking(),
								'hoa'			=>  $property->get_property_hoa(),
						),
						'mortgages_page'	=> array(
								'loan_to_cost1' =>  $property->get_property_loan_to_cost1(),
								'loan_to_cost2' =>  $property->get_property_loan_to_cost2(),
								'loan_to_value1'=>  $property->get_property_loan_to_value1(),
								'loan_to_value2'=>  $property->get_property_loan_to_value2(),
								'loan_amount1'	=>  $property->get_actual_loan_amount1(),
								'loan_amount2'	=>  $property->get_actual_loan_amount2(),
								'loan_type1'  	=>  $property->get_property_first_loantype_text(),
								'loan_type2'  	=>  $property->get_property_second_loantype_text(),
								'term1'  		=>  $property->get_property_first_term(),
								'term2'  		=>  $property->get_property_second_term(),
								'interest1'  	=>  $property->get_property_first_interestrate(),
								'interest2'  	=>  $property->get_property_second_interestrate(),
								'payment1'      =>  $property->get_payment1(),
								'payment2'      =>  $property->get_payment2(),

						),
						'financial_metrics'	=> array(
								'grm'           =>  $property->get_gross_rent_multipier(),
								'oer'           =>  $property->get_operating_expense_ratio(),
								'dcr'           =>  $property->get_debt_coverage_ratio(),
								'cap_rate'      =>  $property->get_cap_rate(),
								'cash_on_cash_r'=>  $property->get_cash_on_cash_return(),
						),
						'assumptions'		=> array(
								'appre_rate'	=>  $property->get_property_appreciation_rate(),
								'vacancy_rate'  =>  $property->get_property_vacancy_rate(),
								'in_inflat_rate'=>  $property->get_property_incomeinflation_rate(),
								'ex_inflat_rate'=>  $property->get_property_expenseinflation_rate(),
								'lvt'           =>  $property->get_property_lvt_for_refinance(),
								'selling_cost'  =>  $property->get_sellingcost(),
						),
						'purchase'			=> array(
								'purchase_price'=>  $property->get_property_purchaseprice(),
								'mortgage1'     =>  $property->get_actual_loan_amount1(),
								'mortgage2'     =>  $property->get_actual_loan_amount2(),
								'downpayment'   =>  $property->get_downpayment(),
								'buying_cost'   =>  $property->get_buyingcost(),
								'init_improve'  =>  $property->get_property_initimprove(),
								'init_cash_invested'=>  $property->get_initialcash_invested(),
								'cost_p_sqr'    =>  $property->get_cost_per_squarfeet(),

						),
						'income' 			=> array(
								'gross_rent_m'  =>  $property->get_gross_rent_permonth(),
								'gross_rent_y'  =>  $property->get_gross_rent_peryear(),
								'vacancy_m'		=>  $property->get_vacancy_loss_permonth(),
								'vacancy_y'		=>  $property->get_vacancy_loss_peryear(),
								'opt_income_m'  =>  $property->get_operating_income_permonth(),
								'opt_income_y'  =>  $property->get_operating_income_peryear(),
								'income_table'  =>  $property->get_income_table(),

						),
						'expenses'			=> array(
								'expenses_table'=>  $property->get_expenses_table(),
								'opt_expense_m' =>  $property->get_property_otherexpense()/12,
								'opt_expense_y' =>  $property->get_property_otherexpense(),
						),
						'net_performance'   => array(
								'net_opt_income_m'=>  $property->get_net_operation_income_permonth(),
								'net_opt_income_y'=>  $property->get_net_operation_income_peryear(),
								'mortgage_m'      =>  $property->get_mortgage_permonth(),
								'mortgage_y'      =>  $property->get_mortgage_peryear(),
								'cashflow_m'      =>  $property->get_cashflow_permonth(),
								'cashflow_y'      =>  $property->get_cashflow_peryear(),
						),

				),
 				'projection'    => $property->get_projection(),
 				'rent_summary'  => array(
 									'unit'   =>   $property->get_units_info(),
 				//					'rent'   =>   $property->get_rent_projection()
 								   ),
 				'buying_cost_details' =>     $property->get_buyingcost_items(),
 				'selling_cost_details'=>     $property->get_sellingcost_items(),

 				'fullpage_photos'     =>     $fullpage_photos_array,

		));



		return $JsonObj;
	}

	public function getpropertycompareajaxAction(){

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();

		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$this->memberTable   = $sm->get('Index\Model\MemberTable');
		$member = $this->memberTable->getMember($user_session->id);

		$member = new Member($member);

    	$properties = $member->get_properties();

		$property_outputs='';

 		foreach($properties as $property){

			$property_outputs[] = array(
											'property_id' 	=> $property->get_property_id(),
											'property_name'	=> $property->get_property_name()
									   );

		}

		$JsonObj = new JsonModel(array(
										'property_list'		=> $property_outputs
									  )
								);

		return $JsonObj;

	}

	public function getpropertycomparecolumnajaxAction(){

		$user_session = new Container('user');



		$sm = $this->getServiceLocator();
		$property_id = $_POST['property_id'];

		if($_POST['column'] == 'first'){

			$user_session->first_column_property_id = $_POST['property_id'];

		}else if($_POST['column'] == 'second'){

			$user_session->second_column_property_id = $_POST['property_id'];

		}else if($_POST['column'] == 'third'){

			$user_session->third_column_property_id = $_POST['property_id'];

		}else if($_POST['column'] == 'fourth'){

			$user_session->fourth_column_property_id = $_POST['property_id'];

		}


		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		$property = $this->propertyTable->getProperty($_POST['property_id']);
		$property = new Property( $property);

		$JsonObj = new JsonModel(array(
										'property_image'    	=> $property->get_property_cover_photo_path(),
										'property_name'			=> $property->get_property_name(),
										'mls'               	=> $property->get_property_mls(),
										'squarefeet' 			=> $property->get_property_squarefeet(),
										'lotsize' 				=> $property->get_property_lotsize(),
										'yearbuilt'         	=> $property->get_property_yearbuilt(),
										'parking' 				=> $property->get_property_parking(),

 										'loan_to_cost1'     	=> $property->get_property_loan_to_cost1(),
				                        'loan_to_value1'    	=> $property->get_property_loan_to_value1(),
				                        'loan_amount1'      	=> $property->get_actual_loan_amount1(),
										'loan_term1'        	=> $property->get_property_first_term(),
										'intrate1'          	=> $property->get_property_first_interestrate(),
										'payment1'          	=> $property->get_payment1(),

										'loan_to_cost2'     	=> $property->get_property_loan_to_cost2(),
										'loan_to_value2'    	=> $property->get_property_loan_to_value2(),
										'loan_amount2'      	=> $property->get_actual_loan_amount2(),
										'loan_term2'        	=> $property->get_property_second_term(),
										'intrate2'          	=> $property->get_property_second_interestrate(),
										'payment2'          	=> $property->get_payment2(),

										'grm'               	=> $property->get_gross_rent_multipier(),
										'operatingexpense_ratio'=> $property->get_operating_expense_ratio(),
				                        'dcr' 					=> $property->get_debt_coverage_ratio(),
										'caprate' 				=> $property->get_cap_rate(),
										'cash_on_cash_return'   => $property->get_cash_on_cash_return(),

 										'purchaseprice' 		=> $property->get_property_purchaseprice(),
 										'downpayment'           => $property->get_downpayment(),

 										'buyingcost'  		    =>  $property->get_buyingcost(),
 										'initimprove'           =>  $property->get_property_initimprove(),
 										'initinvested'          =>  $property->get_initialcash_invested(),
 										'costpersquarefoot'     =>  $property->get_cost_per_squarfeet(),

 										'grossrent'             =>  $property->get_gross_rent_permonth(),
 										'vacancyloss'			=>  $property->get_vacancy_loss_permonth(),
 										'income_table'  		=>  $property->get_income_table(),
 										'operatingincome' 		=>  $property->get_operating_income_permonth(),

 										'operatingexpenses'     =>  $property->get_property_otherexpense()/12,

 										'netoperatingincome'    =>  $property->get_net_operation_income_permonth(),
 										'mortgagepayment'       =>  $property->get_mortgage_permonth(),
 										'cashflow'              =>  $property->get_cashflow_permonth(),
									  )
								);
		
		return $JsonObj;

	}



	public function updatestreetajaxAction(){

		    $street = $_POST['street'];
			$user_session = new Container('user');

			$sm = $this->getServiceLocator();
			$this->propertyTable = $sm->get('Index\Model\PropertyTable');

            $property = $this->propertyTable->getProperty($user_session->property_id);
			$property = new Property( $property);
			$property->set_property_street($street);

			$this->propertyTable->saveProperty($property);
			return $this->response;
	}

	public function updatecityajaxAction(){

			$city = $_POST['city'];
			$user_session = new Container('user');

			$sm = $this->getServiceLocator();
			$this->propertyTable = $sm->get('Index\Model\PropertyTable');

			$property = $this->propertyTable->getProperty($user_session->property_id);
			$property = new Property( $property);
			$property->set_property_city($city);

			$this->propertyTable->saveProperty($property);
			return $this->response;
	}

	public function updatestateajaxAction(){

			$state = $_POST['state'];
			$user_session = new Container('user');

			$sm = $this->getServiceLocator();
			$this->propertyTable = $sm->get('Index\Model\PropertyTable');

			$property = $this->propertyTable->getProperty($user_session->property_id);
			$property = new Property( $property);
			$property->set_property_state($state);

			$this->propertyTable->saveProperty($property);
			return $this->response;
	}

	public function updatezipajaxAction(){

		$zip = $_POST['zip'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_zip($zip);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatecountryajaxAction(){

		$country = $_POST['country'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_country($country);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatemlsajaxAction(){

		$mls = $_POST['mls'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_mls($mls);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatestyleajaxAction(){

		$style = $_POST['style'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_style($style);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatesquarefeetajaxAction(){

		$squarefeet = $_POST['squarefeet'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_squarefeet($squarefeet);

		$this->propertyTable->saveProperty($property);
		return $this->response;

	}

	public function updatelotsizeajaxAction(){

		$lotsize = $_POST['lotsize'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_lotsize($lotsize);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updateyearbuiltajaxAction(){

		$yearbuilt = $_POST['yearbuilt'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_yearbuilt($yearbuilt);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatelastremodelajaxAction(){

		$lastremodel = $_POST['lastremodel'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_lastremodel($lastremodel);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updateparkingajaxAction(){

		$parking = $_POST['parking'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_parking($parking);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatehoaajaxAction(){

		$hoa = $_POST['hoa'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_hoa($hoa);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatelistingpriceajaxAction(){

		$listingprice = $_POST['listingprice'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_listingprice($listingprice);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updateinitimproveajaxAction(){

		$initimprove = $_POST['initimprove'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_initimprove($initimprove);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatepurchasepriceajaxAction(){

		$purchaseprice = $_POST['purchaseprice'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');

		$property = $this->propertyTable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_purchaseprice($purchaseprice);

		$this->propertyTable->saveProperty($property);
		return $this->response;
	}

	public function updatefirstmortgateajaxAction(){

		$loanamount 		= $_POST['percent'];
		$loantype   		= $_POST['loan_type'];
		$interesterate 		= $_POST['interested_rate'];
		$payment_frequency  = $_POST['payment_mode'];
		$term               = $_POST['loan_term'];
		$interestonly_term  = $_POST['int_only_term'];

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property->set_property_first_loanamount( $loanamount );
		$property->set_property_first_loantype( $loantype );
		$property->set_property_first_interestrate( $interesterate );
		$property->set_property_first_payment_frequency( $payment_frequency );
		$property->set_property_first_term( $term );
		$property->set_property_first_interestonly_term( $interestonly_term );

		$propertytable->saveProperty($property);
		return $this->response;

	}

	public function updatesecondmortgateajaxAction(){

		$loanamount 		= $_POST['percent'];
		$loantype   		= $_POST['loan_type'];
		$interesterate 		= $_POST['interested_rate'];
		$payment_frequency  = $_POST['payment_mode'];
		$term               = $_POST['loan_term'];
		$interestonly_term  = $_POST['int_only_term'];

		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);

		$property->set_property_second_loanamount( $loanamount );
		$property->set_property_second_loantype( $loantype );
		$property->set_property_second_interestrate( $interesterate );
		$property->set_property_second_payment_frequency( $payment_frequency );
		$property->set_property_second_term( $term );
		$property->set_property_second_interestonly_term( $interestonly_term );

		$propertytable->saveProperty($property);
		return $this->response;

	}

	public function updatevacancyrateajaxAction(){
		$vacancy_rate 		= $_POST['vacancyrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_vacancy_rate($vacancy_rate);
		$propertytable->saveProperty($property);
		return $this->response;

	}

	public function updatefixamountajaxAction(){
		$fix_amount 		= $_POST['fix_amount'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_singlevalue_rent($fix_amount);
		$propertytable->saveProperty($property);
		return $this->response;

	}

	public function updatesinglevaluefreqajaxAction(){
		$singlevalue_freq 		= $_POST['singlevalue_freq'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_singlevalue_rent_frequency($singlevalue_freq);
		$propertytable->saveProperty($property);
		return $this->response;

	}

	public function updaterentmodeajaxAction(){
		$rent_mode 		= $_POST['rent_mode'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_rent_mode($rent_mode);
		$propertytable->saveProperty($property);
		return $this->response;
	}

	public function updateappreciationrateajaxAction(){
		$appreciationrate 		= $_POST['appreciationrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_appreciation_rate($appreciationrate);
		$propertytable->saveProperty($property);
		return $this->response;
	}

	public function updateincomeinflationrateajaxAction(){
		$incomeinflationrate 		= $_POST['incomeinflationrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_incomeinflation_rate($incomeinflationrate);
		$propertytable->saveProperty($property);
		return $this->response;
	}

	public function updateexpenseinflationrateajaxAction(){
		$expenseinflationrate 		= $_POST['expenseinflationrate'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_expenseinflation_rate($expenseinflationrate);
		$propertytable->saveProperty($property);
		return $this->response;
	}

	public function updatecompoundingperiodajaxAction(){
		$compounding_period 		= $_POST['compounding_period'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_compounding_period($compounding_period);
		$propertytable->saveProperty($property);
		return $this->response;

	}

	public function updatelvtforrefinanceajaxAction(){
		$lvtforrefinance 		= $_POST['lvtforrefinance'];
		$user_session = new Container('user');
		$sm = $this->getServiceLocator();
		$propertytable = $sm->get('Index\Model\PropertyTable');

		$property = $propertytable->getProperty($user_session->property_id);
		$property = new Property( $property);
		$property->set_property_lvt_for_refinance($lvtforrefinance);
		$propertytable->saveProperty($property);
		return $this->response;

	}



	public function addingpropertyajaxAction(){


		$property_name = $_POST['property_name'];
		$user_session = new Container('user');

		$sm = $this->getServiceLocator();

		$this->memberTable = $sm->get('Index\Model\MemberTable');
		$member = $this->memberTable->getMember( $user_session->id );
		$member = new Member( $member );
		
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		

		$this->member_default_buyingcost_itemTable = $sm->get('Index\Model\Member_default_buyingcost_itemTable');
		
		$this->property_buyingcost_itemTable = $sm->get('Index\Model\Property_buyingcost_itemTable');
		
		$this->member_default_sellingcost_itemTable = $sm->get('Index\Model\Member_default_sellingcost_itemTable');
		
		$this->property_sellingcost_itemTable = $sm->get('Index\Model\Property_sellingcost_itemTable');

		

		$property = new Property();

		$property->set_property_name($property_name);

		$property->set_member_id($user_session->id);

		$property->set_property_street('');

		$property->set_property_city('');

		$property->set_property_state('');

		$property->set_property_zip('');
		
		$property->set_property_mls( $member->get_property_default_mls() );
		
		$property->set_property_style( $member->get_property_default_style() );
		
		$property->set_property_squarefeet( $member->get_property_default_squarefeet() );
		
		$property->set_property_lotsize( $member->get_property_default_lotsize() );
		
		$property->set_property_yearbuilt( $member->get_property_default_yearbuilt() );
		
		$property->set_property_lastremodel( $member->get_property_default_lastremodel() );
		
		$property->set_property_parking( $member->get_property_default_parking() );
		
		$property->set_property_listingprice( $member->get_property_default_listingprice() );
		

		$property->set_property_purchaseprice( $member->get_property_default_purchaseprice() );
		

		// first mortgage
		$property->set_property_first_loanamount( $member->get_property_default_first_loanamount() );
		
		$property->set_property_first_loantype( $member->get_property_default_first_loantype() );
		
		$property->set_property_first_interestrate( $member->get_property_default_first_interestonly_term() );
		
		$property->set_property_first_payment_frequency( $member->get_property_default_first_payment_frequency() );
		
		$property->set_property_first_term( $member->get_property_default_first_term() );
		
		$property->set_property_first_interestonly_term( $member->get_property_default_first_interestonly_term() );
		
		// second mortgage
		$property->set_property_second_loanamount( $member->get_property_default_second_loanamount() );
		
		$property->set_property_second_loantype( $member->get_property_default_second_loantype() );
		
		$property->set_property_second_interestrate( $member->get_property_default_second_interestonly_term() );
		
		$property->set_property_second_payment_frequency( $member->get_property_default_second_payment_frequency() );
		
		$property->set_property_second_term( $member->get_property_default_second_term() );
		
		$property->set_property_second_interestonly_term( $member->get_property_default_second_interestonly_term() );
		
		$property->set_property_initimprove( $member->get_property_default_initimprove() );
		
		//
		$property->set_property_rent_mode( $member->get_property_default_rent_mode() );
		
		$property->set_property_singlevalue_rent( $member->get_property_default_singlevalue_rent() );
		
		$property->set_property_singlevalue_rent_frequency( $member->get_property_default_singlevalue_rent_frequency() );
		
		$property->set_property_vacancy_rate( $member->get_property_default_vacancy_rate() );
		
		//
		$property->set_property_appreciation_rate( $member->get_property_default_appreciation_rate() );
		 
		$property->set_property_incomeinflation_rate( $member->get_property_default_incomeinflation_rate() );
		
		$property->set_property_expenseinflation_rate( $member->get_property_default_expenseinflation_rate() );
		
		$property->set_property_lvt_for_refinance( $member->get_property_default_lvt_for_refinance() );
		
		$property->set_property_compounding_period( $member->get_property_default_compounding_period() );
		
		$property_id = $this->propertyTable->saveProperty($property);

			
		// load property default buying cost
		
		$property_default_buyingcost_items = $this->member_default_buyingcost_itemTable->getMember_default_buyingcost_item_by_member_id( $user_session->id );
		
		foreach( $property_default_buyingcost_items as $property_default_buyingcost_item )
		{
			$buyingcost_item = new Propertybuyingcostitem();
			
			$buyingcost_item->set_property_id($property_id);
			
			$buyingcost_item->set_buyingcost_id( $property_default_buyingcost_item->get_buyingcost_id() );
			
			$buyingcost_item->set_property_buyingcost_item_amount( $property_default_buyingcost_item->get_member_default_buyingcost_item_amount() );
						
			$this->property_buyingcost_itemTable->saveProperty_buyingcost_item( $buyingcost_item );
			
		}
		
		// load property default selling cost
		
		$property_default_sellingcost_items = $this->member_default_sellingcost_itemTable->getMember_default_sellingcost_item_by_member_id( $user_session->id );
		
		foreach( $property_default_sellingcost_items as $property_default_sellingcost_item )
		{
			$sellingcost_item = new Propertysellingcostitem();

			$sellingcost_item->set_property_id($property_id);
			
			$sellingcost_item->set_sellingcost_id( $property_default_sellingcost_item->get_sellingcost_id() );
			
			$sellingcost_item->set_property_sellingcost_item_amount( $property_default_sellingcost_item->get_member_default_sellingcost_item_amount() );
						
			$this->property_sellingcost_itemTable->saveProperty_sellingcost_item( $sellingcost_item );
			
		}


		if( $property_id != 0){  // new property for a member

			$view = new ViewModel(
									array(
										'property_name' => $property_name,
										'property_id'   => $property_id
									)
								 );

			// Disable layouts; `MvcEvent` will use this View Model instead
			$view->setTerminal(true);
			return $view;

		}else{   // dupllicate name $property_id = 0

			echo 0;
			return $this->response;

		}


	}

	public function delettingfullpagephotoajaxAction(){
		$user_session = new Container('user');
		$fullpage_photo_id = $_POST['fullpage_photo_id'];
		$sm = $this->getServiceLocator();
		$Fullpage_photo_Table = $sm->get('Index\Model\FullpagephotoTable');
		$Fullpage_photo_Table->deleteFullpagephoto($fullpage_photo_id);
		system('rm ./public/img/members/member_id_'.$user_session->id.'/fullpage_photos/fullpage_photo_id_'.$fullpage_photo_id.'.*');
		return $this->response;
	}

	public function delettingpropertyvideoajaxAction(){
		$user_session = new Container('user');
		$property_video_id = $_POST['property_video_id'];
		$sm = $this->getServiceLocator();
		$Property_video_Table = $sm->get('Index\Model\PropertyvideoTable');
		$Property_video_Table->deletePropertyvideo($property_video_id);
		system('rm ./public/videos/members/member_id_'.$user_session->id.'/property_video_id_'.$property_video_id.'.*');
		return $this->response;
	}

	public function delettingpropertyajaxAction(){

		$user_session = new Container('user');
		$property_id = $_POST['property_id'];

		$sm = $this->getServiceLocator();
		
		$this->propertyTable = $sm->get('Index\Model\PropertyTable');
		
		$this->property_buyingcost_itemTable = $sm->get('Index\Model\Property_buyingcost_itemTable');
		
		$this->property_sellingcost_itemTable = $sm->get('Index\Model\Property_sellingcost_itemTable');
		
		$this->property_income_itemTable = $sm->get('Index\Model\Property_member_income_itemTable');
		
		$this->property_expense_itemTable = $sm->get('Index\Model\Property_member_expense_itemTable');

		$property = $this->propertyTable->getProperty($property_id);
		
		

		$property = new Property($property);

		$fullpage_photos = $property->get_fullpage_photos();

		foreach($fullpage_photos as $fullpage_photo){
			$fullpage_photo_id = $fullpage_photo->get_fullpage_photo_id();
			system('rm ./public/img/members/member_id_'.$user_session->id.'/fullpage_photos/fullpage_photo_id_'.$fullpage_photo_id.'.*');
			$sm->get('Index\Model\FullpagephotoTable')->deleteFullpagephoto($fullpage_photo_id);
		}

		system('rm ./public/img/members/member_id_'.$user_session->id.'/cover_photo_id_'.$property_id.'.*');
		
		$this->propertyTable->deleteProperty($property_id);

		$this->property_buyingcost_itemTable->deleteProperty_buyingcost_item_by_property_id( $property_id );
		
		$this->property_sellingcost_itemTable->deleteProperty_sellingcost_item_by_property_id( $property_id );
		
		$this->property_income_itemTable->deleteProperty_member_income_item_by_property_id($property_id);
		
		$this->property_expense_itemTable->deleteProperty_member_expense_item_by_property_id($property_id);

		// to set new default selected property to the first one or none
		$rowset    = $this->propertyTable->getMemberProperties( $user_session->id );
		if( count($rowset) !=0 ){
			$property = $rowset->current();
			$user_session->property_id 	= $property->get_property_id();
		}else{
			$user_session->property_id 	= 0;
		}

		return $this->response;

	}

}
