<?php
namespace Index\Model;

// Add these import statements

class Propertymemberincomeitem{

	private $property_income_id;
	private $property_id;
	private $income_id;
	private $amount;
	private $frequency;
	private $sort_by;
	private $created_date;
	private $last_modified;


	public function __construct(){

	}

	public function exchangeArray($data)
    {
        $this->property_income_id    		=    (	isset(  $data['property_income_id']) 	) ? $data['property_income_id'] : null;
        $this->property_id                  =    (	isset(  $data['property_id']) 			) ? $data['property_id'] 		: null;
		$this->income_id                    =    (	isset(  $data['income_id']) 			) ? $data['income_id'] 			: null;
		$this->amount                       =    (	isset(  $data['amount']) 				) ? $data['amount'] 	 		: null;
		$this->frequency                    =     (	isset(  $data['frequency']) 			) ? $data['frequency'] 	 		: null;
		$this->sort_by      				=    (	isset(  $data['sort_by']) 				) ? $data['sort_by'] 			: null;
        $this->created_date      			=    (	isset(  $data['created_date']) 			) ? $data['created_date'] 		: null;
        $this->last_modified      			=    (	isset(  $data['last_modified']) 		) ? $data['last_modified'] 		: null;
    }


    public function get_property_income_id(){
		return $this->property_income_id;
    }

    public function get_property_id(){
    	return $this->property_id;
    }

    public function get_income_id(){
    	return $this->income_id;
    }

    public function get_amount(){
    	return $this->amount;
    }

    public function get_frequency(){
    	return $this->frequency;
    }
    public function get_sort_by(){
    	return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_property_income_id($property_income_id){
    	$this->property_income_id = $property_income_id;
    }
    public function set_property_id($property_id){
    	$this->property_id = $property_id;
    }
    public function set_income_id($income_id){
    	$this->income_id = $income_id;
    }
	public function set_amount($amount){
		$this->amount = $amount;
	}
	public function set_frequency($frequency){
		$this->frequency = $frequency;
	}
	public function set_sort_by($sort_by){
		$this->sort_by = $sort_by;
	}
	public function set_created_date($created_date){
		$this->created_date = $created_date;
	}
	public function set_last_modified($last_modified){
		$this->last_modified = $last_modified;
	}








}
