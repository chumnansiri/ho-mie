<?php
namespace Index\Model;

// Add these import statements

class Member{

	private $member_id;
	private $email;
	private $password;
	private $hash;
	private $member_type_id;
	private $active;
	private $expired_date;
	
	// for default value
	private $property_default_mls;
	private $property_default_style;
	private $property_default_squarefeet;
	private $property_default_lotsize;
	private $property_default_yearbuilt;
	private $property_default_lastremodel;
	private $property_default_parking;
	private $property_default_listingprice;
	private $property_default_initimprove;
	private $property_default_purchaseprice;
	private $property_default_first_loanamount;
	private $property_default_first_loantype;
	private $property_default_first_interestrate;
	private $property_default_first_payment_frequency;
	private $property_default_first_term;
	private $property_default_first_interestonly_term;
	private $property_default_second_loanamount;
	private $property_default_second_loantype;
	private $property_default_second_interestrate;
	private $property_default_second_payment_frequency;
	private $property_default_second_term;
	private $property_default_second_interestonly_term;
	private $property_default_vacancy_rate;
	private $property_default_rent_mode;
	private $property_default_singlevalue_rent;
	private $property_default_singlevalue_rent_frequency;
	private $property_default_appreciation_rate;
	private $property_default_incomeinflation_rate;
	private $property_default_expenseinflation_rate;
	private $property_default_lvt_for_refinance;
	private $property_default_compounding_period;
	
	
	private $sort_by;
	private $created_date;
	private $last_modified;

	private $properties;

	private $income_items;
	private $expense_items;

	private $buyingcost_items;
	private $sellingcost_items;
	
	private $default_buyingcost_items;
	private $default_sellingcost_items;



	public function __construct(Member $member=null){
		if($member){

			$this->member_id 	  								= $member->get_member_id();
			$this->email          								= $member->get_email();
			$this->password       								= $member->get_password();
			$this->hash           								= $member->get_hash();
			$this->member_type_id 								= $member->get_member_type_id();
			$this->active         								= $member->is_active();
			$this->expired_date     							= $member->get_expired_date();
			// for default value
			$this->property_default_mls     					= $member->get_property_default_mls();
			$this->property_default_style   					= $member->get_property_default_style();
			$this->property_default_squarefeet					= $member->get_property_default_squarefeet();
			$this->property_default_lotsize						= $member->get_property_default_lotsize();
			$this->property_default_yearbuilt					= $member->get_property_default_yearbuilt();
			$this->property_default_lastremodel					= $member->get_property_default_lastremodel();
			$this->property_default_parking                     = $member->get_property_default_parking();
			$this->property_default_listingprice				= $member->get_property_default_listingprice();
			$this->property_default_initimprove					= $member->get_property_default_initimprove();
			$this->property_default_purchaseprice				= $member->get_property_default_purchaseprice();
			$this->property_default_first_loanamount			= $member->get_property_default_first_loanamount();
			$this->property_default_first_loantype				= $member->get_property_default_first_loantype();			
			$this->property_default_first_interestrate			= $member->get_property_default_first_interestrate();
			$this->property_default_first_payment_frequency		= $member->get_property_default_first_payment_frequency();
			$this->property_default_first_term					= $member->get_property_default_first_term();
			$this->property_default_first_interestonly_term     = $member->get_property_default_first_interestonly_term();
			$this->property_default_second_loanamount 			= $member->get_property_default_second_loanamount();
			$this->property_default_second_loantype				= $member->get_property_default_second_loantype();
			$this->property_default_second_interestrate 		= $member->get_property_default_second_interestrate();			
			$this->property_default_second_payment_frequency    = $member->get_property_default_second_payment_frequency();
			$this->property_default_second_term 				= $member->get_property_default_second_term();
			$this->property_default_second_interestonly_term    = $member->get_property_default_second_interestonly_term();
			$this->property_default_vacancy_rate				= $member->get_property_default_vacancy_rate();
			$this->property_default_rent_mode					= $member->get_property_default_rent_mode();
			$this->property_default_singlevalue_rent			= $member->get_property_default_singlevalue_rent();
			$this->property_default_singlevalue_rent_frequency  = $member->get_property_default_singlevalue_rent_frequency();
			$this->property_default_appreciation_rate 			= $member->get_property_default_appreciation_rate();
			$this->property_default_incomeinflation_rate 		= $member->get_property_default_incomeinflation_rate();
			$this->property_default_expenseinflation_rate 		= $member->get_property_default_expenseinflation_rate();
			$this->property_default_lvt_for_refinance			= $member->get_property_default_lvt_for_refinance();
			$this->property_default_compounding_period 			= $member->get_property_default_compounding_period();
		
			$this->sort_by        								= $member->get_sort_by();
			$this->created_date   								= $member->get_created_date();
			$this->last_modified  								= $member->get_last_modified();
			$this->properties     								= $member->get_properties();
			$this->income_items   								= $member->get_income_items();
			$this->expense_items  								= $member->get_expense_items();
			$this->buyingcost_items 							= $member->get_buyingcost_items();
			$this->sellingcost_items							= $member->get_sellingcost_items();
			
			$this->default_buyingcost_items                     = $member->get_default_buyingcost_items();
			$this->default_sellingcost_items					= $member->get_default_sellingcost_items();

		}
	}

	public function exchangeArray($data)
    {
        $this->member_id     								=    (		isset(  $data['member_id']) 									) ? 	$data['member_id'] 									: 	null;
        $this->email      									=    (		isset(  $data['email']) 										) ? 	$data['email'] 			    						: 	null;
        $this->password      								=    (		isset(  $data['password']) 										) ? 	$data['password'] 									: 	null;
        $this->hash                 						=    (      isset(  $data['hash'])                  						) ?     $data['hash']               						:   null;
        $this->member_type_id       						=    (		isset(  $data['member_type_id']) 								) ? 	$data['member_type_id'] 							: 	null;
        $this->active               						=    (      isset(  $data['active'])                						) ?     $data['active']             						:   null;
        $this->expired_date         						=    (      isset(  $data['expired_date'])          						) ?     $data['expired_date']       						:   null;
     
        $this->property_default_mls     					= 	 (		isset(  $data['property_default_mls']) 							) ? 	$data['property_default_mls'] 						: 	null;
        $this->property_default_style   					= 	 (		isset(  $data['property_default_style']) 						) ? 	$data['property_default_style'] 					: 	null;
        $this->property_default_squarefeet					= 	 (		isset(  $data['property_default_squarefeet']) 					) ? 	$data['property_default_squarefeet'] 				: 	null;
        $this->property_default_lotsize						= 	 (		isset(  $data['property_default_lotsize']) 						) ? 	$data['property_default_lotsize'] 					: 	null;
        $this->property_default_yearbuilt					= 	 (		isset(  $data['property_default_yearbuilt']) 					) ? 	$data['property_default_yearbuilt'] 				: 	null;
        $this->property_default_lastremodel					= 	 (		isset(  $data['property_default_lastremodel']) 					) ? 	$data['property_default_lastremodel'] 				: 	null;
        $this->property_default_parking                     = 	 (		isset(  $data['property_default_parking']) 						) ? 	$data['property_default_parking'] 					: 	null;
        $this->property_default_listingprice				= 	 (		isset(  $data['property_default_listingprice']) 				) ? 	$data['property_default_listingprice'] 				: 	null;
        $this->property_default_initimprove					= 	 (		isset(  $data['property_default_initimprove']) 					) ? 	$data['property_default_initimprove'] 				: 	null;
        $this->property_default_purchaseprice				= 	 (		isset(  $data['property_default_purchaseprice']) 				) ? 	$data['property_default_purchaseprice'] 			: 	null;
        $this->property_default_first_loanamount			= 	 (		isset(  $data['property_default_first_loanamount']) 			) ? 	$data['property_default_first_loanamount'] 			: 	null;
        $this->property_default_first_loantype				= 	 (		isset(  $data['property_default_first_loantype']) 				) ? 	$data['property_default_first_loantype'] 			: 	null;
        $this->property_default_first_interestrate			= 	 (		isset(  $data['property_default_first_interestrate']) 			) ? 	$data['property_default_first_interestrate'] 		: 	null;
        $this->property_default_first_payment_frequency		= 	 (		isset(  $data['property_default_first_payment_frequency']) 		) ? 	$data['property_default_first_payment_frequency'] 	: 	null;
        $this->property_default_first_term					= 	 (		isset(  $data['property_default_first_term']) 					) ? 	$data['property_default_first_term'] 				: 	null;
        $this->property_default_first_interestonly_term     = 	 (		isset(  $data['property_default_first_interestonly_term']) 		) ? 	$data['property_default_first_interestonly_term'] 	: 	null;
        $this->property_default_second_loanamount 			= 	 (		isset(  $data['property_default_second_loanamount']) 			) ? 	$data['property_default_second_loanamount'] 		: 	null;
        $this->property_default_second_loantype				= 	 (		isset(  $data['property_default_second_loantype']) 				) ? 	$data['property_default_second_loantype'] 			: 	null;
        $this->property_default_second_interestrate 		= 	 (		isset(  $data['property_default_second_interestrate']) 			) ? 	$data['property_default_second_interestrate'] 		: 	null;
        $this->property_default_second_payment_frequency    = 	 (		isset(  $data['property_default_second_payment_frequency']) 	) ? 	$data['property_default_second_payment_frequency'] 	: 	null;
        $this->property_default_second_term 				= 	 (		isset(  $data['property_default_second_term']) 					) ? 	$data['property_default_second_term'] 				: 	null;
        $this->property_default_second_interestonly_term    = 	 (		isset(  $data['property_default_second_interestonly_term']) 	) ? 	$data['property_default_second_interestonly_term'] 	: 	null;
        $this->property_default_vacancy_rate				= 	 (		isset(  $data['property_default_vacancy_rate']) 				) ? 	$data['property_default_vacancy_rate'] 				: 	null;
        $this->property_default_rent_mode					= 	 (		isset(  $data['property_default_rent_mode']) 					) ? 	$data['property_default_rent_mode'] 				: 	null;
        $this->property_default_singlevalue_rent			= 	 (		isset(  $data['property_default_singlevalue_rent']) 			) ? 	$data['property_default_singlevalue_rent'] 			: 	null;
        $this->property_default_singlevalue_rent_frequency  = 	 (		isset(  $data['property_default_singlevalue_rent_frequency']) 	) ? 	$data['property_default_singlevalue_rent_frequency']: 	null;
        $this->property_default_appreciation_rate 			= 	 (		isset(  $data['property_default_appreciation_rate']) 			) ? 	$data['property_default_appreciation_rate'] 		: 	null;
        $this->property_default_incomeinflation_rate 		= 	 (		isset(  $data['property_default_incomeinflation_rate']) 		) ? 	$data['property_default_incomeinflation_rate'] 		: 	null;
        $this->property_default_expenseinflation_rate 		= 	 (		isset(  $data['property_default_expenseinflation_rate']) 		) ? 	$data['property_default_expenseinflation_rate'] 	: 	null;
        $this->property_default_lvt_for_refinance			= 	 (		isset(  $data['property_default_lvt_for_refinance']) 			) ? 	$data['property_default_lvt_for_refinance'] 		: 	null;
        $this->property_default_compounding_period 			= 	 (		isset(  $data['property_default_compounding_period']) 			) ? 	$data['property_default_compounding_period'] 		: 	null;
        
        
        $this->sort_by      								=    (		isset(  $data['sort_by']) 										) ? 	$data['sort_by'] 									: 	null;
        $this->created_date      							=    (		isset(  $data['created_date']) 									) ? 	$data['created_date'] 								: 	null;
        $this->last_modified      							=    (		isset(  $data['last_modified']) 								) ? 	$data['last_modified'] 								: 	null;

    }


    public function get_member_id(){
		return $this->member_id;
    }

    public function get_email(){
		return $this->email;
    }

    public function get_password(){
		return $this->password;
    }

    public function get_hash(){
    	return $this->hash;
    }

    public function get_member_type_id(){
		return $this->member_type_id;
    }

    public function is_active(){
    	return $this->active;
    }

    public function is_expired(){

    	$date = date('Y-m-d H:i:s');



    	return $this->expired_date;
    }

    public function get_expired_date(){
    	return $this->expired_date;
    }
    
	////////// default value /////////////
	
    public function get_property_default_mls(){
    	return $this->property_default_mls;	
    }
   
    public function get_property_default_style(){
    	return $this->property_default_style;
    }
    
    public function get_property_default_squarefeet(){
    	return $this->property_default_squarefeet;
    }
    
    public function get_property_default_lotsize(){
    	return $this->property_default_lotsize;
    }
    
    public function get_property_default_yearbuilt(){
    	return $this->property_default_yearbuilt;
    }
   
    public function get_property_default_lastremodel(){
    	return $this->property_default_lastremodel;
    }
    
    public function get_property_default_parking(){
    	return $this->property_default_parking;
    }
    
    public function get_property_default_listingprice(){
    	return $this->property_default_listingprice;
    }
   
    public function get_property_default_initimprove(){
    	return $this->property_default_initimprove;
    }
    
    public function get_property_default_purchaseprice(){
    	return $this->property_default_purchaseprice;
    }
    
    public function get_property_default_first_loanamount(){
    	return $this->property_default_first_loanamount;
    }
    
    public function get_property_default_firstmortgage_amount(){
    	
    	
    	$default_firstmortgage_amount = $this->get_property_default_purchaseprice() - ( $this->get_property_default_purchaseprice() * ( $this->property_default_first_loanamount / 100) );
    	
    	return $default_firstmortgage_amount;
    }
    
    public function get_property_default_first_loantype(){
    	return $this->property_default_first_loantype;
    }
    
    public function get_property_default_first_interestrate(){
    	return $this->property_default_first_interestrate;
    }
   
    public function get_property_default_first_payment_frequency(){
    	return $this->property_default_first_payment_frequency;
    }
   
    public function get_property_default_first_term(){
    	return $this->property_default_first_term;
    }
    
    public function get_property_default_first_interestonly_term(){
    	return $this->property_default_first_interestonly_term;
    }
    
    public function get_property_default_second_loanamount(){
    	return $this->property_default_second_loanamount;
    }
    
    public function get_property_default_secondmortgage_amount(){
    	 
    	 
    	$default_secondmortgage_amount = $this->get_property_default_purchaseprice() - ( $this->get_property_default_purchaseprice() * ( $this->property_default_second_loanamount / 100) );
    	 
    	return $default_secondmortgage_amount;
    }
    
    public function get_property_default_second_loantype(){
    	return $this->property_default_second_loantype;
    }
    
    public function get_property_default_second_interestrate(){
    	return $this->property_default_second_interestrate;
    }
   
    public function get_property_default_second_payment_frequency(){
    	return $this->property_default_second_payment_frequency;
    }
    
    public function get_property_default_second_term(){
    	return $this->property_default_second_term;
    }
    
    public function get_property_default_second_interestonly_term(){
    	return $this->property_default_second_interestonly_term;
    }
    
    public function get_default_downpayment(){
    	
    	$default_downpayment = $this->get_property_default_purchaseprice() - $this->get_property_default_firstmortgage_amount() - $this->get_property_default_secondmortgage_amount();
    	return $default_downpayment;
    	
    }
    
   
    
    public function get_property_default_vacancy_rate(){
    	return $this->property_default_vacancy_rate;
    }
   
    public function get_property_default_rent_mode(){
    	return $this->property_default_rent_mode;
    }
    
    public function get_property_default_singlevalue_rent(){
    	return $this->property_default_singlevalue_rent;
    }
    
    public function get_property_default_singlevalue_rent_frequency(){
    	return $this->property_default_singlevalue_rent_frequency;
    }
    
    public function get_property_default_appreciation_rate(){
    	return $this->property_default_appreciation_rate;
    }
    
    public function get_property_default_incomeinflation_rate(){
    	return $this->property_default_incomeinflation_rate;
    }
    
    public function get_property_default_expenseinflation_rate(){
    	return $this->property_default_expenseinflation_rate;
    }
    
    public function get_property_default_lvt_for_refinance(){
    	return $this->property_default_lvt_for_refinance;
    }
    
    public function get_property_default_compounding_period(){
    	return $this->property_default_compounding_period;
    }
    
    ////////// default value /////////////
    
    public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    public function get_properties(){
    	return $this->properties;
    }

    public function get_income_items(){
    	return $this->income_items;
    }

    public function get_expense_items(){
    	return $this->expense_items;
    }

    public function get_buyingcost_items(){
    	return $this->buyingcost_items;
    }

    public function get_sellingcost_items(){
    	return $this->sellingcost_items;
    }
    		  
    public function get_default_buyingcost_items(){
    	return $this->default_buyingcost_items;
    }
    
    public function get_default_sellingcost_items(){
    	return $this->default_sellingcost_items;
    }
    ////////////////////////////////

    public function set_member_id($member_id){
    	$this->member_id = $member_id;
    }

    public function set_email($email){
    	$this->email  = $email;
    }

    public function set_password($password){
    	$this->password  = $password;
    }

    public function set_hash($hash){
    	$this->hash = $hash;
    }

    public function set_member_type_id($member_type_id){
    	$this->member_type_id = $member_type_id;
    }

    public function set_active($active){
    	$this->active = $active;
    }

    public function set_expired_date($expired_date){
    	$this->expired_date = $expired_date;
    }

    ////////// default value /////////////
    
    public function set_property_default_mls( $property_default_mls ){
    	 $this->property_default_mls = $property_default_mls;
    }
     
    public function set_property_default_style( $property_default_style ){
    	 $this->property_default_style = $property_default_style;
    }
    
    public function set_property_default_squarefeet( $property_default_squarefeet ){
    	 $this->property_default_squarefeet = $property_default_squarefeet;
    }
    
    public function set_property_default_lotsize( $property_default_lotsize ){
    	 $this->property_default_lotsize = $property_default_lotsize;
    }
    
    public function set_property_default_yearbuilt( $property_default_yearbuilt ){
    	 $this->property_default_yearbuilt = $property_default_yearbuilt;
    }
     
    public function set_property_default_lastremodel( $property_default_lastremodel ){
    	 $this->property_default_lastremodel = $property_default_lastremodel;
    }
    
    public function set_property_default_parking( $property_default_parking ){
    	 $this->property_default_parking = $property_default_parking;
    }
    
    public function set_property_default_listingprice( $property_default_listingprice ){
    	 $this->property_default_listingprice = $property_default_listingprice;
    }
     
    public function set_property_default_initimprove( $property_default_initimprove ){
    	 $this->property_default_initimprove = $property_default_initimprove;
    }
    
    public function set_property_default_purchaseprice( $property_default_purchaseprice ){
    	 $this->property_default_purchaseprice = $property_default_purchaseprice;
    }
    
    public function set_property_default_first_loanamount( $property_default_first_loanamount ){
    	 $this->property_default_first_loanamount = $property_default_first_loanamount;
    }
    
    public function set_property_default_first_loantype( $property_default_first_loantype ){
    	 $this->property_default_first_loantype = $property_default_first_loantype;
    }
    
    public function set_property_default_first_interestrate( $property_default_first_interestrate ){
    	 $this->property_default_first_interestrate = $property_default_first_interestrate;
    }
     
    public function set_property_default_first_payment_frequency( $property_default_first_payment_frequency ){
    	 $this->property_default_first_payment_frequency = $property_default_first_payment_frequency;
    }
     
    public function set_property_default_first_term( $property_default_first_term ){
    	 $this->property_default_first_term = $property_default_first_term;
    }
    
    public function set_property_default_first_interestonly_term( $property_default_first_interestonly_term ){
    	 $this->property_default_first_interestonly_term =$property_default_first_interestonly_term;
    }
    
    public function set_property_default_second_loanamount( $property_default_second_loanamount ){
    	 $this->property_default_second_loanamount = $property_default_second_loanamount;
    }
    
    public function set_property_default_second_loantype( $property_default_second_loantype ){
    	 $this->property_default_second_loantype = $property_default_second_loantype;
    }
    
    public function set_property_default_second_interestrate( $property_default_second_interestrate ){
    	 $this->property_default_second_interestrate = $property_default_second_interestrate;
    }
     
    public function set_property_default_second_payment_frequency( $property_default_second_payment_frequency ){
    	 $this->property_default_second_payment_frequency = $property_default_second_payment_frequency;
    }
    
    public function set_property_default_second_term( $property_default_second_term ){
    	 $this->property_default_second_term = $property_default_second_term;
    }
    
    public function set_property_default_second_interestonly_term( $property_default_second_interestonly_term ){
    	 $this->property_default_second_interestonly_term = $property_default_second_interestonly_term;
    }
    
    public function set_property_default_vacancy_rate( $property_default_vacancy_rate ){
    	 $this->property_default_vacancy_rate = $property_default_vacancy_rate;;
    }
     
    public function set_property_default_rent_mode( $property_default_rent_mode ){
    	 $this->property_default_rent_mode = $property_default_rent_mode;
    }
    
    public function set_property_default_singlevalue_rent( $property_default_singlevalue_rent ){
    	 $this->property_default_singlevalue_rent = $property_default_singlevalue_rent;
    }
    
    public function set_property_default_singlevalue_rent_frequency( $property_default_singlevalue_rent_frequency ){
    	 $this->property_default_singlevalue_rent_frequency = $property_default_singlevalue_rent_frequency;
    }
    
    public function set_property_default_appreciation_rate( $property_default_appreciation_rate ){
    	 $this->property_default_appreciation_rate = $property_default_appreciation_rate;
    }
    
    public function set_property_default_incomeinflation_rate( $property_default_incomeinflation_rate ){
    	 $this->property_default_incomeinflation_rate = $property_default_incomeinflation_rate;
    }
    
    public function set_property_default_expenseinflation_rate( $property_default_expenseinflation_rate ){
    	 $this->property_default_expenseinflation_rate = $property_default_expenseinflation_rate;
    }
    
    public function set_property_default_lvt_for_refinance( $property_default_lvt_for_refinance ){
    	 $this->property_default_lvt_for_refinance = $property_default_lvt_for_refinance;
    }
    
    public function set_property_default_compounding_period( $property_default_compounding_period ){
    	 $this->property_default_compounding_period = $property_default_compounding_period;
    }
    
    ////////// default value /////////////
    
    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }

    public function set_properties($properties){
    	$this->properties = $properties;
    }

    public function set_income_items($income_items){
    	$this->income_items = $income_items;
    }

    public function set_expense_items($expense_items){
    	$this->expense_items = $expense_items;
    }

    public function set_buyingcost_items($buyingcost_items){
    	$this->buyingcost_items = $buyingcost_items;
    }

    public function set_sellingcost_items($sellingcost_items){
    	$this->sellingcost_items = $sellingcost_items;
    }

    public function set_default_buyingcost_items( $default_buyingcost_items ){
       	$this->default_buyingcost_items = $default_buyingcost_items;    	
    }
    
    public function set_default_sellingcost_items( $default_sellingcost_items ){
    	$this->default_sellingcost_items = $default_sellingcost_items;
    	
    }
    
   

    ////////////////////////////////
    // below are abstract value
    
    public function get_property_default_buyingcost(){
    	$default_buyingcost = 0;
    	
     	$default_buyingcost_items = $this->default_buyingcost_items;
            
    
      	foreach( (array)$default_buyingcost_items as $default_buyingcost_item)
      	{
      		$default_buyingcost += $default_buyingcost_item['member_default_buyingcost_item_amount'];
    
      	}
    	
    	return  $default_buyingcost;
    
    }
    
    public function get_property_default_sellingcost(){
    	$default_sellingcost = 0;
    	 
    	$default_sellingcost_items = $this->default_sellingcost_items;
    
    
    	foreach( (array)$default_sellingcost_items as $default_sellingcost_item)
    	{
    		$default_sellingcost += $default_sellingcost_item['member_default_sellingcost_item_amount'];
    
    	}
    	 
    	return  $default_sellingcost;
    
    }
    
    public function get_property_default_initialcost(){
    	
    	$default_initialcost = $this->get_property_default_initimprove() + $this->get_property_default_buyingcost();
    	return $default_initialcost;
    }
    
    public function get_property_default_initialcash_invested(){
    	$default_intialcash_invested = $this->get_property_default_initialcost() + $this->get_default_downpayment();
    	return $default_intialcash_invested;
    }
    
}
