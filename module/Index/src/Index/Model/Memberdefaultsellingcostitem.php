<?php

namespace Index\Model;

class Memberdefaultsellingcostitem {
	
	private $member_default_sellingcost_item_id;
	private $member_id;
	private $sellingcost_id;
	private $member_default_sellingcost_item_amount;
	private $sort_by;
	private $created_date;
	private $last_modified;
	
	public function __construct(){
	
	}
	
	public function exchangeArray($data)// all $data element need to be match with table's column name
	{
		$this->member_default_sellingcost_item_id     	=    (	isset(  $data['member_default_sellingcost_item_id'])   	)? $data['member_default_sellingcost_item_id'] 		: null;
		$this->member_id     							=    (	isset(  $data['member_id']) 					  	  	)? $data['member_id'] 				 				: null;
		$this->sellingcost_id     						=    (	isset(  $data['sellingcost_id']) 				  	  	)? $data['sellingcost_id'] 		 					: null;
		$this->member_default_sellingcost_item_amount  	=    (	isset(  $data['member_default_sellingcost_item_amount']))? $data['member_default_sellingcost_item_amount'] 	: null;
		$this->sort_by      							=    (	isset(  $data['sort_by']) 						  		)? $data['sort_by'] 								: null;
		$this->created_date      						=    (	isset(  $data['created_date']) 					  		)? $data['created_date'] 							: null;
		$this->last_modified      						=    (	isset(  $data['last_modified']) 				  		)? $data['last_modified'] 							: null;
	}
	

	public function get_member_default_sellingcost_item_id(){
		return $this->member_default_sellingcost_item_id;
	}
	
	public function get_member_id(){
		return $this->member_id;
	}
	
	public function get_sellingcost_id(){
		return $this->sellingcost_id;
	}
	
	public function get_member_default_sellingcost_item_amount(){
		return $this->member_default_sellingcost_item_amount;		     
	}
	
	public function get_sort_by(){
		return $this->sort_by;
	}
	
	public function get_created_date(){
		return $this->created_date.'-';
	}
	
	public function get_last_modified(){
		return $this->last_modified;
	}
	
	//////////////////////////
	
	public function set_member_default_sellingcost_item_id($member_default_sellingcost_item_id){
		$this->member_default_sellingcost_item_id = $member_default_sellingcost_item_id;
	}
	
	public function set_member_id($member_id){
		$this->member_id = $member_id;
	}
	
	public function set_sellingcost_id($sellingcost_id){
		$this->sellingcost_id = $sellingcost_id;
	}
	
	public function set_member_default_sellingcost_item_amount($member_default_sellingcost_item_amount){
		$this->member_default_sellingcost_item_amount = $member_default_sellingcost_item_amount;
	}
	
	public function set_sort_by($sort_by){
		$this->sort_by = $sort_by;
	}
	
	public function set_created_date($created_date){
		$this->created_date = $created_date;
	}
	
	public function set_last_modified($last_modified){
		$this->last_modified = $last_modified;
	}
	
	
}

?>