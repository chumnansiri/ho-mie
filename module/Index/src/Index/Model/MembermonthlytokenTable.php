<?php

namespace Index\Model;


use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Index\Model\Membermonthlytoken;

class MembermonthlytokenTable {
	
	protected $membermonthlytoken_tableGateway;
	protected $adapter;
	
	public function __construct(TableGateway $membermonthlytoken_tableGateway,$adapter)
	{
		$this->membermonthlytoken_tableGateway   	= $membermonthlytoken_tableGateway;
		$this->adapter 				 				= $adapter;
	}
	
	public function fetchAll()
	{
	
		$resultSet = $this->membermonthlytoken_tableGateway->select();
		return $resultSet;
	}
	
	public function getMembermonthlytoken($id)
	{
		$id  = (int) $id;
		$rowset = $this->membermonthlytoken_tableGateway->select(array('member_monthly_token_id' => $id));
		$membermonthlytoken = $rowset->current();
	
		if (!$membermonthlytoken ) {
			throw new \Exception("Could not find row $id from member_sellingcost_items table");
		}
	
		return $membermonthlytoken;
	}
	
	public function getMembercurrenttoken( $member_id ){
		$member_id = (int)$member_id;
		
		
		$rowset = $this->membermonthlytoken_tableGateway->select(function (Select $select) use($member_id){
			$select->where('member_id ='.$member_id );
			$select->order('member_monthly_token_id DESC')->limit(1);
		});
				
		$membermonthlytoken = $rowset->current();
		
		if (!$membermonthlytoken ) {
			throw new \Exception("Could not find row $member_id from member_sellingcost_items table");
		}
		
		return $membermonthlytoken;
		
	}
	
	public function saveMembermonthlytoken(Membermonthlytoken $membermonthlytoken)
	{
		
		$member_monthly_token_id = (int)$membermonthlytoken->get_member_monthly_token_id();
		if ($member_monthly_token_id == 0) {
			
			$data = array(
	
					'member_id'     				=> $membermonthlytoken->get_member_id(),
					'membership_type_id'       		=> $membermonthlytoken->get_membership_type_id(),
					'start_date' 					=> $membermonthlytoken->get_start_date(),
					'stop_date' 					=> $membermonthlytoken->get_stop_date(),
					'num_of_properties'				=> $membermonthlytoken->get_num_of_properties(),
					'num_of_email_per_month'    	=> $membermonthlytoken->get_num_of_email_per_month(),
					'used_num_of_email_per_month'	=> $membermonthlytoken->get_used_num_of_email_per_month(),					
					'sort_by'						=> $membermonthlytoken->get_sort_by(),
					'created_date'					=> date('Y-m-d H:i:s'),
					'last_modified' 				=> date('Y-m-d H:i:s')
	
			);
	
			$this->membermonthlytoken_tableGateway->insert($data);
			return $this->membermonthlytoken_tableGateway->lastInsertValue;
		} else {
	
			$data = array(
	
					'member_id'     				=> $membermonthlytoken->get_member_id(),
					'membership_type_id'       		=> $membermonthlytoken->get_membership_type_id(),
					'start_date' 					=> $membermonthlytoken->get_start_date(),
					'stop_date' 					=> $membermonthlytoken->get_stop_date(),
					'num_of_properties'				=> $membermonthlytoken->get_num_of_properties(),
					'num_of_email_per_month'    	=> $membermonthlytoken->get_num_of_email_per_month(),
					'used_num_of_email_per_month'	=> $membermonthlytoken->get_used_num_of_email_per_month(),					
					'sort_by'						=> $membermonthlytoken->get_sort_by(),
					'created_date'					=> $membermonthlytoken->get_created_date(),
					'last_modified' 				=> date('Y-m-d H:i:s')
	
			);
	
			if ($this->getMembermonthlytoken( $member_monthly_token_id ) ) {
				$this->membermonthlytoken_tableGateway->update($data, array('member_monthly_token_id' => $member_monthly_token_id));
				return $member_monthly_token_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}
	
	
}

?>