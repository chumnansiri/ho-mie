<?php
namespace Index\Model;

class Membersubscription{
	
	private $member_subscription_id;
	private $member_id;
	private $membership_type_id;
	private $purchased_date;
	private $start_date;
	private $stop_date;
	private $num_of_properties;
	private $num_of_email_per_month;
	private $next_membership_type_id;	
	private $sort_by;
	private $created_date;
	private $last_modified;
	
	public function __construct(Membersubscription $membersubscription=null){
		
		if($membersubscription){
	
			$this->member_subscription_id      	= $membersubscription->get_member_subscription_id();
			$this->member_id 	  				= $membersubscription->get_member_id();
			$this->membership_type_id         	= $membersubscription->get_membership_type_id();
			$this->purchased_date				= $membersubscription->get_purchased_date();
			$this->start_date					= $membersubscription->get_start_date();
			$this->stop_date					= $membersubscription->get_stop_date();
			$this->num_of_properties			= $membersubscription->get_num_of_properties();
			$this->num_of_email_per_month       = $membersubscription->get_num_of_email_per_month();
			$this->next_membership_type_id		= $membersubscription->get_next_membership_type_id();
			$this->sort_by						= $membersubscription->get_sort_by();
			$this->created_date					= $membersubscription->get_created_date();
			$this->last_modified				= $membersubscription->get_last_modified();
						
		}
	}
	
	public function exchangeArray($data)
	{
		$this->member_subscription_id          =    (	isset(  $data['member_subscription_id']) 	) ? 	$data['member_subscription_id'] : 	null;
		$this->member_id     				   =    (	isset(  $data['member_id']) 				) ? 	$data['member_id'] 				: 	null;
		$this->membership_type_id              =    (	isset(  $data['membership_type_id']) 		) ? 	$data['membership_type_id'] 	: 	null;		
		$this->purchased_date          		   =    (	isset(  $data['purchased_date']) 			) ? 	$data['purchased_date'] 		: 	null;
		$this->start_date     				   =    (	isset(  $data['start_date']) 				) ? 	$data['start_date'] 			: 	null;
		$this->stop_date              		   =    (	isset(  $data['stop_date']) 				) ? 	$data['stop_date'] 				: 	null;
		$this->num_of_properties          	   =    (	isset(  $data['num_of_properties']) 		) ? 	$data['num_of_properties'] 		: 	null;
		$this->num_of_email_per_month     	   =    (	isset(  $data['num_of_email_per_month']) 	) ? 	$data['num_of_email_per_month'] : 	null;
		$this->next_membership_type_id         =    (	isset(  $data['next_membership_type_id']) 	) ? 	$data['next_membership_type_id']: 	null;		
		$this->sort_by      				   =    (	isset(  $data['sort_by']) 					) ? 	$data['sort_by'] 				: 	null;
		$this->created_date      			   =    (	isset(  $data['created_date']) 				) ? 	$data['created_date'] 			: 	null;
		$this->last_modified      			   =    (	isset(  $data['last_modified']) 			) ? 	$data['last_modified'] 			: 	null;	
	}
	
	
	public function get_member_subscription_id(){
		return $this->member_subscription_id;
	}
	
	public function get_member_id(){
		return $this->member_id;
	}
	
	public function get_membership_type_id(){
		return $this->membership_type_id;
	}
	
	public function get_purchased_date(){
		return $this->purchased_date;
	}
	
	public function get_start_date(){
		return $this->start_date;
	}
	
	public function get_stop_date(){
		return $this->stop_date;
	}
	
	public function get_num_of_properties(){
		return $this->num_of_properties;
	}
	
	public function get_num_of_email_per_month(){
		return $this->num_of_email_per_month;
	}
	
	public function get_next_membership_type_id(){
		return $this->next_membership_type_id;
	}
	
	public function get_sort_by(){
		return $this->sort_by;
	}
	
	public function get_created_date(){
		return $this->created_date;
	}
	
	public function get_last_modified(){
		return $this->last_modified;
	}
	
	////////////////////////////////////
	
	public function set_member_subscription_id( $member_subscription_id ){
		 $this->member_subscription_id = $member_subscription_id;
	}
	
	public function set_member_id( $member_id ){
		 $this->member_id = $member_id;
	}
	
	public function set_membership_type_id( $membership_type_id ){
		 $this->membership_type_id = $membership_type_id;
	}
	
	public function set_purchased_date( $purchased_date ){
		 $this->purchased_date = $purchased_date;
	}
	
	public function set_start_date( $start_date ){
		 $this->start_date = $start_date;
	}
	
	public function set_stop_date( $stop_date ){
		 $this->stop_date = $stop_date;
	}
	
	public function set_num_of_properties( $num_of_properties ){
		 $this->num_of_properties = $num_of_properties;
	}
	
	public function set_num_of_email_per_month( $num_of_email_per_month ){
		 $this->num_of_email_per_month = $num_of_email_per_month;
	}
	
	public function set_next_membership_type_id( $next_membership_type_id ){
		 $this->next_membership_type_id = $next_membership_type_id;
	}
	
	public function set_sort_by( $sort_by ){
		 $this->sort_by = $sort_by;
	}
	
	public function set_created_date( $created_date ){
		 $this->created_date = $created_date;
	}
	
	public function set_last_modified( $last_modified ){
		 $this->last_modified = $last_modified;
	}

}