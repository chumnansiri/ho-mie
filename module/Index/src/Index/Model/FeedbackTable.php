<?php

namespace Index\Model;


use Zend\Db\TableGateway\TableGateway;
use Index\Model\Feedback;

class FeedbackTable{
	protected $feedback_tableGateway;
	protected $adapter;

	public function __construct(TableGateway $feedback_tableGateway,$adapter)
	{
		$this->feedback_tableGateway   	= $feedback_tableGateway;
		$this->adapter 				 = $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->feedback_tableGateway->select();
		return $resultSet;
	}

	public function getFeedback($id)
	{
		$id  = (int) $id;
		$rowset = $this->feedback_tableGateway->select(array('feedback_id' => $id));
		$feedback = $rowset->current();

		if (!$feedback ) {
			throw new \Exception("Could not find row $id from feedback table");
		}

		return $feedback;
	}


	public function saveFeedback(Feedback $feedback)
	{

		

		$feedback_id = (int)$feedback->get_feedback_id();
		if ($feedback_id == 0) {

			$data = array(

					'member_id'     => $feedback->get_member_id(),
					'message'       => $feedback->get_message(),
					'sort_by'		=> $feedback->get_sort_by(),
					'created_date'	=> date('Y-m-d H:i:s'),
					'last_modified' => date('Y-m-d H:i:s')

			);

			$this->feedback_tableGateway->insert($data);
			return $this->feedback_tableGateway->lastInsertValue;
		} else {

			$data = array(

					'member_id'     => $feedback->get_member_id(),
					'message'       => $feedback->get_message(),
					'sort_by'		=> $feedback->get_sort_by(),
					'created_date'	=> $feedback->get_created_date(),
					'last_modified' => date('Y-m-d H:i:s')

			);

			if ($this->getFeedback($feedback_id)) {
				$this->feedback_tableGateway->update($data, array('feedback_id' => $feedback_id));
				return $feedback_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}


}

?>

