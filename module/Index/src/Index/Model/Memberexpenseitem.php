<?php
namespace Index\Model;

// Add these import statements

class Memberexpenseitem{

	private $expense_id;
	private $expense_name;
	private $member_id;
	private $sort_by;
	private $created_date;
	private $last_modified;


	public function __construct(){

	}

	public function exchangeArray($data)
    {
        $this->expense_id     		=    (		isset(  $data['expense_id']) 			) ? 	$data['expense_id'] 		: 	null;
        $this->expense_name     	=    (		isset(  $data['expense_name']) 			) ? 	$data['expense_name'] 		: 	null;
        $this->member_id     		=    (		isset(  $data['member_id']) 			) ? 	$data['member_id'] 			: 	null;
        $this->sort_by     			=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date     	=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified     	=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;


    }

    public function get_expense_id(){
       	return $this->expense_id;
    }

    public function get_expense_name(){
       	return $this->expense_name;
    }

    public function get_member_id(){
		return $this->member_id;
    }

 	public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_expense_id($expense_id){
    	$this->expense_id = $expense_id;
    }

    public function set_expense_name($expense_name){
    	$this->expense_name = $expense_name;
    }

    public function set_member_id($member_id){
    	$this->member_id = $member_id;
    }

	public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }


}
