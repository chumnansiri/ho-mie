<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class PropertyvideoTable{
	protected $PropertyvideotableGateway;
	protected $adapter;

	public function __construct(TableGateway $PropertyvideotableGateway, $adapter )
	{
		$this->PropertyvideotableGateway 		= $PropertyvideotableGateway;
		$this->adapter                  		= $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->PropertyvideotableGateway->select();
		return $resultSet;
	}

	public function getPropertyvideo($id)
	{
		$id  = (int) $id;
		$rowset = $this->PropertyvideotableGateway->select(array('property_video_id' => $id));
		$propertyvideo = $rowset->current();
		if ( !$propertyvideo ) {
			throw new \Exception("Could not find row $id");
		}
		return $propertyvideo;
	}


	public function savePropertyvideo(Propertyvideo $propertyvideo)
	{

		

		$property_video_id = (int)$propertyvideo->get_property_video_id();
		if ($property_video_id == 0) {

			$data = array(
					'property_id' 			=> $propertyvideo->get_property_id(),
					'video_path'   			=> $propertyvideo->get_video_path(),
					'sort_by'				=> $propertyvideo->get_sort_by(),
					'created_date'			=> date('Y-m-d H:i:s'),
					'last_modified' 		=> date('Y-m-d H:i:s')
			);

			$this->PropertyvideotableGateway->insert($data);
			return $this->PropertyvideotableGateway->lastInsertValue;
		} else {

			$data = array(
					'property_id'   		=> $propertyvideo->get_property_id(),
					'video_path'   			=> $propertyvideo->get_video_path(),
					'sort_by'				=> $propertyvideo->get_sort_by(),
					'created_date'			=> $propertyvideo->get_created_date(),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			if ($this->getPropertyvideo($property_video_id) ) {
				$this->PropertyvideotableGateway->update($data, array('property_video_id' => $property_video_id));
				return $property_video_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deletePropertyvideo($property_video_id)
	{
		$this->PropertyvideotableGateway->delete( array('property_video_id' => $property_video_id) );

	}

}

?>

