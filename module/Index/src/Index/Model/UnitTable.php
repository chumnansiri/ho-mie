<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class UnitTable {
	protected $UnittableGateway;
	protected $UnitrenttableGateway;
	protected $adapter;

	public function __construct(TableGateway $UnittableGateway, TableGateway $UnitrenttableGateway, $adapter )
	{
		$this->UnittableGateway 		= $UnittableGateway;
		$this->UnitrenttableGateway     = $UnitrenttableGateway;
		$this->adapter                  = $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getUnit($id)
	{
		$id  = (int) $id;
		$rowset = $this->UnittableGateway->select(array('unit_id' => $id));
		$unit = $rowset->current();
		if (!$unit) {
			throw new \Exception("Could not find row $id");
		}

		$statement = $this->adapter->query('select unit_rent_id
											from unit_rents
											where unit_id ='.$id.' order by year');
		$unit_rents = $statement->execute();
		$k = 0;
		$unitrentoutputs = array();
		foreach( $unit_rents as $unit_rent){
			$rowset 	= $this->UnitrenttableGateway->select( array('unit_rent_id' => $unit_rent['unit_rent_id']) );
			$unitrentoutputs[$k] = $rowset->current();

			$k++;
		}
		$unit->set_unit_rents($unitrentoutputs);


		return $unit;
	}


	public function saveUnit(Unit $unit)
	{

		date_default_timezone_set('America/Los_Angeles');

		$unit_id = (int)$unit->get_unit_id();
		if ($unit_id == 0) {

			$data = array(
					'property_id' 			=> $unit->get_property_id(),
					'unit_type'				=> $unit->get_unit_type(),
					'number_of_units'		=> $unit->get_number_of_units(),
					'number_of_bedrooms'	=> $unit->get_number_of_bedrooms(),
					'number_of_bathrooms'	=> $unit->get_number_of_bathrooms(),
					'furnished'				=> $unit->is_furnished(),
					'unit_squarfeet'		=> $unit->get_unit_squarfeet(),
					'unit_description'		=> $unit->get_unit_description(),
					'sort_by'				=> $unit->get_sort_by(),
					'created_date'			=> date('Y-m-d H:i:s'),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			$this->UnittableGateway->insert($data);
			return $this->UnittableGateway->lastInsertValue;
		} else {

			$data = array(
					'property_id'   		=> $unit->get_property_id(),
					'unit_type'				=> $unit->get_unit_type(),
					'number_of_units'		=> $unit->get_number_of_units(),
					'number_of_bedrooms'	=> $unit->get_number_of_bedrooms(),
					'number_of_bathrooms'	=> $unit->get_number_of_bathrooms(),
					'furnished'				=> $unit->is_furnished(),
					'unit_squarfeet'		=> $unit->get_unit_squarfeet(),
					'unit_description'		=> $unit->get_unit_description(),
					'sort_by'				=> $unit->get_sort_by(),
					'created_date'			=> $unit->get_created_date(),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			if ($this->getUnit($unit_id)) {
				$this->UnittableGateway->update($data, array('unit_id' => $unit_id));
				return $unit_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteUnit($unit_id)
	{
		$this->UnittableGateway->delete( array('unit_id' => $unit_id) );
		$this->UnitrenttableGateway->delete( array('unit_id' => $unit_id) );
	}

}

?>

