<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;
use Index\Model\Membershiptype;


class MembershiptypeTable {
	
	protected $membershiptype_tableGateway;
	protected $adapter;
	
	public function __construct(TableGateway $membershiptype_tableGateway,$adapter)
	{
		$this->membershiptype_tableGateway   	= $membershiptype_tableGateway;
		$this->adapter 				 			= $adapter;
	}
	
	public function fetchAll()
	{
	
		$resultSet = $this->membershiptype_tableGateway->select();
		return $resultSet;
	}
	
	public function getMembershiptype($id)
	{
		$id  = (int) $id;
		$rowset = $this->membershiptype_tableGateway->select(array('membership_type_id' => $id));
		$membershiptype = $rowset->current();
	
		if (!$membershiptype ) {
			throw new \Exception("Could not find row $id from membership_types table");
		}
	
		return $membershiptype;
	}
	
	public function saveMembershiptype(Membershiptype $membershiptype)
	{
		
		$membership_type_id = (int)$membershiptype->get_membership_type_id();
		
		if ($membership_type_id == 0) {
			
			$data = array(
	
					'membership_type_name'       => $membershiptype->get_member_type_name(),
					'membership_type_cost'       => $membershiptype->get_membership_type_cost(),
					'membership_type_period'     => $membershiptype->get_membership_type_period(),
					'num_of_properties'			 => $membershiptype->get_num_of_properties(),
					'num_of_email_per_month'     => $membershiptype->get_num_of_email_per_month(),
					'level' 					 => $membershiptype->get_level(),					
					'sort_by'					 => $membershiptype->get_sort_by(),
					'created_date'				 => date('Y-m-d H:i:s'),
					'last_modified' 			 => date('Y-m-d H:i:s')
	
			);
	
			$this->membershiptype_tableGateway->insert($data);
			return $this->membershiptype_tableGateway->lastInsertValue;
		} else {
	
			$data = array(
	
					'membership_type_name'       => $membershiptype->get_member_type_name(),
					'membership_type_cost'       => $membershiptype->get_membership_type_cost(),
					'membership_type_period'     => $membershiptype->get_membership_type_period(),
					'num_of_properties'			 => $membershiptype->get_num_of_properties(),
					'num_of_email_per_month'     => $membershiptype->get_num_of_email_per_month(),
					'level' 					 => $membershiptype->get_level(),					
					'sort_by'					 => $membershiptype->get_sort_by(),
					'created_date'				 => $membershiptype->get_created_date(),
					'last_modified' 			 => date('Y-m-d H:i:s')
		
			);
	
			if ($this->getMembershiptype($membership_type_id)) {
				$this->membershiptype_tableGateway->update($data, array('membership_type_id' => $membership_type_id));
				return $membership_type_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}
}

?>