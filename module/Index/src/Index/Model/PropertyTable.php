<?php

namespace Index\Model;


//use Zend\Tag\Item;

//use Index\module;

use Zend\Db\TableGateway\TableGateway;
//use Index\Model\Unit;


class PropertyTable {

	protected $property_tableGateway;
	protected $unit_tableGateway;
	protected $unit_rent_tableGateway;
	protected $fullpage_photo_tableGateway;
	protected $property_video_tableGateway;
	protected $adapter;



	public function __construct(TableGateway $property_tableGateway,$unit_tableGateway,$unit_rent_tableGateway,$fullpage_photo_tableGateway,$property_video_tableGateway,$adapter)
	{
		$this->property_tableGateway      = $property_tableGateway;
		$this->unit_tableGateway          = $unit_tableGateway;
		$this->unit_rent_tableGateway     = $unit_rent_tableGateway;
		$this->fullpage_photo_tableGateway= $fullpage_photo_tableGateway;
		$this->property_video_tableGateway= $property_video_tableGateway;
		$this->adapter                    = $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->property_tableGateway->select();
		return $resultSet;
	}

	public function getProperty($id)
	{
		$id  = (int) $id;
		$rowset = $this->property_tableGateway->select(array('property_id' => $id));
		$property = $rowset->current();
		if (!$property) {
			throw new \Exception("Could not find row $id");
		}

		$statement = $this->adapter->query('select property_buyingcost_item_id,
	   										mbi.buyingcost_id,
	   										mbi.buyingcost_name,
	   										property_buyingcost_item_amount
											from property_buyingcost_items pbi
											inner join member_buyingcost_items mbi on pbi.buyingcost_id = mbi.buyingcost_id
											where pbi.property_id ='.$id);

		$buyingcost_items = $statement->execute();


		$i = 0;
		$output = array();
		foreach($buyingcost_items as $buyingcost_item){
			$output[$i]['property_buyingcost_item_id']     = $buyingcost_item['property_buyingcost_item_id'];
			$output[$i]['buyingcost_id'] 			   	   = $buyingcost_item['buyingcost_id'];
			$output[$i]['buyingcost_name']  			   = $buyingcost_item['buyingcost_name'];
			$output[$i]['property_buyingcost_item_amount'] = $buyingcost_item['property_buyingcost_item_amount'];
			$i++;
		}

		$property->set_buyingcost_items($output);

		$statement = $this->adapter->query('select property_sellingcost_item_id,
											msi.sellingcost_id,
											msi.sellingcost_name,
											property_sellingcost_item_amount
											from property_sellingcost_items psi
											inner join member_sellingcost_items msi on psi.sellingcost_id = msi.sellingcost_id
											where psi.property_id ='.$id);

		$sellingcost_items = $statement->execute();


		$i = 0;
		$output = array();
		foreach($sellingcost_items as $sellingcost_item){
			$output[$i]['property_sellingcost_item_id']     = $sellingcost_item['property_sellingcost_item_id'];
			$output[$i]['sellingcost_id'] 			   		= $sellingcost_item['sellingcost_id'];
			$output[$i]['sellingcost_name']  			    = $sellingcost_item['sellingcost_name'];
			$output[$i]['property_sellingcost_item_amount'] = $sellingcost_item['property_sellingcost_item_amount'];
			$i++;
		}

		$property->set_sellingcost_items($output);



		$statement = $this->adapter->query('select property_income_id,pmii.income_id,income_name,amount,frequency
										  from property_member_income_items pmii
										  inner join member_income_items mii on pmii.income_id = mii.income_id
										  where pmii.property_id ='.$id);

		$other_income_items = $statement->execute();
		$i = 0;
		$output = array();
		foreach($other_income_items as $other_income_item){
			$output[$i]['property_income_id']     	= $other_income_item['property_income_id'];
			$output[$i]['income_id'] 			   	= $other_income_item['income_id'];
			$output[$i]['income_name']  			= $other_income_item['income_name'];
			$output[$i]['amount'] 					= $other_income_item['amount'];
			$output[$i]['frequency']                = $other_income_item['frequency'];
			$i++;
		}

		$property->set_other_income_items($output);

		$statement = $this->adapter->query('select property_expense_id,pmii.expense_id,expense_name,amount,frequency
										  from property_member_expense_items pmii
										  inner join member_expense_items mii on pmii.expense_id = mii.expense_id
										  where pmii.property_id ='.$id);

		$other_expense_items = $statement->execute();
		$i = 0;

		$output = array();
		foreach($other_expense_items as $other_expense_item){ 
			
			$output[$i]['property_expense_id']     	= $other_expense_item['property_expense_id'];
			$output[$i]['expense_id'] 			   	= $other_expense_item['expense_id'];
			$output[$i]['expense_name']  			= $other_expense_item['expense_name'];
			$output[$i]['amount'] 					= $other_expense_item['amount'];
			$output[$i]['frequency']                = $other_expense_item['frequency'];
			$i++;
		}

		$property->set_other_expense_items($output);

		$statement = $this->adapter->query('select unit_id,
												   property_id,
												   unit_type,
												   number_of_units,
												   number_of_bedrooms,
												   number_of_bathrooms,
												   furnished,
												   unit_squarfeet,
												   unit_description,
												   sort_by,
												   created_date,
												   last_modified
											from units
											where property_id = '.$id);
		$units = $statement->execute();
		$i = 0;


		$unitoutputs = array();

		foreach($units as $unit){
			$rowset 	= $this->unit_tableGateway->select( array('unit_id' => $unit['unit_id']) );
			$unitoutputs[$i] = $rowset->current();


				$statement = $this->adapter->query('select unit_rent_id
													from unit_rents
													where unit_id ='.$unit['unit_id'].' order by year');
				$unit_rents = $statement->execute();
				$k = 0;
				$unitrentoutputs = array();
				foreach( $unit_rents as $unit_rent){
					$rowset 	= $this->unit_rent_tableGateway->select( array('unit_rent_id' => $unit_rent['unit_rent_id']) );
					$unitrentoutputs[$k] = $rowset->current();

					$k++;
				}
				$unitoutputs[$i]->set_unit_rents($unitrentoutputs);

			$i++;
		}
		$property->set_units($unitoutputs);

		// for fullpage photos
		$statement = $this->adapter->query('select fullpage_photo_id,
												   property_id,
												   fullpage_photo_path,
												   sort_by,
												   created_date,
												   last_modified
											from fullpage_photos
											where property_id = '.$id);
		$fullpage_photos = $statement->execute();
		$i = 0;

		$fullpage_photooutputs = array();

		foreach($fullpage_photos as $fullpage_photo){
			$rowset 	= $this->fullpage_photo_tableGateway->select( array('fullpage_photo_id' => $fullpage_photo['fullpage_photo_id']) );
			$fullpage_photooutputs[$i] = $rowset->current();
			$i++;
		}

		$property->set_fullpage_photos($fullpage_photooutputs);

		// for property videos
		$statement = $this->adapter->query('select property_video_id,
	   											   property_id,
	   											   video_path,
	   											   sort_by,
	   											   created_date,
	   											   last_modified
											from property_videos
											where property_id = '.$id);
		$property_videos = $statement->execute();
		$i = 0;

		$property_videooutputs = array();

		foreach( $property_videos as $property_video){

			$rowset 	= $this->property_video_tableGateway->select( array('property_video_id' => $property_video['property_video_id'] ) );
			$property_videooutputs[$i] = $rowset->current();
			$i++;
		}

		$property->set_property_videos($property_videooutputs);

		$statement = $this->adapter->query('select item_text
											from items
											where id ='.$property->get_property_first_loantype() );



		$first_loantype_texts = $statement->execute();
		foreach( $first_loantype_texts as $first_loantype_text){
			$text = $first_loantype_text['item_text'];
		}
		$property->set_property_first_loantype_text( $text );

		$statement = $this->adapter->query('select item_text
											from items
											where id ='.$property->get_property_second_loantype() );



		$second_loantype_texts = $statement->execute();
		foreach( $second_loantype_texts as $second_loantype_text){
			$text = $second_loantype_text['item_text'];
		}
		$property->set_property_second_loantype_text( $text );

///
/*
		foreach($units as $unit){

			$output[$i] = new Unit();
			$output[$i]->set_unit_id(			 $unit['unit_id']);
			$output[$i]->set_property_id(		 $unit['property_id']);
			$output[$i]->set_unit_type(			 $unit['unit_type']);
			$output[$i]->set_number_of_units(	 $unit['number_of_units']);
			$output[$i]->set_number_of_bedrooms( $unit['number_of_bedrooms']);
			$output[$i]->set_number_of_bathrooms($unit['number_of_bathrooms']);
			$output[$i]->set_furnished(          $unit['furnished']);
			$output[$i]->set_unit_squarfeet(     $unit['unit_squarfeet']);
			$output[$i]->set_unit_description(   $unit['unit_description']);
			$i++;
		}
		$property->set_units($output);
*/

		return $property;
	}

	public function getMemberProperties($member_id)
	{
		$member_id  = (int)$member_id;
		$rowset = $this->property_tableGateway->select(array('member_id' => $member_id));
		$rows = $rowset;

		if (!$rows) {
			throw new \Exception("Could not find row $member_id");
		}
		return $rows;
	}

	public function saveProperty(Property $property)
	{
		


		$property_id = (int)$property->get_property_id();
		if ($property_id == 0) {

			$rowset = $this->property_tableGateway->select(array('member_id'    => $property->get_member_id(),
					'property_name'=> $property->get_property_name()
			));



			if( count( $rowset ) ){
				//throw new \Exception('This property name is already exist for member_id '.$property->get_member_id());
				return 0;
			}

			$data = array(
					'property_name'  	    				=> $property->get_property_name(),

					'property_street'       				=> $property->get_property_street(),
					'property_city'							=> $property->get_property_city(),
					'property_state'						=> $property->get_property_state(),
					'property_zip'							=> $property->get_property_zip(),
					'property_country'						=> $property->get_property_country(),

					'property_mls'							=> $property->get_property_mls(),
					'property_style'						=> $property->get_property_style(),
					'property_squarefeet'					=> $property->get_property_squarefeet(),
					'property_lotsize'						=> $property->get_property_lotsize(),
					'property_yearbuilt'					=> $property->get_property_yearbuilt(),
					'property_lastremodel'  				=> $property->get_property_lastremodel(),
					'property_parking'						=> $property->get_property_parking(),
					'property_hoa'							=> $property->get_property_hoa(),

					'property_listingprice' 				=> $property->get_property_listingprice(),
					'property_initimprove'  				=> $property->get_property_initimprove(),
					'property_purchaseprice'				=> $property->get_property_purchaseprice(),
					'property_first_loanamount' 			=> $property->get_property_first_loanamount(),
					'property_first_loantype'   			=> $property->get_property_first_loantype(),
					'property_first_interestrate'			=> $property->get_property_first_interestrate(),
					'property_first_payment_frequency'		=> $property->get_property_first_payment_frequency(),
					'property_first_term'					=> $property->get_property_first_term(),
					'property_first_interestonly_term'		=> $property->get_property_first_interestonly_term(),
					'property_second_loanamount' 			=> $property->get_property_second_loanamount(),
					'property_second_loantype'   			=> $property->get_property_second_loantype(),
					'property_second_interestrate'			=> $property->get_property_second_interestrate(),
					'property_second_payment_frequency'		=> $property->get_property_second_payment_frequency(),
					'property_second_term'					=> $property->get_property_second_term(),
					'property_second_interestonly_term'		=> $property->get_property_second_interestonly_term(),

					'property_vacancy_rate'					=> $property->get_property_vacancy_rate(),
					'property_rent_mode'					=> $property->get_property_rent_mode(),
					'property_singlevalue_rent'				=> $property->get_property_singlevalue_rent(),
					'property_singlevalue_rent_frequency'	=> $property->get_property_singlevalue_rent_frequency(),

					'property_appreciation_rate'            => $property->get_property_appreciation_rate(),
					'property_incomeinflation_rate'         => $property->get_property_incomeinflation_rate(),
					'property_expenseinflation_rate'        => $property->get_property_expenseinflation_rate(),
					'property_lvt_for_refinance'            => $property->get_property_lvt_for_refinance(),
					'property_compounding_period'           => $property->get_property_compounding_period(),

					'property_cover_photo_path'             => $property->get_property_cover_photo_path(),

					'member_id'								=> $property->get_member_id(),
					'sort_by'								=> $property->get_sort_by(),
					'created_date'							=> date('Y-m-d H:i:s'),
					'last_modified' 						=> date('Y-m-d H:i:s')
			);

			$this->property_tableGateway->insert($data);

			//return insert id
			return $this->property_tableGateway->lastInsertValue;
		} else {
			if ($this->getProperty($property_id)) {

				$data = array(
						'property_name'  	    				=> $property->get_property_name(),

						'property_street'       				=> $property->get_property_street(),
						'property_city'							=> $property->get_property_city(),
						'property_state'						=> $property->get_property_state(),
						'property_zip'							=> $property->get_property_zip(),
						'property_country'						=> $property->get_property_country(),

						'property_mls'							=> $property->get_property_mls(),
						'property_style'						=> $property->get_property_style(),
						'property_squarefeet'					=> $property->get_property_squarefeet(),
						'property_lotsize'						=> $property->get_property_lotsize(),
						'property_yearbuilt'					=> $property->get_property_yearbuilt(),
						'property_lastremodel'  				=> $property->get_property_lastremodel(),
						'property_parking'						=> $property->get_property_parking(),
						'property_hoa'							=> $property->get_property_hoa(),

						'property_listingprice' 				=> $property->get_property_listingprice(),
						'property_initimprove'  				=> $property->get_property_initimprove(),
						'property_purchaseprice'				=> $property->get_property_purchaseprice(),
						'property_first_loanamount' 			=> $property->get_property_first_loanamount(),
						'property_first_loantype'   			=> $property->get_property_first_loantype(),
						'property_first_interestrate'			=> $property->get_property_first_interestrate(),
						'property_first_payment_frequency'		=> $property->get_property_first_payment_frequency(),
						'property_first_term'					=> $property->get_property_first_term(),
					    'property_first_interestonly_term'		=> $property->get_property_first_interestonly_term(),
						'property_second_loanamount' 			=> $property->get_property_second_loanamount(),
						'property_second_loantype'   			=> $property->get_property_second_loantype(),
						'property_second_interestrate'			=> $property->get_property_second_interestrate(),
						'property_second_payment_frequency'		=> $property->get_property_second_payment_frequency(),
						'property_second_term'					=> $property->get_property_second_term(),
						'property_second_interestonly_term'		=> $property->get_property_second_interestonly_term(),

						'property_vacancy_rate'					=> $property->get_property_vacancy_rate(),
						'property_rent_mode'					=> $property->get_property_rent_mode(),
						'property_singlevalue_rent'				=> $property->get_property_singlevalue_rent(),
						'property_singlevalue_rent_frequency'	=> $property->get_property_singlevalue_rent_frequency(),

						'property_appreciation_rate'            => $property->get_property_appreciation_rate(),
						'property_incomeinflation_rate'         => $property->get_property_incomeinflation_rate(),
						'property_expenseinflation_rate'        => $property->get_property_expenseinflation_rate(),
						'property_lvt_for_refinance'            => $property->get_property_lvt_for_refinance(),
						'property_compounding_period'           => $property->get_property_compounding_period(),

						'property_cover_photo_path'             => $property->get_property_cover_photo_path(),

						'member_id'							=> $property->get_member_id(),
						'sort_by'							=> $property->get_sort_by(),
						'created_date'						=> $property->get_created_date(),
						'last_modified'						=> date('Y-m-d H:i:s')
				);
				$this->property_tableGateway->update($data, array('property_id' => $property_id));
				return $property_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteProperty($property_id)
	{
		$this->property_tableGateway->delete(array('property_id' => $property_id));
	}


}

?>