<?php
namespace Index\Model;

// Add these import statements

class Unit{

	private $unit_id;
	private $property_id;
	private $unit_type;
	private $number_of_units;
	private $number_of_bedrooms;
	private $number_of_bathrooms;
	private $furnished;
	private $unit_squarfeet;
	private $unit_description;
	private $unit_rents;
	private $sort_by;
	private $created_date;
	private $last_modified;

	public function __construct(Unit $unit=null){
		if( $unit ){
			$this->unit_id  		   =        $unit->get_unit_id();
			$this->property_id         = 	    $unit->get_property_id();
			$this->unit_type	       =        $unit->get_unit_type();
			$this->number_of_units     =		$unit->get_number_of_units();
			$this->number_of_bedrooms  =        $unit->get_number_of_bedrooms();
			$this->number_of_bathrooms =        $unit->get_number_of_bathrooms();
			$this->furnished           =        $unit->is_furnished();
			$this->unit_squarfeet      =        $unit->get_unit_squarfeet();
			$this->unit_description    =        $unit->get_unit_description();
			$this->unit_rents          =        $unit->get_unit_rents();
			$this->sort_by             =        $unit->get_sort_by();
			$this->created_date        =        $unit->get_created_date();
			$this->last_modified       =        $unit->get_last_modified();
		}
	}

	public function exchangeArray($data)
    {
        $this->unit_id     				=    (		isset(  $data['unit_id']) 				) ? 	$data['unit_id'] 			: 	null;
        $this->property_id     			=    (		isset(  $data['property_id']) 			) ? 	$data['property_id'] 		: 	null;
        $this->unit_type     			=    (		isset(  $data['unit_type']) 			) ? 	$data['unit_type'] 			: 	null;
        $this->number_of_units      	=    (		isset(  $data['number_of_units']) 		) ? 	$data['number_of_units'] 	: 	null;
        $this->number_of_bedrooms       =    (		isset(  $data['number_of_bedrooms']) 	) ? 	$data['number_of_bedrooms'] : 	null;
        $this->number_of_bathrooms      =    (		isset(  $data['number_of_bathrooms']) 	) ? 	$data['number_of_bathrooms']: 	null;
        $this->furnished     			=    (		isset(  $data['furnished']) 			) ? 	$data['furnished'] 			: 	null;
        $this->unit_squarfeet     		=    (		isset(  $data['unit_squarfeet']) 		) ? 	$data['unit_squarfeet'] 	: 	null;
        $this->unit_description      	=    (		isset(  $data['unit_description']) 		) ? 	$data['unit_description'] 	: 	null;
        $this->sort_by      			=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date      		=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified      		=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;

    }


    public function get_unit_id(){
		return $this->unit_id;
    }

    public function get_property_id(){
    	return $this->property_id;
    }

    public function get_unit_type(){
		return $this->unit_type;
    }

    public function get_number_of_units(){
		return $this->number_of_units;
    }

    public function get_number_of_bedrooms(){
		return $this->number_of_bedrooms;
    }

    public function get_number_of_bathrooms(){
    	return $this->number_of_bathrooms;
    }

    public function is_furnished(){
    	return $this->furnished;
    }

    public function get_unit_squarfeet(){
    	return $this->unit_squarfeet;
    }

    public function get_unit_description(){
    	return $this->unit_description;
    }

    public function get_unit_rents(){
		return $this->unit_rents;
    }

    public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_unit_id($unit_id){
		$this->unit_id = $unit_id;
    }

    public function set_property_id($property_id){
    	$this->property_id = $property_id;
    }

    public function set_unit_type($unit_type){
		$this->unit_type = $unit_type;
    }

    public function set_number_of_units($number_of_units){
		$this->number_of_units = $number_of_units;
    }

    public function set_number_of_bedrooms($number_of_bedrooms){
		$this->number_of_bedrooms = $number_of_bedrooms;
    }

    public function set_number_of_bathrooms($number_of_bathrooms){
    	$this->number_of_bathrooms = $number_of_bathrooms;
    }

    public function set_furnished($furnished){
    	$this->furnished = $furnished;
    }

    public function set_unit_squarfeet($unit_squarfeet){
    	$this->unit_squarfeet = $unit_squarfeet;
    }

    public function set_unit_description($unit_description){
    	$this->unit_description = $unit_description;
    }

    public function set_unit_rents($unit_rents){
    	$this->unit_rents = $unit_rents;
    }

    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }

}
