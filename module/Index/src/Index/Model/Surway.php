<?php

namespace Index\Model;

class Surway {
	
	private $id;
	private $member_id;
	private $created_date;
	
	public function __construct(Surway $surway=null){
		if($surway){
	
			$this->id      			= $surway->get_id();
			$this->member_id 	  	= $surway->get_member_id();			
			$this->created_date   	= $surway->get_created_date();
		}
	}
	
	public function exchangeArray($data)
	{
		$this->id          			=    (		isset(  $data['id']) 			) ? 	$data['fid'] 		 : 	null;
		$this->member_id     		=    (		isset(  $data['member_id']) 	) ? 	$data['member_id'] 	 : 	null;	
		$this->created_date      	=    (		isset(  $data['created_date']) 	) ? 	$data['created_date']: 	null;
	}

	public function get_id(){
		return $this->id;
	}
	
	public function get_member_id(){
		return $this->member_id;
	}
		
	public function get_created_date(){
		return $this->created_date;
	}
	
	public function set_id( $id ){
		$this->id = $id;
	}
	
	public function set_member_id( $member_id ){
		$this->member_id = $member_id;
	}
	
	public function set_created_date( $created_date){
		$this->created_date = $created_date;
	}
	
	
	
}

?>