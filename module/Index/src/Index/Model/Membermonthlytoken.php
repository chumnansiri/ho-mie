<?php

namespace Index\Model;

class Membermonthlytoken {
	
	private $member_monthly_token_id;
	private $member_id;
	private $membership_type_id;
	private $start_date;
	private $stop_date;
	private $num_of_properties;
	private $num_of_email_per_month;
	private $used_num_of_email_per_month;
	private $sort_by;
	private $created_date;
	private $last_modified;
	
	public function __construct(Membermonthlytoken $membermonthlytokens=null){
		
		if($membermonthlytokens){
	
			$this->member_monthly_token_id      = $membermonthlytokens->get_member_monthly_token_id();
			$this->member_id 	  				= $membermonthlytokens->get_member_id();
			$this->membership_type_id          	= $membermonthlytokens->get_membership_type_id();
			$this->start_date 					= $membermonthlytokens->get_start_date();
			$this->stop_date 					= $membermonthlytokens->get_stop_date();
			$this->num_of_properties            = $membermonthlytokens->get_num_of_properties();
			$this->num_of_email_per_month       = $membermonthlytokens->get_num_of_email_per_month();		
			$this->used_num_of_email_per_month  = $membermonthlytokens->get_used_num_of_email_per_month();	
			$this->sort_by        				= $membermonthlytokens->get_sort_by();
			$this->created_date   				= $membermonthlytokens->get_created_date();
			$this->last_modified  				= $membermonthlytokens->get_last_modified();
		
		}
	}
	
	public function exchangeArray($data)
	{
		$this->member_monthly_token_id      =    (	isset(  $data['member_monthly_token_id']) 	) ? 	$data['member_monthly_token_id'] 	: 	null;
		$this->member_id     				=    (	isset(  $data['member_id']) 				) ? 	$data['member_id'] 					: 	null;
		$this->membership_type_id           =    (	isset(  $data['membership_type_id']) 		) ? 	$data['membership_type_id'] 		: 	null;
		$this->start_date      				=    (	isset(  $data['start_date']) 				) ? 	$data['start_date'] 				: 	null;
		$this->stop_date     				=    (	isset(  $data['stop_date']) 				) ? 	$data['stop_date'] 					: 	null;
		$this->num_of_properties            =    (	isset(  $data['num_of_properties']) 		) ? 	$data['num_of_properties'] 			: 	null;
		$this->num_of_email_per_month       =    (	isset(  $data['num_of_email_per_month']) 	) ? 	$data['num_of_email_per_month'] 	: 	null;
		$this->used_num_of_email_per_month  =    (	isset(  $data['used_num_of_email_per_month']) ) ? 	$data['used_num_of_email_per_month']: 	null;
		$this->sort_by      				=    (	isset(  $data['sort_by']) 					) ? 	$data['sort_by'] 					: 	null;
		$this->created_date      			=    (	isset(  $data['created_date']) 				) ? 	$data['created_date'] 				: 	null;
		$this->last_modified      			=    (	isset(  $data['last_modified']) 			) ? 	$data['last_modified'] 				: 	null;
	
	}
	
	public function get_member_monthly_token_id(){
		return $this->member_monthly_token_id;
	}
	
	public function get_member_id(){
		return $this->member_id;
	}
	
	public function get_membership_type_id(){
		return $this->membership_type_id;
	}
	
	public function get_start_date(){
		return $this->start_date;
	}
	
	public function get_stop_date(){
		return $this->stop_date;
	}
	
	public function get_num_of_properties(){
		return $this->num_of_properties;
	}
	
	public function get_num_of_email_per_month(){
		return $this->num_of_email_per_month;
	}
	
	public function get_used_num_of_email_per_month(){
		return $this->used_num_of_email_per_month;
	}
	
	public function get_sort_by(){
		return $this->sort_by;
	}
	
	public function get_created_date(){
		return $this->created_date;
	}
	
	public function get_last_modified(){
		return $this->last_modified;
	}
	
	///////////////
	
	public function set_member_monthly_token_id( $member_monthly_token_id ){
		 $this->member_monthly_token_id = member_monthly_token_id;
	}
	
	public function set_member_id( $member_id ){
		 $this->member_id = $member_id;
	}
	
	public function set_membership_type_id( $membership_type_id ){
		 $this->membership_type_id = $membership_type_id;
	}
	
	public function set_start_date( $start_date ){
		 $this->start_date = $start_date;
	}
	
	public function set_stop_date( $stop_date){
		 $this->stop_date = $stop_date;
	}
	
	public function set_num_of_properties( $num_of_properties ){
		 $this->num_of_properties = $num_of_properties;
	}
	
	public function set_num_of_email_per_month( $num_of_email_per_month ){
		 $this->num_of_email_per_month = $num_of_email_per_month;
	}
	
	public function set_used_num_of_email_per_month( $used_num_of_email_per_month ){
		$this->used_num_of_email_per_month = $used_num_of_email_per_month;
	}
	
	public function set_sort_by( $sort_by ){
		 $this->sort_by = $sort_by;
	}
	
	public function set_created_date( $created_date ){
		 $this->created_date = $created_date;
	}
	
	public function set_last_modified( $last_modified ){
		 $this->last_modified = $last_modified;
	}
	
}



?>