<?php


namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class MemberbuyingcostitemTable{

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getMemberbuyingcostitem($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('buyingcost_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getMemberbuyingcostitems($member_id){

		$where = array(
				'member_id'	=> $member_id,

		);

		$resultSet = $this->tableGateway->select($where);

		return $resultSet;
	}

	public function saveMemberbuyingcostitem(Memberbuyingcostitem $memberbuyingcostitem)
	{

		

		$buyingcost_id = (int)$memberbuyingcostitem->get_buyingcost_id();
		if ($buyingcost_id == 0) {

			$data = array(
					'buyingcost_name'  	=> $memberbuyingcostitem->get_buyingcost_name(),
					'member_id'  		=> $memberbuyingcostitem->get_member_id(),
					'sort_by'			=> $memberbuyingcostitem->get_sort_by(),
					'created_date'		=> date('Y-m-d H:i:s'),
					'last_modified' 	=> date('Y-m-d H:i:s')
			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'buyingcost_name'  	=> $memberbuyingcostitem->get_buyingcost_name(),
					'member_id'  		=> $memberbuyingcostitem->get_member_id(),
					'sort_by'			=> $memberbuyingcostitem->get_sort_by(),
					'created_date'		=> $memberbuyingcostitem->get_created_date(),
					'last_modified' 	=> date('Y-m-d H:i:s')
			);

			if ($this->getMemberbuyingcostitem($buyingcost_id)) {
				$this->tableGateway->update($data, array('buyingcost_id' => $buyingcost_id));
				return $buyingcost_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteMemberbuyingcostitem($buyingcost_id)
	{
		$this->tableGateway->delete(array('buyingcost_id' => $buyingcost_id));
	}

}