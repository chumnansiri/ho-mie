<?php
namespace Index\Model;



// Add these import statements

class Property{

	private $property_id;
	private $property_name;

	private $property_street;
	private $property_city;
	private $property_state;
	private $property_zip;
	private $property_country;

	private $property_mls;
	private $property_style;
	private $property_squarefeet;
	private $property_lotsize;
	private $property_yearbuilt;
	private $property_lastremodel;
	private $property_parking;
	private $property_hoa;

	private $property_listingprice;
	private $property_initimprove;
	private $property_purchaseprice;
	private $property_first_loanamount;
	private $property_first_loantype;
	private $property_first_loantype_text;
	private $property_first_interestrate;
	private $property_first_payment_frequency;
	private $property_first_term;
	private $property_first_interestonly_term;
	private $property_second_loanamount;
	private $property_second_loantype;
	private $property_second_loantype_text;
	private $property_second_interestrate;
	private $property_second_payment_frequency;
	private $property_second_term;
	private $property_second_interestonly_term;

	private $property_vacancy_rate;
	private $property_rent_mode;
	private $property_singlevalue_rent;
	private $property_singlevalue_rent_frequency;

	private $property_appreciation_rate;
	private $property_incomeinflation_rate;
	private $property_expenseinflation_rate;
	private $property_lvt_for_refinance;
	private $property_compounding_period;

	private $property_cover_photo_path;

	private $member_id;
	private $sort_by;
	private $created_date;
	private $last_modified;

	private $buyingcost_items;
	private $sellingcost_items;
	private $other_income_items;
	private $other_expense_items;

	private $units;

	private $fullpage_photos;
	private $property_videos;


	public function __construct(Property $property=null){
		if($property){

			$this->property_id 							= $property->get_property_id();
			$this->property_name 						= $property->get_property_name();

			$this->property_street  					= $property->get_property_street();
			$this->property_city    					= $property->get_property_city();
			$this->property_state   					= $property->get_property_state();
			$this->property_zip     					= $property->get_property_zip();
			$this->property_country 					= $property->get_property_country();

			$this->property_mls 						= $property->get_property_mls();
			$this->property_style   					= $property->get_property_style();
			$this->property_squarefeet  				= $property->get_property_squarefeet();
			$this->property_lotsize     				= $property->get_property_lotsize();
			$this->property_yearbuilt   				= $property->get_property_yearbuilt();
			$this->property_lastremodel 				= $property->get_property_lastremodel();
			$this->property_parking     				= $property->get_property_parking();
			$this->property_hoa         				= $property->get_property_hoa();

			$this->property_listingprice 				= $property->get_property_listingprice();
			$this->property_initimprove 				= $property->get_property_initimprove();
			$this->property_purchaseprice 				= $property->get_property_purchaseprice();
			$this->property_first_loanamount 			= $property->get_property_first_loanamount();
			$this->property_first_loantype      		= $property->get_property_first_loantype();
			$this->property_first_loantype_text         = $property->get_property_first_loantype_text();
			$this->property_first_interestrate  		= $property->get_property_first_interestrate();
			$this->property_first_payment_frequency 	= $property->get_property_first_payment_frequency();
			$this->property_first_term 	        		= $property->get_property_first_term();
			$this->property_first_interestonly_term 	= $property->get_property_first_interestonly_term();
			$this->property_second_loanamount 			= $property->get_property_second_loanamount();
			$this->property_second_loantype      		= $property->get_property_second_loantype();
			$this->property_second_loantype_text      	= $property->get_property_second_loantype_text();
			$this->property_second_interestrate  		= $property->get_property_second_interestrate();
			$this->property_second_payment_frequency    = $property->get_property_second_payment_frequency();
			$this->property_second_term 	        	= $property->get_property_second_term();
			$this->property_second_interestonly_term    = $property->get_property_second_interestonly_term();

			$this->property_vacancy_rate            	= $property->get_property_vacancy_rate();
			$this->property_rent_mode                   = $property->get_property_rent_mode();
			$this->property_singlevalue_rent        	= $property->get_property_singlevalue_rent();
			$this->property_singlevalue_rent_frequency  = $property->get_property_singlevalue_rent_frequency();

			$this->property_appreciation_rate           = $property->get_property_appreciation_rate();
			$this->property_incomeinflation_rate        = $property->get_property_incomeinflation_rate();
			$this->property_expenseinflation_rate       = $property->get_property_expenseinflation_rate();
			$this->property_lvt_for_refinance           = $property->get_property_lvt_for_refinance();
			$this->property_compounding_period          = $property->get_property_compounding_period();

			$this->property_cover_photo_path            = $property->get_property_cover_photo_path();

			$this->member_id        					= $property->get_member_id();
			$this->sort_by          					= $property->get_sort_by();
			$this->created_date     					= $property->get_created_date();
			$this->last_modified    					= $property->get_last_modified();

			$this->buyingcost_items     				= $property->get_buyingcost_items();
			$this->sellingcost_items                    = $property->get_sellingcost_items();
			$this->other_income_items                   = $property->get_other_income_items();
			$this->other_expense_items                  = $property->get_other_expense_items();

			$this->units                                = $property->get_units();

			$this->fullpage_photos                      = $property->get_fullpage_photos();
			$this->property_videos                      = $property->get_property_videos();
			
		}

	}

	public function exchangeArray($data) // used when map class to database, only defined attribute that exist in database column
    {
        $this->property_id     						=    (	isset(  $data['property_id']) 						) ? 	$data['property_id'] 						: 	null;
        $this->property_name     					=    (	isset(  $data['property_name']) 					) ? 	$data['property_name'] 						: 	null;

        $this->property_street     					=    (	isset(  $data['property_street']) 					) ? 	$data['property_street'] 					: 	null;
        $this->property_city     					=    (	isset(  $data['property_city']) 					) ? 	$data['property_city'] 						: 	null;
        $this->property_state     					=    (	isset(  $data['property_state']) 					) ? 	$data['property_state'] 					: 	null;
        $this->property_zip     					=    (	isset(  $data['property_zip']) 		    			) ? 	$data['property_zip'] 						: 	null;
        $this->property_country     				=    (	isset(  $data['property_country']) 					) ? 	$data['property_country'] 					: 	null;

        $this->property_mls     					=    (	isset(  $data['property_mls']) 		    			) ? 	$data['property_mls'] 	     				: 	null;
        $this->property_style     					=    (	isset(  $data['property_style']) 					) ? 	$data['property_style']      				: 	null;
        $this->property_squarefeet  				=    (  isset(  $data['property_squarefeet'])   			) ? 	$data['property_squarefeet'] 				: 	null;
        $this->property_lotsize     				=    (  isset(  $data['property_lotsize'])      			) ? 	$data['property_lotsize'] 	 				: 	null;
        $this->property_yearbuilt   				=    (  isset(  $data['property_yearbuilt'])    			) ? 	$data['property_yearbuilt']  				: 	null;
        $this->property_lastremodel 				=    (  isset(  $data['property_lastremodel'])  			) ? 	$data['property_lastremodel'] 				: 	null;
        $this->property_parking     				=    (  isset(  $data['property_parking'])      			) ? 	$data['property_parking']   				: 	null;
        $this->property_hoa         				=    (  isset(  $data['property_hoa'])          			) ? 	$data['property_hoa']       				: 	null;

		$this->property_listingprice 				=    (  isset(  $data['property_listingprice']) 			) ? 	$data['property_listingprice']       		: 	null;
		$this->property_initimprove 				=    (  isset(  $data['property_initimprove']) 				) ? 	$data['property_initimprove']       		: 	null;
		$this->property_purchaseprice 				=    (  isset(  $data['property_purchaseprice']) 			) ? 	$data['property_purchaseprice']       		: 	null;
		$this->property_first_loanamount       		=    (  isset(  $data['property_first_loanamount']) 		) ? 	$data['property_first_loanamount']          : 	null;
		$this->property_first_loantype          	=    (  isset(  $data['property_first_loantype']) 			) ? 	$data['property_first_loantype']       	    : 	null;
		$this->property_first_interestrate      	=    (  isset(  $data['property_first_interestrate']) 		) ? 	$data['property_first_interestrate']        : 	null;
		$this->property_first_payment_frequency 	=    (  isset(  $data['property_first_payment_frequency']) 	) ? 	$data['property_first_payment_frequency']   : 	null;
		$this->property_first_term              	=    (  isset(  $data['property_first_term']) 				) ? 	$data['property_first_term']       		    : 	null;
		$this->property_first_interestonly_term 	=    (  isset(  $data['property_first_interestonly_term']) 	) ? 	$data['property_first_interestonly_term']   : 	null;
		$this->property_second_loanamount       	=    (  isset(  $data['property_second_loanamount']) 	  	) ? 	$data['property_second_loanamount']       	: 	null;
		$this->property_second_loantype         	=    (  isset(  $data['property_second_loantype']) 			) ? 	$data['property_second_loantype']       	: 	null;
		$this->property_second_interestrate     	=    (  isset(  $data['property_second_interestrate']) 		) ? 	$data['property_second_interestrate']       : 	null;
		$this->property_second_payment_frequency    =    (  isset(  $data['property_second_payment_frequency']) ) ? 	$data['property_second_payment_frequency']  : 	null;
		$this->property_second_term             	=    (  isset(  $data['property_second_term'])				) ? 	$data['property_second_term']       		: 	null;
		$this->property_second_interestonly_term 	=    (  isset(  $data['property_second_interestonly_term']) ) ? 	$data['property_second_interestonly_term']  : 	null;

		$this->property_vacancy_rate            	=    (  isset(  $data['property_vacancy_rate']) 			) ? 	$data['property_vacancy_rate']  			: 	null;
		$this->property_rent_mode                   =    (  isset(  $data['property_rent_mode']) 				) ? 	$data['property_rent_mode']  				: 	null;
		$this->property_singlevalue_rent            =    (  isset(  $data['property_singlevalue_rent']) 		) ? 	$data['property_singlevalue_rent']  		: 	null;
		$this->property_singlevalue_rent_frequency  =    (  isset(  $data['property_singlevalue_rent_frequency'])) ? 	$data['property_singlevalue_rent_frequency']: 	null;

		$this->property_appreciation_rate           =    (  isset(  $data['property_appreciation_rate']) 		) ? 	$data['property_appreciation_rate']  		: 	null;
		$this->property_incomeinflation_rate        =    (  isset(  $data['property_incomeinflation_rate']) 	) ? 	$data['property_incomeinflation_rate']  	: 	null;
		$this->property_expenseinflation_rate       =    (  isset(  $data['property_expenseinflation_rate']) 	) ? 	$data['property_expenseinflation_rate']  	: 	null;
		$this->property_lvt_for_refinance           =    (  isset(  $data['property_lvt_for_refinance']) 		) ? 	$data['property_lvt_for_refinance']  		: 	null;
		$this->property_compounding_period  		=    (  isset(  $data['property_compounding_period'])		) ? 	$data['property_compounding_period']        : 	null;

		$this->property_cover_photo_path  		    =    (  isset(  $data['property_cover_photo_path'])		    ) ? 	$data['property_cover_photo_path']        : 	null;

        $this->member_id            = 	 (		isset(  $data['member_id']) 		    ) ? 	$data['member_id'] 	        : 	null;
        $this->sort_by      		=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date      	=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified      	=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;



    }


    public function get_property_id(){
		return $this->property_id;
    }

    public function get_property_name(){
		return $this->property_name;
    }

    public function get_property_street(){
    	return $this->property_street;
    }

    public function get_property_city(){
    	return $this->property_city;
    }

    public function get_property_state(){
    	return $this->property_state;
    }

    public function get_property_zip(){
    	return $this->property_zip;
    }

    public function get_property_country(){
    	return $this->property_country;
    }

    public function get_property_mls(){
    	return $this->property_mls;
    }

    public function get_property_style(){
    	return $this->property_style;
    }

    public function get_property_squarefeet(){
    	return $this->property_squarefeet;
    }

    public function get_property_lotsize(){
    	return $this->property_lotsize;
    }

    public function get_property_yearbuilt(){
    	return $this->property_yearbuilt;
    }

    public function get_property_lastremodel(){
    	return $this->property_lastremodel;
    }

    public function get_property_parking(){
    	return $this->property_parking;
    }

    public function get_property_hoa(){
    	return $this->property_hoa;
    }

    public function get_property_listingprice(){
    	return $this->property_listingprice;
    }

    public function get_property_initimprove(){
    	return $this->property_initimprove;
    }

    public function get_property_purchaseprice(){
    	return $this->property_purchaseprice;
    }

    public function get_property_first_loanamount(){
    	return $this->property_first_loanamount;
    }

    public function get_property_first_loantype(){
    	return $this->property_first_loantype;
    }

    public function get_property_first_loantype_text(){
    	return $this->property_first_loantype_text;
    }

    public function get_property_first_interestrate(){
    	return $this->property_first_interestrate;
    }

    public function get_property_first_payment_frequency(){
    	return $this->property_first_payment_frequency;
    }

    public function get_property_first_term(){
    	return $this->property_first_term;
    }

    public function get_property_first_interestonly_term(){
    	return $this->property_first_interestonly_term;
    }

    public function get_property_second_loanamount(){
    	return $this->property_second_loanamount;
    }

    public function get_property_second_loantype(){
    	return $this->property_second_loantype;
    }

    public function get_property_second_loantype_text(){
    	return $this->property_second_loantype_text;
    }

    public function get_property_second_interestrate(){
    	return $this->property_second_interestrate;
    }

    public function get_property_second_payment_frequency(){
    	return $this->property_second_payment_frequency;
    }

    public function get_property_second_term(){
    	return $this->property_second_term;
    }

    public function get_property_second_interestonly_term(){
    	return $this->property_second_interestonly_term;
    }

    public function get_property_vacancy_rate(){
    	return $this->property_vacancy_rate;
    }

    public function get_property_rent_mode(){
    	return $this->property_rent_mode;
    }

    public function get_property_singlevalue_rent(){

    	return $this->property_singlevalue_rent;

    }

    public function get_property_singlevalue_rent_frequency(){
    	return $this->property_singlevalue_rent_frequency;
    }

    public function get_property_appreciation_rate(){

    	return $this->property_appreciation_rate;
    }

    public function get_property_incomeinflation_rate(){
    	return $this->property_incomeinflation_rate;
    }

    public function get_property_expenseinflation_rate(){
    	return $this->property_expenseinflation_rate;
    }

    public function get_property_lvt_for_refinance(){
    	return $this->property_lvt_for_refinance;
    }

    public function get_property_compounding_period(){
    	return $this->property_compounding_period;
    }

    public function get_property_cover_photo_path(){
    	return $this->property_cover_photo_path;
    }

    public function get_member_id(){
    	return $this->member_id;
    }

    public function get_sort_by(){
    	return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    public function get_buyingcost_items(){
    	return $this->buyingcost_items;
    }

    public function get_sellingcost_items(){
    	return $this->sellingcost_items;
    }

    public function get_other_income_items(){
    	return $this->other_income_items;
    }

    public function get_other_expense_items(){
	   	return $this->other_expense_items;
	}

	public function get_units(){
		return $this->units;
	}

	public function get_fullpage_photos(){
		return $this->fullpage_photos;
	}

	public function get_property_videos(){
		return $this->property_videos;
	}



    ////////////////////////////////
    // below are abstract value

    public function get_buyingcost(){
    	$buyingcost = 0;
        //$buyingcost_items = $this->buyingcost_items;


        $buyingcost_items = $this->buyingcost_items;




    	foreach( (array)$buyingcost_items as $buyingcost_item)
    	{
			$buyingcost += $buyingcost_item['property_buyingcost_item_amount'];

    	}
    	return $buyingcost;

    }

    public function get_sellingcost(){
    	$sellingcost = 0;
    	//$sellingcost_items = $this->sellingcost_items;


    	$sellingcost_items = $this->sellingcost_items;




    	foreach( (array)$sellingcost_items as $sellingcost_item)
    	{
    		$sellingcost += $sellingcost_item['property_sellingcost_item_amount'];

    	}
    	return $sellingcost;

    }

    public function get_initialcost(){
    	$initialcost =  $this->get_property_initimprove()+$this->get_buyingcost();
	   	return $initialcost;
    }

    public function get_firstmortgage_amount(){
    	$firstmortgage_amount  = $this->get_property_purchaseprice() - ( $this->get_property_purchaseprice() * ($this->property_first_loanamount / 100) );
    	return $firstmortgage_amount;
    }

    public function get_secondmortgage_amount(){
    	$secondmortgage_amount = $this->get_property_purchaseprice() - ( $this->get_property_purchaseprice() * ($this->property_second_loanamount / 100) );
    	return $secondmortgage_amount;
    }

    public function get_downpayment(){
    	$downpayment = $this->get_property_purchaseprice() - $this->get_firstmortgage_amount() - $this->get_secondmortgage_amount();
    	return $downpayment;
    }

    public function get_initialcash_invested(){
    	$intialcash_invested = $this->get_initialcost() + $this->get_downpayment();
    	return $intialcash_invested;
    }

	public function get_property_otherincome(){

		$otherincome = 0;

		$otherincome_items = $this->other_income_items;
		$i=0;
		foreach((array)$otherincome_items as $otherincome_item)
		{
			if( $otherincome_item['frequency'] == 30 ){ // bi-weekly
				$multipier = 26;
			}else if( $otherincome_item['frequency'] == 31 ){ // monthly
				$multipier = 12;
			}else if( $otherincome_item['frequency'] == 32 ){ // yearly
				$multipier = 1;
			}

			$otherincome += ( $otherincome_item['amount'] * $multipier );
			$i++;
		}

		return $otherincome;



	}

	public function get_property_otherexpense(){
		$otherexpense=0;

		$otherexpense_items = $this->other_expense_items;

		foreach((array)$otherexpense_items as $otherexpense_item)
		{
			if( $otherexpense_item['frequency'] == 30 ){ // bi-weekly
				$multipier = 26;
			}else if( $otherexpense_item['frequency'] == 31 ){ // monthly
				$multipier = 12;
			}else if( $otherexpense_item['frequency'] == 32 ){ // yearly
				$multipier = 1;
			}

			$otherexpense += ( $otherexpense_item['amount'] * $multipier );
		}

		return $otherexpense;
	}

	public function get_property_sum_of_units_permonth(){
		$total_rent=0;

		foreach($this->units as $unit){
			$rents = $unit->get_unit_rents();
			if( count($rents) > 0 ){
				$firstrent 		= $rents[0]->get_unit_rent_rent();
				$firstfrequency = $rents[0]->get_unit_rent_frequency();
				if( $firstfrequency == 30 ){
					$total_rent += ($firstrent * $unit->get_number_of_units() * 26)/12;
				}else if( $firstfrequency == 31 ){
					$total_rent += $firstrent * $unit->get_number_of_units();
				}else if($firstfrequency == 32){
					$total_rent += ($firstrent * $unit->get_number_of_units() )/12;
				}
			}
			/* this will never happen because front end force user to add rent
			else{
				$total_rent += 0;
			}
			*/

			//$total_rent = $total_rent * $unit->get_number_of_units();
		}


			return $total_rent;



	}

	public function get_property_loan_to_cost1(){
		return 100-$this->get_property_first_loanamount();
	}

	public function get_property_loan_to_cost2(){
		return 100-$this->get_property_second_loanamount();
	}

	public function get_property_loan_to_value1(){
		return ( $this->get_actual_loan_amount1()/ $this->get_property_listingprice()*100);
	}

	public function get_property_loan_to_value2(){
		return ( $this->get_actual_loan_amount2()/ $this->get_property_listingprice()*100);
	}

	public function get_actual_loan_amount1(){
		return ($this->get_property_purchaseprice() - ( $this->get_property_first_loanamount()*( $this->get_property_purchaseprice()/100) ) );
	}

	public function get_actual_loan_amount2(){
		return ($this->get_property_purchaseprice() - ( $this->get_property_second_loanamount()*( $this->get_property_purchaseprice()/100) ) );
	}

	public function get_payment1(){


		$loan_term = $this->get_property_first_term();

		if($this->get_property_first_payment_frequency() == 22){
			$n = $loan_term * 26;
			$int_only_paymentterm=26;
		}else if( $this->get_property_first_payment_frequency() == 23){
			$n = $loan_term * 12;
			$int_only_paymentterm=12;
		}else if( $this->get_property_first_payment_frequency() == 24){
			$n = $loan_term ;
			$int_only_paymentterm=1;
		}

		$interested_rate = $this->get_property_first_interestrate();
		$c = ( ( $interested_rate/100 ) / 12);
		$loan_amount = $this->get_actual_loan_amount1();

		if( $this->get_property_first_loantype() == 25){
			$payment_amount = ($loan_amount * ( $c * pow( (1+$c), $n ) ) )  / ( ( pow( (1+$c), $n ) ) - 1 );
		}else{
			$payment_amount = ( $loan_amount*( $interested_rate/100 ) ) / $int_only_paymentterm;
		}


		return $payment_amount;

	}

	public function get_payment2(){


		$loan_term = $this->get_property_second_term();

		if($this->get_property_second_payment_frequency() == 22){
			$n = $loan_term * 26;
			$int_only_paymentterm=26;
		}else if( $this->get_property_second_payment_frequency() == 23){
			$n = $loan_term * 12;
			$int_only_paymentterm=12;
		}else if( $this->get_property_second_payment_frequency() == 24){
			$n = $loan_term ;
			$int_only_paymentterm=1;
		}

		$interested_rate = $this->get_property_second_interestrate();
		$c = ( ( $interested_rate/100 ) / 12);
		$loan_amount = $this->get_actual_loan_amount2();

		if( $this->get_property_second_loantype() == 25){
			$payment_amount = ($loan_amount * ( $c * pow( (1+$c), $n ) ) )  / ( ( pow( (1+$c), $n ) ) - 1 );
		}else{
			$payment_amount = ( $loan_amount*( $interested_rate/100 ) ) / $int_only_paymentterm;
		}


		return $payment_amount;
	}



	public function get_gross_rent_multipier(){

		$grm = 0;


		if( ( $this->get_property_singlevalue_rent() == 0 ) && ( $this->get_property_sum_of_units_permonth() == 0 ) ) {
			return $grm;
		}

		if( ( $this->get_property_rent_mode() == 28 ) && ( $this->get_property_singlevalue_rent() != 0 ) ){// single value

			if( $this->get_property_singlevalue_rent_frequency() == 30 ){ // bi-weekly

				$grm = $this->get_property_purchaseprice() / ( 26 * $this->get_property_singlevalue_rent() );

			}else if( $this->get_property_singlevalue_rent_frequency() == 31 ){ // monthly

				$grm = $this->get_property_purchaseprice() / ( 12 * $this->get_property_singlevalue_rent() );

			}else if(  $this->get_property_singlevalue_rent_frequency() == 32 ){ // yearly

				$grm = $this->get_property_purchaseprice() / $this->get_property_singlevalue_rent();

			}

		}

		else if( ( $this->get_property_rent_mode() == 29 ) && ( $this->get_property_sum_of_units_permonth() != 0 ) ){// sum of unit



				$grm = $this->get_property_purchaseprice() / ( $this->get_property_sum_of_units_permonth()*12 );

		}else{
			return $grm;
		}

		return $grm;
	}

	public function get_operating_expense_ratio(){
		$oer = 0;

		$gross_rent = $this->get_gross_rent_peryear();
		$net_income = $this->get_operating_income_peryear() ;// per year
		//return $net_income;
		if( $net_income != 0){
			$oer = ($this->get_property_otherexpense()/$net_income)*100;
		}else{
			return $oer;
		}

		return $oer;
	}

	public function get_debt_coverage_ratio(){
		$dcr = 0;

		$gross_rent = $this->get_gross_rent_peryear();// per year

		$net_income = $this->get_operating_income_peryear() - $this->get_vacancy_loss_peryear();// per year

		$net_income_after_expense = $net_income - $this->get_property_otherexpense();// per year

		if( ( $this->get_payment1() + $this->get_payment2() ) > 0){
			$dcr = ( $net_income_after_expense/12 ) / ( $this->get_payment1() + $this->get_payment2() );
		}else{
			$dcr = 0;
		}


		return $dcr;
	}

	public function get_cap_rate(){
		$cap_rate = 0;

		$gross_rent = $this->get_gross_rent_peryear();// per year

		$net_income = $this->get_operating_income_peryear();// per year

		$net_income_after_expense = $net_income - $this->get_property_otherexpense();// per year

		if( $this->get_property_purchaseprice() != 0 ){
		
			$cap_rate = ( $net_income_after_expense/ $this->get_property_purchaseprice() ) * 100;
			
			return $cap_rate;
		
		}else{
			return $cap_rate;
		}
		
	}

	public function get_cash_on_cash_return(){
		$cash_on_cash_return = 0;

		$gross_rent = $this->get_gross_rent_peryear();// per year

		$net_income = $this->get_operating_income_peryear() - $this->get_vacancy_loss_peryear();// per year

		$net_income_after_expense = $net_income - $this->get_property_otherexpense();// per year

		$cash_flow = ( $net_income_after_expense/12 ) - ( $this->get_payment1() + $this->get_payment2() );

		if( $this->get_initialcash_invested() != 0 ){
			$cash_on_cash_return = ( $this->get_cashflow_peryear() /  $this->get_initialcash_invested()  ) * 100;
			//return ( $cash_flow*12 );
			return $cash_on_cash_return;
		}else{
			return $cash_on_cash_return;
		}
		
		
	}

	public function get_lvt(){
		$lvt = 0;

		$lvt = 100 - $this->get_property_first_loanamount();

		return $lvt;
	}

	public function get_cost_per_squarfeet(){
		$cost_p_sqr = 0;

		if( $this->get_property_squarefeet() != 0){

			$cost_p_sqr = $this->get_property_purchaseprice() / $this->get_property_squarefeet();

		}

		return $cost_p_sqr;
	}

	public function get_gross_rent_permonth(){
		$gross_rent = 0;

		if( $this->get_property_rent_mode() == 28 ){

			if( $this->get_property_singlevalue_rent_frequency() == 30 ){ // bi-weekly

				$gross_rent = ( 26 * $this->get_property_singlevalue_rent() ) / 12;

			}else if( $this->get_property_singlevalue_rent_frequency() == 31 ){ // monthly

				$gross_rent = $this->get_property_singlevalue_rent();

			}else if(  $this->get_property_singlevalue_rent_frequency() == 32 ){ // yearly

				$gross_rent = $this->get_property_singlevalue_rent() / 12;

			}

		}else if( $this->get_property_rent_mode() == 29 ){
			$gross_rent = $this->get_property_sum_of_units_permonth();
		}

		return $gross_rent;
	}

	public function get_gross_rent_peryear(){
		$gross_rent = 0;

		if( $this->get_property_rent_mode() == 28 ){
			if( $this->get_property_singlevalue_rent_frequency() == 30 ){ // bi-weekly

				$gross_rent = 26 * $this->get_property_singlevalue_rent();

			}else if( $this->get_property_singlevalue_rent_frequency() == 31 ){ // monthly

				$gross_rent = 12 * $this->get_property_singlevalue_rent();

			}else if(  $this->get_property_singlevalue_rent_frequency() == 32 ){ // yearly

				$gross_rent = $this->get_property_singlevalue_rent();

			}

		}else if( $this->get_property_rent_mode() == 29 ){
			$gross_rent = $this->get_property_sum_of_units_permonth()*12;
		}


		return $gross_rent;
	}

	public function get_vacancy_loss_permonth(){
		$vacancy_loss_peryear = 0;
		$vacancy_loss_peryear = ( $this->get_property_vacancy_rate()/100 )* $this->get_gross_rent_peryear();
		return $vacancy_loss_peryear/12;
	}

	public function get_vacancy_loss_peryear(){
		$vacancy_loss_peryear = 0;
		$vacancy_loss_peryear = ( $this->get_property_vacancy_rate()/100 )* $this->get_gross_rent_peryear();
		return $vacancy_loss_peryear;
	}

	public function get_operating_income_permonth(){
		$operating_income_permonth = 0;
		$operating_income_permonth = $this->get_gross_rent_permonth() - $this->get_vacancy_loss_permonth();

		$income_items = $this->get_other_income_items();
		foreach( $income_items as $income_item){

			if( $income_item['frequency'] == 30 ){
				$yearly_income = 26*$income_item['amount'];
			}else if( $income_item['frequency'] == 31 ){
				$yearly_income = 12*$income_item['amount'];
			}else if( $income_item['frequency'] == 32 ){
				$yearly_income = $income_item['amount'];
			}

		$operating_income_permonth+= ( $yearly_income/12 );

		}

		return $operating_income_permonth;
	}

	public function get_operating_income_peryear(){
		$operating_income_peryear = 0;
		$operating_income_peryear = $this->get_gross_rent_peryear() - $this->get_vacancy_loss_peryear();

		$income_items = $this->get_other_income_items();
		foreach( $income_items as $income_item){

			if( $income_item['frequency'] == 30 ){
				$yearly_income = 26*$income_item['amount'];
			}else if( $income_item['frequency'] == 31 ){
				$yearly_income = 12*$income_item['amount'];
			}else if( $income_item['frequency'] == 32 ){
				$yearly_income = $income_item['amount'];
			}

			$operating_income_peryear+= $yearly_income;

		}
		
		return $operating_income_peryear;
	}

	public function get_income_table(){
		$income_table = array();

		$income_items = $this->get_other_income_items();
		foreach( $income_items as $income_item){

			if( $income_item['frequency'] == 30 ){
				$yearly_income = 26*$income_item['amount'];
			}else if( $income_item['frequency'] == 31 ){
				$yearly_income = 12*$income_item['amount'];
			}else if( $income_item['frequency'] == 32 ){
				$yearly_income = $income_item['amount'];
			}

			$income_table[] = array(
										'income_name'	=> $income_item['income_name'],
										'monthly'       => $yearly_income/12,
										'yearly'		=> $yearly_income
								   );
		}

		return $income_table;
	}

	public function get_expenses_table(){
		$expenses_table = array();

		$expense_items = $this->get_other_expense_items();
		foreach( $expense_items as $expense_item){
			
			if( $expense_item['frequency'] == 30 ){
				$yearly_expense = 26*$expense_item['amount'];
			}else if( $expense_item['frequency'] == 31 ){
				$yearly_expense = 12*$expense_item['amount'];
			}else if( $expense_item['frequency'] == 32 ){
				$yearly_expense = $expense_item['amount'];
			}

			if( $this->get_operating_income_peryear() != 0){
				$percentage = ( $yearly_expense/$this->get_operating_income_peryear() ) * 100;
			}else{
				$percentage = 0;
			}
			
			
			$expenses_table[] = array(
										'expense_name'	=> $expense_item['expense_name'],
										'percentage'    => $percentage,
										'monthly'       => $yearly_expense/12,
										'yearly'		=> $yearly_expense/1
									 );

		}


		return $expenses_table;
	}

	public function get_net_operation_income_permonth(){
		$net_operation_income_permonth = 0;
		$net_operation_income_permonth = $this->get_operating_income_permonth() - ( $this->get_property_otherexpense()/12 );
		return $net_operation_income_permonth;
	}

	public function get_net_operation_income_peryear(){
		$net_operation_income_peryear = 0;
		$net_operation_income_peryear = $this->get_operating_income_peryear() - ( $this->get_property_otherexpense() );
		return $net_operation_income_peryear;
	}

	public function get_mortgage_permonth(){

		return $this->get_payment1() + $this->get_payment2();
	}

	public function get_mortgage_peryear(){

		return ( $this->get_payment1() + $this->get_payment2() ) * 12;
	}

	public function get_cashflow_permonth(){
		$cashflow_m = 0;
		$cashflow_m = $this->get_net_operation_income_permonth() - $this->get_mortgage_permonth();
		return $cashflow_m;
	}

	public function get_cashflow_peryear(){
		$cashflow_y = 0;
		$cashflow_y = $this->get_net_operation_income_peryear() - $this->get_mortgage_peryear();
		return $cashflow_y;
	}

	public function get_first_mortgage_by_year($year=1){

		$first_mortgage_payment = 0;

		if( $this->get_property_first_loantype() == 25){ // amortizing
			if($year <= $this->get_property_first_term()){
				$first_mortgage_payment = $this->get_payment1()*12;
			}

		}else if( $this->get_property_first_loantype() == 26){ // interested only
			if($year <= $this->get_property_first_interestonly_term() ){
				$first_mortgage_payment = $this->get_payment1()*12;
			}
		}



		return $first_mortgage_payment;
	}

	public function get_second_mortgage_by_year($year=1){

		$second_mortgage_payment = 0;

		if( $this->get_property_second_loantype() == 25){ // amortizing
			if($year <= $this->get_property_second_term()){
				$second_mortgage_payment = $this->get_payment2()*12;
			}

		}else if( $this->get_property_second_loantype() == 26){ // interested only
			if($year <= $this->get_property_second_interestonly_term() ){
				$second_mortgage_payment = $this->get_payment2()*12;
			}
		}

		return $second_mortgage_payment;
	}

	public function get_loan_first_loanbalance_by_year($year=1){

		$first_laon_balance = 0;
		$payment_frequency = $this->get_property_first_payment_frequency();
		$loan_term         = $this->get_property_first_term();
		$loan_int_only_term= $this->get_property_first_interestonly_term();
		$interested_rate   = $this->get_property_first_interestrate();
		$loan_amount       = $this->get_actual_loan_amount1();

		if( $payment_frequency == 22 ){
			$n  = $loan_term * 26;
			$p1 = $year*26;
			$p2 = ($year - $loan_int_only_term)*26;
			$n1 = $loan_int_only_term * 26;
			$n2 = ( $loan_term - $loan_int_only_term ) * 26;
			$int_only_paymentterm=26;
		}else if( $payment_frequency == 23 ){
			$n  = $loan_term * 12;
			$p1  = $year*12;
			$p2 = ($year - $loan_int_only_term)*12;
			$n1 = $loan_int_only_term * 12;
			$n2 = ( $loan_term - $loan_int_only_term ) * 12;
			$int_only_paymentterm=12;
		}else if( $payment_frequency == 24 ){
			$n  = $loan_term;
			$p1  = $year;
			$p2 = $year - $loan_int_only_term;
			$n1 = $loan_int_only_term;
			$n2 = $loan_term - $loan_int_only_term;
			$int_only_paymentterm=1;
		}

		$c = ( ( $interested_rate/100 ) / 12);

		if( $this->get_property_first_loantype() == 25 ){
			$first_laon_balance     = ( $loan_amount * ( pow( (1+$c), $n ) - pow( (1+$c), $p1 ) ) )/( ( pow( (1+$c), $n ) - 1 ) );

		}else if( $this->get_property_first_loantype() == 26){

			if( $year <= $loan_int_only_term){
				$first_laon_balance 	= $loan_amount;
			}else{
				$first_laon_balance     = ( $loan_amount * ( pow( (1+$c), $n2 ) - pow( (1+$c), $p2 ) ) )/( ( pow( (1+$c), $n2 ) - 1 ) );
			}
		}

		return $first_laon_balance;
	}

	public function get_loan_second_loanbalance_by_year($year){
		$second_loan_balance = 0;

		$payment_frequency = $this->get_property_second_payment_frequency();
		$loan_term         = $this->get_property_second_term();
		$loan_int_only_term= $this->get_property_second_interestonly_term();
		$interested_rate   = $this->get_property_second_interestrate();
		$loan_amount       = $this->get_actual_loan_amount2();

		if( $payment_frequency == 22 ){
			$n  = $loan_term * 26;
			$p1 = $year*26;
			$p2 = ($year - $loan_int_only_term)*26;
			$n1 = $loan_int_only_term * 26;
			$n2 = ( $loan_term - $loan_int_only_term ) * 26;
			$int_only_paymentterm=26;
		}else if( $payment_frequency == 23 ){
			$n  = $loan_term * 12;
			$p1  = $year*12;
			$p2 = ($year - $loan_int_only_term)*12;
			$n1 = $loan_int_only_term * 12;
			$n2 = ( $loan_term - $loan_int_only_term ) * 12;
			$int_only_paymentterm=12;
		}else if( $payment_frequency == 24 ){
			$n  = $loan_term;
			$p1  = $year;
			$p2 = $year - $loan_int_only_term;
			$n1 = $loan_int_only_term;
			$n2 = $loan_term - $loan_int_only_term;
			$int_only_paymentterm=1;
		}

		$c = ( ( $interested_rate/100 ) / 12);

		if( $this->get_property_second_loantype() == 25 ){
			$second_laon_balance     = ( $loan_amount * ( pow( (1+$c), $n ) - pow( (1+$c), $p1 ) ) )/( ( pow( (1+$c), $n ) - 1 ) );

		}else if( $this->get_property_second_loantype() == 26){

			if( $year <= $loan_int_only_term){
				$second_laon_balance 	= $loan_amount;
			}else{
				$second_laon_balance     = ( $loan_amount * ( pow( (1+$c), $n2 ) - pow( (1+$c), $p2 ) ) )/( ( pow( (1+$c), $n2 ) - 1 ) );
			}
		}

		return $second_laon_balance;
	}

	public function get_projection(){
		$projection = array();

		// operation_income
		$operation_income = $this->get_operating_income_peryear();

		$projection['operation_income'][1] = $operation_income;

		// operation_expenses
		$operation_expense = $this->get_property_otherexpense();

		$projection['operation_expense'][1] = $operation_expense;

		// net_operation_income
		$projection['net_operation_income'][1] = $projection['operation_income'][1] - $projection['operation_expense'][1];

		// mortgage_payment
		$projection['mortgage_payment'][1]    		=  $this->get_first_mortgage_by_year(1) + $this->get_second_mortgage_by_year(1);

		// cash_flow
		$projection['cash_flow'][1]    				= $projection['net_operation_income'][1] - $projection['mortgage_payment'][1];

		// cap_rate purchase price
		$projection['cap_rate_purchase_price'][1] 	= ($projection['net_operation_income'][1] / $this->get_property_purchaseprice()) * 100;

		// market value
		$projection['market_value'][1]          	= $this->get_property_listingprice() * ( ( $this->get_property_appreciation_rate()/100 ) + 1 );

		// cap_rate market value
		$projection['cap_rate_market_value'][1] 	= ($projection['net_operation_income'][1] / $projection['market_value'][1] ) * 100;

		// cash on cash return
		$projection['cash_on_cash_return'][1]   	=  ( $projection['cash_flow'][1] / ( $this->get_property_purchaseprice() - ( $this->get_actual_loan_amount1() + $this->get_actual_loan_amount2() ) ) ) * 100;

		// loan balance
		$projection['loan_balance'][1]  			= $this->get_loan_first_loanbalance_by_year(1) + $this->get_loan_second_loanbalance_by_year(1);

		// equity
		$projection['equity'][1]                    = $projection['market_value'][1] - $projection['loan_balance'][1];

		// return on equity
		$projection['return_on_equity'][1]          = ($projection['cash_flow'][1] / $projection['equity'][1] ) * 100;

		// loan to value ratio
		$projection['loan_to_value_ratio'][1]       = ($projection['loan_balance'][1] / $projection['market_value'][1]) * 100;

		// potential cash out refinance
		$projection['cash_out_refinance'][1]        = ( $projection['market_value'][1] * ( $this->get_property_lvt_for_refinance() / 100 ) ) - $projection['loan_balance'][1];

		// selling cost
		$projection['selling_cost'][1] 			    = $this->get_sellingcost() * ( ( $this->get_property_appreciation_rate() / 100 ) + 1 );

		// proceed after sell
		$projection['proceed_after_sell'][1]        = $projection['equity'][1] - $projection['selling_cost'][1];

		// accumulate cash flow
		$projection['accumulate_cash_flow'][1]      = $projection['cash_flow'][1];

		// intial cash invested
		$projection['initial_cash_invested']        = $this->get_initialcash_invested();

		// net profit
		$projection['net_profit'][1]                = $projection['proceed_after_sell'][1] + $projection['accumulate_cash_flow'][1] - $projection['initial_cash_invested'];

		// return on invest
		$projection['return_on_invest'][1]          = $projection['net_profit'][1] / $projection['initial_cash_invested'] * 100;

		for( $year = 2 ; $year <= 30; $year++){
			$projection['operation_income'][$year]  	  = $projection['operation_income'][$year-1] * ( ( $this->get_property_incomeinflation_rate()/100 ) + 1 );
			$projection['operation_expense'][$year] 	  = $projection['operation_expense'][$year-1] * ( ( $this->get_property_expenseinflation_rate()/100 ) + 1 );
			$projection['net_operation_income'][$year]    = $projection['operation_income'][$year] - $projection['operation_expense'][$year];
			$projection['mortgage_payment'][$year]        = $this->get_first_mortgage_by_year($year) + $this->get_second_mortgage_by_year($year);
			$projection['cash_flow'][$year]               = $projection['net_operation_income'][$year] - $projection['mortgage_payment'][$year];
			$projection['cap_rate_purchase_price'][$year] = ( $projection['net_operation_income'][$year] / $this->get_property_purchaseprice() )*100;
			$projection['market_value'][$year]            = $projection['market_value'][$year-1] * ( ( $this->get_property_appreciation_rate()/100 ) + 1 );
			$projection['cap_rate_market_value'][$year]   = ($projection['net_operation_income'][$year] / $projection['market_value'][$year] ) * 100;
			$projection['cash_on_cash_return'][$year]     = ( $projection['cash_flow'][$year] /  ( $this->get_property_purchaseprice() - ( $this->get_actual_loan_amount1() + $this->get_actual_loan_amount2() ) ) ) * 100;
			$projection['loan_balance'][$year]            = $this->get_loan_first_loanbalance_by_year($year) + $this->get_loan_second_loanbalance_by_year($year);
			$projection['equity'][$year]                  = $projection['market_value'][$year] - $projection['loan_balance'][$year];
			$projection['return_on_equity'][$year]        = ($projection['cash_flow'][$year] / $projection['equity'][$year]) * 100;
			$projection['loan_to_value_ratio'][$year]     = ($projection['loan_balance'][$year] / $projection['market_value'][$year]) * 100;
			$projection['cash_out_refinance'][$year]      = ( $projection['market_value'][$year] * ( $this->get_property_lvt_for_refinance() / 100 ) ) - $projection['loan_balance'][$year];
			$projection['selling_cost'][$year] 			  = $projection['selling_cost'][$year-1]  *  ( ( $this->get_property_appreciation_rate() / 100 ) + 1 );
			$projection['proceed_after_sell'][$year]      = $projection['equity'][$year] - $projection['selling_cost'][$year];
			$projection['accumulate_cash_flow'][$year]    = $projection['accumulate_cash_flow'][$year-1] + $projection['cash_flow'][$year];
			$projection['net_profit'][$year]              = $projection['proceed_after_sell'][$year] + $projection['accumulate_cash_flow'][$year] - $projection['initial_cash_invested'];
			$projection['return_on_invest'][$year]        = $projection['net_profit'][$year] / $projection['initial_cash_invested'] * 100;
		}





		return $projection;
	}

	public function get_units_info(){

		$units = $this->get_units();
		$output = array();
		$i=0;
		foreach( $units as $unit ){

			$unit_type 	   = $unit->get_unit_type();
			$square_feet   = $unit->get_unit_squarfeet();
			$num_of_units   = $unit->get_number_of_units();

			$rents         = $unit->get_unit_rents();
			$rent          = $rents[0]->get_unit_rent_rent();

			if( $rents[0]->get_unit_rent_frequency() == 30){
				$rent_freq   = 'Bi-weekly';
				$total_rent  = 26*$rent*$num_of_units;
			}else if( $rents[0]->get_unit_rent_frequency() == 31){
				$rent_freq   = 'Month';
				$total_rent  = 12*$rent*$num_of_units;
			}else if( $rents[0]->get_unit_rent_frequency() == 32){
				$rent_freq   = 'Year';
				$total_rent  = $rent*$num_of_units;
			}





  			$output[] = array(
								'unit_type' 	=> $unit_type,
								'square_feet'   => $square_feet,
								'num_of_units'  => $num_of_units,
								'rent'          => $rent,
  								'rent_freq'     => $rent_freq,
  								'total_rent'    => $total_rent
							 );
		}
		return $output;
	}

	public function get_mortgage_amortization_report($loan=1){


		if( $loan == 1 ){ // first mortgage
			$payments= array();
			if( $this->get_actual_loan_amount1() > 0){
				$percent 			= $this->get_property_first_loanamount(); // how much is the down payment
				$interested_rate    = $this->get_property_first_interestrate();
				$loan_term          = $this->get_property_first_term();
				$payment_mode       = $this->get_property_first_payment_frequency(); // bi-weekly,monthly,yearly
				$loan_type          = $this->get_property_first_loantype();
				$int_only_term      = $this->get_property_first_interestonly_term();

				$purchase_price 	= $this->get_property_purchaseprice();

				$loan_amount 		= $purchase_price * ( 1-( $percent/100 ) );

				if( $payment_mode == 22 ){ // bi-weekly
					$n = $loan_term * 26;
					$n1 = $int_only_term * 26;
					$n2 = ( $loan_term - $int_only_term ) * 26;
					$int_only_paymentterm=26;
				}else if( $payment_mode == 23 ){ // monthly
					$n = $loan_term * 12;
					$n1 = $int_only_term * 12;
					$n2 = ( $loan_term - $int_only_term ) * 12;
					$int_only_paymentterm=12;
				}else if( $payment_mode == 24 ){ // yearly
					$n = $loan_term;
					$n1 = $int_only_term;
					$n2 = $loan_term - $int_only_term ;
					$int_only_paymentterm=1;
				}else{
					//error
				}

				$c = ( ( $interested_rate/100 ) / 12);

				if( $loan_type == 25 ){ // amortizing

					$payment_amount = ($loan_amount * ( $c * pow( (1+$c), $n ) ) )  / ( ( pow( (1+$c), $n ) ) - 1 );
					$previous_principal=0;

					for($p=1; $p<=$n; $p++){

						$balance     = ( $loan_amount * ( pow( (1+$c), $n ) - pow( (1+$c), $p ) ) )/( ( pow( (1+$c), $n ) - 1 ) );
						$principal   = $loan_amount - $balance - $previous_principal;
						$interest    = $payment_amount - $principal;
						$previous_principal += $principal;

						if( $payment_mode == 22 ){ // bi-weekly
							$year = intval( ( $p-1 )/26) + 1;
						}else if( $payment_mode == 23 ){ // monthly
							$year = intval( ( $p-1 ) /12) + 1;
						}else if( $payment_mode == 24 ){ // yearly
							$year = $p;
						}else{
							//error
						}

						$payments[$p-1] = array(
								'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
								'year'			 =>  $year,
								'number'		 =>  $p,
								'interest'		 =>  number_format($interest, 2, '.', ''),
								'principal'		 =>  number_format($principal, 2, '.', ''),
								'balance'		 =>  number_format($balance, 2, '.', ''),
						);


					}
				}else if( $loan_type == 26 ){ // interest only

						$payment_amount = ( $loan_amount*( $interested_rate/100 ) ) / $int_only_paymentterm;


						for($p=1; $p<=$n1; $p++){
							$balance 	= $loan_amount;
							$principal  = 0;
							$interest   = $payment_amount;

							if( $payment_mode == 22 ){ // bi-weekly
								$year = intval( ( $p-1 )/26) + 1;
							}else if( $payment_mode == 23 ){ // monthly
								$year = intval( ( $p-1 ) /12) + 1;
							}else if( $payment_mode == 24 ){ // yearly
								$year = $p;
							}else{
								//error
							}

							$payments[$p-1] = array(
													'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
													'year'			 =>  $year,
													'number'		 =>  $p,
													'interest'		 =>  number_format($interest, 2, '.', ''),
													'principal'		 =>  number_format($principal, 2, '.', ''),
													'balance'		 =>  number_format($balance, 2, '.', ''),
												   );
						}

						$payment_amount2 = ($loan_amount * ( $c * pow( (1+$c), $n2 ) ) )  / ( ( pow( (1+$c), $n2 ) ) - 1 );
						$previous_principal=0;

						$index = $n1;
						for($p=1; $p<=$n2; $p++){
							$balance     = ( $loan_amount * ( pow( (1+$c), $n2 ) - pow( (1+$c), $p ) ) )/( ( pow( (1+$c), $n2 ) - 1 ) );
							$principal   = $loan_amount - $balance - $previous_principal;
							$interest    = $payment_amount2 - $principal;
							$previous_principal += $principal;

							if( $payment_mode == 22 ){
								$year = intval( ( $p-1 )/26) + 1 + $int_only_term;
							}else if( $payment_mode == 23 ){
								$year = intval( ( $p-1 ) /12) + 1 + $int_only_term;
							}else if( $payment_mode == 24 ){
								$year = $p + $int_only_term;
							}else{
								//error
							}

							$payments[$index] = array(
									'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
									'year'			 =>  $year,
									'number'		 =>  $index+1,
									'interest'		 =>  number_format($interest, 2, '.', ''),
									'principal'		 =>  number_format($principal, 2, '.', ''),
									'balance'		 =>  number_format($balance, 2, '.', ''),
							);

							$index++;
						}
				}// end interest only
			}// end loan amount more than 0


		}elseif( $loan == 2 ){ // second mortgage

			$payments= array();
			if( $this->get_actual_loan_amount2() > 0){


					$percent 			= $this->get_property_second_loanamount(); // how much is the down payment
					$interested_rate    = $this->get_property_second_interestrate();
					$loan_term          = $this->get_property_second_term();
					$payment_mode       = $this->get_property_second_payment_frequency(); // bi-weekly,monthly,yearly
					$loan_type          = $this->get_property_second_loantype();
					$int_only_term      = $this->get_property_second_interestonly_term();

					$purchase_price 	= $this->get_property_purchaseprice();

					$loan_amount 		= $purchase_price * ( 1-( $percent/100 ) );

					if( $payment_mode == 22 ){ // bi-weekly
						$n = $loan_term * 26;
						$n1 = $int_only_term * 26;
						$n2 = ( $loan_term - $int_only_term ) * 26;
						$int_only_paymentterm=26;
					}else if( $payment_mode == 23 ){ // monthly
						$n = $loan_term * 12;
						$n1 = $int_only_term * 12;
						$n2 = ( $loan_term - $int_only_term ) * 12;
						$int_only_paymentterm=12;
					}else if( $payment_mode == 24 ){ // yearly
						$n = $loan_term;
						$n1 = $int_only_term;
						$n2 = $loan_term - $int_only_term ;
						$int_only_paymentterm=1;
					}else{
						//error
					}

					$c = ( ( $interested_rate/100 ) / 12);

					if( $loan_type == 25 ){ // amortizing

						$payment_amount = ($loan_amount * ( $c * pow( (1+$c), $n ) ) )  / ( ( pow( (1+$c), $n ) ) - 1 );
						$previous_principal=0;

						for($p=1; $p<=$n; $p++){

							$balance     = ( $loan_amount * ( pow( (1+$c), $n ) - pow( (1+$c), $p ) ) )/( ( pow( (1+$c), $n ) - 1 ) );
							$principal   = $loan_amount - $balance - $previous_principal;
							$interest    = $payment_amount - $principal;
							$previous_principal += $principal;

							if( $payment_mode == 22 ){ // bi-weekly
								$year = intval( ( $p-1 )/26) + 1;
							}else if( $payment_mode == 23 ){ // monthly
								$year = intval( ( $p-1 ) /12) + 1;
							}else if( $payment_mode == 24 ){ // yearly
								$year = $p;
							}else{
								//error
							}

							$payments[$p-1] = array(
									'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
									'year'			 =>  $year,
									'number'		 =>  $p,
									'interest'		 =>  number_format($interest, 2, '.', ''),
									'principal'		 =>  number_format($principal, 2, '.', ''),
									'balance'		 =>  number_format($balance, 2, '.', ''),
							);


						}
					}else if( $loan_type == 26 ){ // interest only

						$payment_amount = ( $loan_amount*( $interested_rate/100 ) ) / $int_only_paymentterm;


						for($p=1; $p<=$n1; $p++){
							$balance 	= $loan_amount;
							$principal  = 0;
							$interest   = $payment_amount;

							if( $payment_mode == 22 ){ // bi-weekly
								$year = intval( ( $p-1 )/26) + 1;
							}else if( $payment_mode == 23 ){ // monthly
								$year = intval( ( $p-1 ) /12) + 1;
							}else if( $payment_mode == 24 ){ // yearly
								$year = $p;
							}else{
								//error
							}

							$payments[$p-1] = array(
									'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
									'year'			 =>  $year,
									'number'		 =>  $p,
									'interest'		 =>  number_format($interest, 2, '.', ''),
									'principal'		 =>  number_format($principal, 2, '.', ''),
									'balance'		 =>  number_format($balance, 2, '.', ''),
							);
						}

						$payment_amount2 = ($loan_amount * ( $c * pow( (1+$c), $n2 ) ) )  / ( ( pow( (1+$c), $n2 ) ) - 1 );
						$previous_principal=0;

						$index = $n1;
						for($p=1; $p<=$n2; $p++){
							$balance     = ( $loan_amount * ( pow( (1+$c), $n2 ) - pow( (1+$c), $p ) ) )/( ( pow( (1+$c), $n2 ) - 1 ) );
							$principal   = $loan_amount - $balance - $previous_principal;
							$interest    = $payment_amount2 - $principal;
							$previous_principal += $principal;

							if( $payment_mode == 22 ){
								$year = intval( ( $p-1 )/26) + 1 + $int_only_term;
							}else if( $payment_mode == 23 ){
								$year = intval( ( $p-1 ) /12) + 1 + $int_only_term;
							}else if( $payment_mode == 24 ){
								$year = $p + $int_only_term;
							}else{
								//error
							}

							$payments[$index] = array(
									'onetime_payment'=>  number_format($payment_amount, 2, '.', ''),
									'year'			 =>  $year,
									'number'		 =>  $index+1,
									'interest'		 =>  number_format($interest, 2, '.', ''),
									'principal'		 =>  number_format($principal, 2, '.', ''),
									'balance'		 =>  number_format($balance, 2, '.', ''),
							);

							$index++;
						}
					}// end interest only

			}// end loan amount more than 0

		}


		return $payments;
	}


/*
	public function get_rent_projection(){
		return 0;
	}
*/

    ////////////////////////////////

    public function set_property_id($property_id){
    	$this->property_id = $property_id;
    }

    public function set_property_name($property_name){
    	$this->property_name  = $property_name;
    }

    public function set_property_street($property_street){
    	$this->property_street  = $property_street;
    }

    public function set_property_city($property_city){
    	$this->property_city  = $property_city;
    }

    public function set_property_state($property_state){
    	$this->property_state  = $property_state;
    }

    public function set_property_zip($property_zip){
    	$this->property_zip  = $property_zip;
    }

    public function set_property_country($property_country){
    	$this->property_country  = $property_country;
    }

    public function set_property_mls($property_mls){
    	$this->property_mls = $property_mls;
    }

    public function set_property_style($property_style){
    	$this->property_style = $property_style;
    }

    public function set_property_squarefeet($property_squarefeet){
       	$this->property_squarefeet = $property_squarefeet;
    }

    public function set_property_lotsize($property_lotsize){
    	$this->property_lotsize = $property_lotsize;
    }

    public function set_property_yearbuilt($property_yearbuilt){
    	$this->property_yearbuilt = $property_yearbuilt;
    }

    public function set_property_lastremodel($property_lastremodel){
    	$this->property_lastremodel = $property_lastremodel;
    }

    public function set_property_parking($property_parking){
    	$this->property_parking = $property_parking;
    }

    public function set_property_hoa($property_hoa){
    	$this->property_hoa = $property_hoa;
    }

    public function set_property_listingprice($property_listingprice){
    	$this->property_listingprice = $property_listingprice;
    }

    public function set_property_initimprove($property_initimprove){
    	$this->property_initimprove = $property_initimprove;
    }

    public function set_property_purchaseprice($property_purchaseprice){
    	$this->property_purchaseprice = $property_purchaseprice;
    }

    public function set_property_first_loanamount($property_first_loanamount){
    	$this->property_first_loanamount = $property_first_loanamount;
    }

    public function set_property_first_loantype($property_first_loantype){
    	$this->property_first_loantype = $property_first_loantype;
    }

    public function set_property_first_loantype_text($first_loantype_text){
    	$this->property_first_loantype_text = $first_loantype_text;

    }

    public function set_property_first_interestrate($property_first_interestrate){
		$this->property_first_interestrate = $property_first_interestrate;
    }

    public function set_property_first_payment_frequency($property_first_payment_frequency){
    	$this->property_first_payment_frequency = $property_first_payment_frequency;
    }

    public function set_property_first_term($property_first_term){
    	$this->property_first_term = $property_first_term;
    }

    public function set_property_first_interestonly_term($property_first_interestonly_term){
    	$this->property_first_interestonly_term = $property_first_interestonly_term;
    }

    public function set_property_second_loanamount($property_second_loanamount){
    	$this->property_second_loanamount = $property_second_loanamount;
    }

    public function set_property_second_loantype($property_second_loantype){
    	$this->property_second_loantype = $property_second_loantype;
    }

    public function set_property_second_loantype_text($second_loantype_text){
    	$this->property_second_loantype_text = $second_loantype_text;
    }

    public function set_property_second_interestrate($property_second_interestrate){
    	$this->property_second_interestrate = $property_second_interestrate;
    }

    public function set_property_second_payment_frequency($property_second_payment_frequency){
    	$this->property_second_payment_frequency = $property_second_payment_frequency;
    }

    public function set_property_second_term($property_second_term){
    	$this->property_second_term = $property_second_term;
    }

    public function set_property_second_interestonly_term($property_second_interestonly_term){
    	$this->property_second_interestonly_term = $property_second_interestonly_term;
    }

    public function set_property_rent_mode($rent_mode){
    	$this->property_rent_mode = $rent_mode;
    }

    public function set_property_vacancy_rate($property_vacancy_rate){
    	$this->property_vacancy_rate = $property_vacancy_rate;
    }

    public function set_property_singlevalue_rent($property_singlevalue_rent){
       	$this->property_singlevalue_rent = $property_singlevalue_rent;
    }

    public function set_property_singlevalue_rent_frequency($property_singlevalue_rent_frequency){
    	$this->property_singlevalue_rent_frequency = $property_singlevalue_rent_frequency;
    }

 	public function set_property_appreciation_rate($property_appreciation_rate){
    	$this->property_appreciation_rate = $property_appreciation_rate;
    }

    public function set_property_incomeinflation_rate($property_incomeinflation_rate){
    	$this->property_incomeinflation_rate = $property_incomeinflation_rate;
    }

    public function set_property_expenseinflation_rate($property_expenseinflation_rate){
    	$this->property_expenseinflation_rate = $property_expenseinflation_rate;
    }

    public function set_property_lvt_for_refinance($property_lvt_for_refinance){
    	$this->property_lvt_for_refinance = $property_lvt_for_refinance;
    }

    public function set_property_compounding_period($property_compounding_period){
    	$this->property_compounding_period = $property_compounding_period;
    }

    public function set_property_cover_photo_path($property_cover_photo_path){
    	$this->property_cover_photo_path = $property_cover_photo_path;
    }

    public function set_member_id($member_id){
    	$this->member_id  = $member_id;
    }

    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }

    public function set_buyingcost_items($buyingcost_items){
    	$this->buyingcost_items = $buyingcost_items;
    }

    public function set_sellingcost_items($sellingcost_items){
    	$this->sellingcost_items = $sellingcost_items;
    }

    public function set_other_income_items($other_income_items){
    	$this->other_income_items = $other_income_items;
    }

    public function set_other_expense_items($other_expense_items){
    	$this->other_expense_items = $other_expense_items;
    }

    public function set_units($units){
    	$this->units = $units;
    }

    public function set_fullpage_photos($fullpage_photos){
    	$this->fullpage_photos = $fullpage_photos;
    }

    public function set_property_videos($property_videos){
    	$this->property_videos = $property_videos;
    }
    ///////////////////

    public function add_buyingcost_item(Propertybuyingcostitem $buyingcost_item){
		$this->buyingcost_items[] = $buyingcost_item;
    }

}
