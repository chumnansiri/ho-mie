<?php

namespace Index\Model;

class Membershiptype{
	
	private $membership_type_id;
	private $membership_type_name;
	private $membership_type_cost;
	private $membership_type_period;
	private $num_of_properties;
	private $num_of_email_per_month;
	private $level;
	private $sort_by;
	private $created_date;
	private $last_modified;
	
	public function __construct(Membershiptype $membershiptypes=null){
	
		if($membershiptypes){
	
			$this->membership_type_id      	= $membershiptypes->get_membership_type_id();
			$this->membership_type_name 	= $membershiptypes->get_membership_type_name();
			$this->membership_type_cost     = $membershiptypes->get_membership_type_cost();
			$this->membership_type_period   = $membershiptypes->get_membership_type_period();
			$this->num_of_properties		= $membershiptypes->get_num_of_properties();
			$this->num_of_email_per_month   = $membershiptypes->get_num_of_email_per_month();
			$this->level                    = $membershiptypes->get_level();			
			$this->sort_by        			= $membershiptypes->get_sort_by();
			$this->created_date   			= $membershiptypes->get_created_date();
			$this->last_modified  			= $membershiptypes->get_last_modified();
		
		}
	}
	
	public function exchangeArray($data)
	{
		
		$this->membership_type_id          	=    (		isset(  $data['membership_type_id']) 			) ? 	$data['membership_type_id'] 			: 	null;
		$this->membership_type_name        	=    (		isset(  $data['membership_type_name']) 			) ? 	$data['membership_type_name'] 			: 	null;
		$this->membership_type_cost        	=    (		isset(  $data['membership_type_cost']) 			) ? 	$data['membership_type_cost'] 			: 	null;
		$this->membership_type_period       =    (		isset(  $data['membership_type_period']) 		) ? 	$data['membership_type_period'] 		: 	null;
		$this->num_of_properties        	=    (		isset(  $data['num_of_properties']) 			) ? 	$data['num_of_properties'] 				: 	null;
		$this->num_of_email_per_month       =    (		isset(  $data['num_of_email_per_month']) 		) ? 	$data['num_of_email_per_month'] 		: 	null;
		$this->level        				=    (		isset(  $data['level']) 						) ? 	$data['level'] 							: 	null;
		$this->sort_by      				=    (		isset(  $data['sort_by']) 						) ? 	$data['sort_by'] 						: 	null;
		$this->created_date      			=    (		isset(  $data['created_date']) 					) ? 	$data['created_date'] 					: 	null;
		$this->last_modified      			=    (		isset(  $data['last_modified']) 				) ? 	$data['last_modified'] 					: 	null;
	
	}
	
	
	public function get_membership_type_id(){
		return $this->membership_type_id;
	}
	
	public function get_membership_type_name(){
		return $this->membership_type_name;
	}
	
	public function get_membership_type_cost(){
		return $this->membership_type_cost;
	}
	
	public function get_membership_type_period(){
		return $this->membership_type_period;
	}
	
	public function get_num_of_properties(){
		return $this->num_of_properties;
	}
	
	public function get_num_of_email_per_month(){
		return $this->num_of_email_per_month;
	}
	
	public function get_level(){
		return $this->level;
	}
	
	public function get_sort_by(){
		return $this->sort_by;
	}
	
	public function get_created_date(){
		return $this->created_date;
	}
	
	public function get_last_modified(){
		return $this->last_modified;
	}
	////////////////////////////////
	
	public function set_membership_type_id($membership_type_id){
		$this->membership_type_id = $membership_type_id;
	}
	
	public function set_membership_type_name($membership_type_name){
		$this->membership_type_name = $membership_type_name;
	}
	
	public function set_membership_type_cost($membership_type_cost){
		 $this->membership_type_cost = $membership_type_cost;
	}
	
	public function set_membership_type_period($membership_type_period){
		 $this->membership_type_period = $membership_type_period;
	}
	
	public function set_num_of_properties($num_of_properties){
		 $this->num_of_properties = $num_of_properties;
	}
	
	public function set_num_of_email_per_month($num_of_email_per_month){
		 $this->num_of_email_per_month = $num_of_email_per_month;
	}
	
	public function set_level($level){
		 $this->level = $level;
	}
	
	public function set_sort_by( $sort_by){
		 $this->sort_by = $sort_by;
	}
	
	public function set_created_date( $created_date ){
		 $this->created_date = $created_date;
	}
	
	public function set_last_modified( $last_modified ){
		 $this->last_modified = $last_modified;
	}
}

?>
