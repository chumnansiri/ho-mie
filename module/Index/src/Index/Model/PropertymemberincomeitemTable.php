<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class PropertymemberincomeitemTable{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{

		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function isUsed($income_id){
		
		$resultSet = $this->tableGateway->select(array(
													   'income_id'			=> $income_id
													  ));
		
		if( $resultSet->count() ){
			// income_id is being used		
			return 1;		
		}else{
			return 0;
		}
		
	}
	
	public function getProperty_member_income_item($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('property_income_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getId($property_id,$income_id){

		$resultSet = $this->tableGateway->select(array('property_id' 		=> $property_id,
													   'income_id'			=> $income_id
		));

		if( $resultSet->count() ){

			foreach($resultSet as $result){

				$output = $result->get_property_income_id();

			}

			return $output;

		}else{
			return 0;
		}
	}

	public function saveProperty_member_income_item(Propertymemberincomeitem $propertymemberincomeitem)
	{


		$property_income_id = (int)$propertymemberincomeitem->get_property_income_id();
		if ($property_income_id == 0) {

			$data = array(
					'property_id'  	    => $propertymemberincomeitem->get_property_id(),
					'income_id'  		=> $propertymemberincomeitem->get_income_id(),
					'amount'			=> $propertymemberincomeitem->get_amount(),
					'frequency'         => $propertymemberincomeitem->get_frequency(),
					'sort_by'			=> $propertymemberincomeitem->get_sort_by(),
					'created_date'		=> date('Y-m-d H:i:s'),
					'last_modified' 	=> date('Y-m-d H:i:s')
			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'property_id'  	    => $propertymemberincomeitem->get_property_id(),
					'income_id'  		=> $propertymemberincomeitem->get_income_id(),
					'amount'			=> $propertymemberincomeitem->get_amount(),
					'frequency'         => $propertymemberincomeitem->get_frequency(),
					'sort_by'			=> $propertymemberincomeitem->get_sort_by(),
					'created_date'		=> $propertymemberincomeitem->get_created_date(),
					'last_modified' 	=> date('Y-m-d H:i:s')

			);

			if ($this->getProperty_member_income_item($property_income_id)) {
				$this->tableGateway->update($data, array('property_income_id' => $property_income_id));
				return $property_income_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteProperty_member_income_item($property_income_id){

		$this->tableGateway->delete(array(
				'property_income_id' 		=> $property_income_id,
		));

	}
	
	public function deleteProperty_member_income_item_by_property_id($property_id){
	
		$this->tableGateway->delete(array(
				'property_id' 		=> $property_id,
		));
	
	}

}

?>

