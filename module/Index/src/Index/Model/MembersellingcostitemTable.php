<?php


namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class MembersellingcostitemTable{

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getMembersellingcostitem($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('sellingcost_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getMembersellingcostitems($member_id){

		$where = array(
				'member_id'	=> $member_id,

		);

		$resultSet = $this->tableGateway->select($where);

		return $resultSet;
	}

	public function saveMembersellingcostitem(Membersellingcostitem $membersellingcostitem)
	{

		
		$sellingcost_id = (int)$membersellingcostitem->get_sellingcost_id();
		if ($sellingcost_id == 0) {

			$data = array(
					'sellingcost_name'  	=> $membersellingcostitem->get_sellingcost_name(),
					'member_id'  			=> $membersellingcostitem->get_member_id(),
					'sort_by'				=> $membersellingcostitem->get_sort_by(),
					'created_date'			=> date('Y-m-d H:i:s'),
					'last_modified' 		=> date('Y-m-d H:i:s')
			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'sellingcost_name'  	=> $membersellingcostitem->get_sellingcost_name(),
					'member_id'  			=> $membersellingcostitem->get_member_id(),
					'sort_by'				=> $membersellingcostitem->get_sort_by(),
					'created_date'			=> $membersellingcostitem->get_created_date(),
					'last_modified' 		=> date('Y-m-d H:i:s')
			);

			if ($this->getMemberbuyingcostitem($sellingcost_id)) {
				$this->tableGateway->update($data, array('sellingcost_id' => $sellingcost_id));
				return $sellingcost_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteMembersellingcostitem($sellingcost_id)
	{
		$this->tableGateway->delete(array('sellingcost_id' => $sellingcost_id));
	}

}