<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class PropertysellingcostitemTable {
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{

		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	
	public function isUsed( $sellingcost_id ){
	
		$resultSet = $this->tableGateway->select(array( 'sellingcost_id'	=> $sellingcost_id
		));
	
		if( $resultSet->count() ){
			// buying is being used
			return 1;
	
		}else{
			return 0;
		}
		
	}

	public function getProperty_sellingcost_item($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('property_sellingcost_item_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}


	public function getId($property_id,$sellingcost_id){

		$resultSet = $this->tableGateway->select(array('property_id' 		=> $property_id,
													   'sellingcost_id'		=> $sellingcost_id
													  ));

		if( $resultSet->count() ){

			foreach($resultSet as $result){

				$output = $result->get_property_sellingcost_item_id();

			}

			return $output;

		}else{
			return 0;
		}



	}

	public function saveProperty_sellingcost_item(Propertysellingcostitem $property_sellingcost_item)
	{
		
		$property_sellingcost_item_id = (int)$property_sellingcost_item->get_property_sellingcost_item_id();
		if ($property_sellingcost_item_id == 0) {


			$data = array(

					'property_id'  	    				=> $property_sellingcost_item->get_property_id(),
					'sellingcost_id'  					=> $property_sellingcost_item->get_sellingcost_id(),
					'property_sellingcost_item_amount'	=> $property_sellingcost_item->get_property_sellingcost_item_amount(),
					'sort_by'							=> $property_sellingcost_item->get_sort_by(),
					'created_date'						=> date('Y-m-d H:i:s'),
					'last_modified' 					=> date('Y-m-d H:i:s')

			);


			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			if ($this->getProperty_sellingcost_item($property_sellingcost_item_id)) {
					$data = array(
							'property_id'  	    				=> $property_sellingcost_item->get_property_id(),
							'sellingcost_id'  					=> $property_sellingcost_item->get_sellingcost_id(),
							'property_sellingcost_item_amount'	=> $property_sellingcost_item->get_property_sellingcost_item_amount(),
							'sort_by'							=> $property_sellingcost_item->get_sort_by(),
							'created_date'						=> $property_sellingcost_item->get_created_date(),
							'last_modified' 					=> date('Y-m-d H:i:s')
				);
				$this->tableGateway->update($data, array('property_sellingcost_item_id' => $property_sellingcost_item_id));
				return $property_sellingcost_item_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}
	public function deleteProperty_sellingcost_item($property_sellingcost_item_id){

		$this->tableGateway->delete(array(
											'property_sellingcost_item_id' 		=> $property_sellingcost_item_id,
										));

	}

	public function deleteProperty_sellingcost_item_by_property_id( $property_id ){
	
		$this->tableGateway->delete(array(
				'property_id' 		=> $property_id,
		));
	
	}




}

?>

