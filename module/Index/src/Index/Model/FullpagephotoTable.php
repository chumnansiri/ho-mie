<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class FullpagephotoTable {
	protected $FullpagephototableGateway;
	protected $adapter;

	public function __construct(TableGateway $FullpagephototableGateway, $adapter )
	{
		$this->FullpagephototableGateway 		= $FullpagephototableGateway;
		$this->adapter                  		= $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->FullpagephototableGateway->select();
		return $resultSet;
	}

	public function getFullpagephoto($id)
	{
		$id  = (int) $id;
		$rowset = $this->FullpagephototableGateway->select(array('fullpage_photo_id' => $id));
		$fullpagephoto = $rowset->current();
		if (!$fullpagephoto) {
			throw new \Exception("Could not find row $id");
		}
		return $fullpagephoto;
	}


	public function saveFullpagephoto(Fullpagephoto $fullpagephoto)
	{

		

		$fullpage_photo_id = (int)$fullpagephoto->get_fullpage_photo_id();
		if ($fullpage_photo_id == 0) {

			$data = array(
					'property_id' 			=> $fullpagephoto->get_property_id(),
					'fullpage_photo_path'   => $fullpagephoto->get_fullpage_photo_path(),
					'sort_by'				=> $fullpagephoto->get_sort_by(),
					'created_date'			=> date('Y-m-d H:i:s'),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			$this->FullpagephototableGateway->insert($data);
			return $this->FullpagephototableGateway->lastInsertValue;
		} else {

			$data = array(
					'property_id'   		=> $fullpagephoto->get_property_id(),
					'fullpage_photo_path'   => $fullpagephoto->get_fullpage_photo_path(),
					'sort_by'				=> $fullpagephoto->get_sort_by(),
					'created_date'			=> $fullpagephoto->get_created_date(),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			if ($this->getFullpagephoto($fullpage_photo_id) ) {
				$this->FullpagephototableGateway->update($data, array('fullpage_photo_id' => $fullpage_photo_id));
				return $fullpage_photo_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteFullpagephoto($fullpage_photo_id)
	{
		$this->FullpagephototableGateway->delete( array('fullpage_photo_id' => $fullpage_photo_id) );

	}

}

?>

