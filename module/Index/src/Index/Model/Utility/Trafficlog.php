<?php
namespace Index\Model\Utility;

use Zend\Db\TableGateway\TableGateway;


class Trafficlog{
	protected $traffic_logsGateway;
	
	public function __construct(TableGateway $traffic_logsGateway)
	{
		$this->traffic_logsGateway   	= $traffic_logsGateway;		
	}
	
	public function log($trafficlog)
	{
		
			$data = array(	
					'remote_addr'     => $trafficlog['remote_addr'],
					'http_user_agent' => $trafficlog['http_user_agent'],					
					'created_date'	  => date('Y-m-d H:i:s'),	
			);
	
			$this->traffic_logsGateway->insert($data);
			return $this->traffic_logsGateway->lastInsertValue;
		
	}
	
}


?>