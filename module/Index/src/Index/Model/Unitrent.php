<?php
namespace Index\Model;

// Add these import statements

class Unitrent{

	private $unit_rent_id;
	private $unit_id;
	private $unit_rent_rent;
	private $unit_rent_frequency;
	private $year;
	private $sort_by;
	private $created_date;
	private $last_modified;

	public function __construct(){

	}

	public function exchangeArray($data)
    {
        $this->unit_rent_id     		 =    (		isset(  $data['unit_rent_id']) 			) ? 	$data['unit_rent_id'] 			: 	null;
        $this->unit_id      			 =    (		isset(  $data['unit_id']) 				) ? 	$data['unit_id'] 			    : 	null;
        $this->unit_rent_rent      		 =    (		isset(  $data['unit_rent_rent']) 		) ? 	$data['unit_rent_rent'] 		: 	null;
        $this->unit_rent_frequency       =    (		isset(  $data['unit_rent_frequency']) 	) ? 	$data['unit_rent_frequency'] 	: 	null;
        $this->year      			 	 =    (		isset(  $data['year']) 					) ? 	$data['year'] 			    	: 	null;
        $this->sort_by      			 =    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 				: 	null;
        $this->created_date      		 =    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 			: 	null;
        $this->last_modified      	     =    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 			: 	null;

    }


    public function get_unit_rent_id(){
		return $this->unit_rent_id;
    }

    public function get_unit_id(){
		return $this->unit_id;
    }

    public function get_unit_rent_rent(){
		return $this->unit_rent_rent;
    }

    public function get_unit_rent_frequency(){
		return $this->unit_rent_frequency;
    }

    public function get_year(){
    	return $this->year;
    }

    public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_unit_rent_id($unit_rent_id){
    	$this->unit_rent_id = $unit_rent_id;
    }

    public function set_unit_id($unit_id){
    	$this->unit_id  = $unit_id;
    }

    public function set_unit_rent_rent($unit_rent_rent){
    	$this->unit_rent_rent  = $unit_rent_rent;
    }

    public function set_unit_rent_frequency($unit_rent_frequency){
    	$this->unit_rent_frequency = $unit_rent_frequency;
    }

    public function set_year($year){
    	$this->year  = $year;
    }

    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }







}
