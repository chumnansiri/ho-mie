<?php
namespace Index\Model;

// Add these import statements

class Propertyvideo{

	private $property_video_id;
	private $property_id;
	private $video_path;
	private $sort_by;
	private $created_date;
	private $last_modified;

	public function __construct(Propertyvideo $propertyvideo=null){
		if( $propertyvideo ){
			$this->property_video_id   =        $propertyvideo->get_property_video_id();
			$this->property_id         = 	    $propertyvideo->get_property_id();
			$this->video_path 		   =        $propertyvideo->get_video_path();
			$this->sort_by             =        $propertyvideo->get_sort_by();
			$this->created_date        =        $propertyvideo->get_created_date();
			$this->last_modified       =        $propertyvideo->get_last_modified();
		}
	}

	public function exchangeArray($data)
    {
        $this->property_video_id     	=    (		isset(  $data['property_video_id']) 	) ? 	$data['property_video_id'] 	: 	null;
        $this->property_id     			=    (		isset(  $data['property_id']) 			) ? 	$data['property_id'] 		: 	null;
        $this->video_path      			=    (		isset(  $data['video_path']) 			) ? 	$data['video_path']			: 	null;
        $this->sort_by      			=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date      		=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified      		=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;

    }


    public function get_property_video_id(){
		return $this->property_video_id;
    }

    public function get_property_id(){
    	return $this->property_id;
    }

  	public function get_video_path(){
  		return $this->video_path;
  	}

    public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_property_video_id($property_video_id){
		$this->fullpage_photo_id = $property_video_id;
    }

    public function set_property_id($property_id){
    	$this->property_id = $property_id;
    }

    public function set_video_path($video_path){
    	$this->video_path = $video_path;
    }

    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }

}
