<?php

namespace Index\Model;


use Zend\Db\TableGateway\TableGateway;
use Index\Model\Surway;

class SurwayTable {
	
	protected $surway_tableGateway;
	protected $adapter;
	
	public function __construct(TableGateway $surway_tableGateway,$adapter)
	{
		$this->surway_tableGateway   	= $surway_tableGateway;
		$this->adapter 				 	= $adapter;
	}
	
	public function fetchAll()
	{
	
		$resultSet = $this->surway_tableGateway->select();
		return $resultSet;
	}
	
	public function getSurway($id)
	{
		$id  = (int) $id;
		$rowset = $this->surway_tableGateway->select(array('id' => $id));
		$surway = $rowset->current();
	
		if (!$surway ) {
			throw new \Exception("Could not find row $id from surway table");
		}
	
		return $surway;
	}
	
	public function saveSurway(Surway $surway)
	{
	
	
	
		$id = (int)$surway->get_id();
		if ($id == 0) {
	
			$data = array(
					'id'			=> $surway->get_id(),
					'member_id'     => $surway->get_member_id(),			
					'created_date'	=> date('Y-m-d H:i:s')
					
	
			);
	
			$this->surway_tableGateway->insert($data);
			return $this->surway_tableGateway->lastInsertValue;
		} else {
	
			$data = array(
					'id'			=> $surway->get_id(),
					'member_id'     => $surway->get_member_id(),
					'created_date'	=> $surway->get_created_date()
	
			);
	
			if ($this->getSurway($id)) {
				$this->surway_tableGateway->update($data, array('id' => $id));
				return $id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}
}

?>