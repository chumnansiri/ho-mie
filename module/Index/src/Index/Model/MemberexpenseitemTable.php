<?php


namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class MemberexpenseitemTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getMemberexpenseitem($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('expense_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getMemberexpenseitems($member_id){

		$where = array(
				'member_id'	=> $member_id,

		);

		$resultSet = $this->tableGateway->select($where);

		return $resultSet;
	}

	public function saveMemberexpenseitem(Memberexpenseitem $memberexpenseitem)
	{


		$expense_id = (int)$memberexpenseitem->get_expense_id();
		if ($expense_id == 0) {

			$data = array(
					'expense_name'  	=> $memberexpenseitem->get_expense_name(),
					'member_id'  	=> $memberexpenseitem->get_member_id(),
					'sort_by'		=> $memberexpenseitem->get_sort_by(),
					'created_date'	=> date('Y-m-d H:i:s'),
					'last_modified' => date('Y-m-d H:i:s')
			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'expense_name'  	=> $memberexpenseitem->get_expense_name(),
					'member_id'  	=> $memberexpenseitem->get_member_id(),
					'sort_by'		=> $memberexpenseitem->get_sort_by(),
					'created_date'	=> $memberexpenseitem->get_created_date(),
					'last_modified' => date('Y-m-d H:i:s')
			);

			if ($this->getMemberexpenseitem($expense_id)) {
				$this->tableGateway->update($data, array('expense_id' => $expense_id));
				return $expense_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteMemberexpenseitem($expense_id)
	{
		$this->tableGateway->delete(array('expense_id' => $expense_id));
	}

}