<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;
use Index\Model\Membersubscription;

class MembersubscriptionTable {
	
	protected $membersubscription_tableGateway;
	protected $adapter;
	
	public function __construct(TableGateway $membersubscription_tableGateway,$adapter)
	{
		$this->membersubscription_tableGateway   	= $membersubscription_tableGateway;
		$this->adapter 				 				= $adapter;
	}
	
	public function fetchAll()
	{
	
		$resultSet = $this->membersubscription_tableGateway->select();
		return $resultSet;
	}
	
	public function getMembersubscription($id)
	{
		$id  = (int) $id;
		$rowset = $this->membersubscription_tableGateway->select(array('member_subscription_id' => $id));
		$membersubscription = $rowset->current();
	
		if (!$membersubscription ) {
			throw new \Exception("Could not find row $id from member subscriptions table");
		}
		
		return $membersubscription;
	}
	
	
	public function saveMembersubscription(Membersubscription $membersubscription)
	{
		
		$member_subscription_id = (int)$membersubscription->get_member_subscription_id();
		if ($member_subscription_id == 0) {
			
			$data = array(
					
					'member_id'       				=> $membersubscription->get_member_id(),
					'membership_type_id'			=> $membersubscription->get_membership_type_id(),
					'purchased_date'				=> $membersubscription->get_purchased_date(),
					'start_date' 					=> $membersubscription->get_start_date(),
					'stop_date' 					=> $membersubscription->get_stop_date(),
					'num_of_properties'             => $membersubscription->get_num_of_properties(),
					'num_of_email_per_month'        => $membersubscription->get_num_of_email_per_month(),
					'next_membership_type_id'		=> $membersubscription->get_next_membership_type_id(),					
					'sort_by'						=> $membersubscription->get_sort_by(),
					'created_date'					=> date('Y-m-d H:i:s'),
					'last_modified' 				=> date('Y-m-d H:i:s')
	
			);
	
			$this->membersubscription_tableGateway->insert($data);
			return $this->membersubscription_tableGateway->lastInsertValue;
			
		} else {
	
			$data = array(
	
					'member_id'       				=> $membersubscription->get_member_id(),
					'membership_type_id'			=> $membersubscription->get_membership_type_id(),
					'purchased_date'				=> $membersubscription->get_purchased_date(),
					'start_date' 					=> $membersubscription->get_start_date(),
					'stop_date' 					=> $membersubscription->get_stop_date(),
					'num_of_properties'             => $membersubscription->get_num_of_properties(),
					'num_of_email_per_month'        => $membersubscription->get_num_of_email_per_month(),
					'next_membership_type_id'		=> $membersubscription->get_next_membership_type_id(),					
					'sort_by'						=> $membersubscription->get_sort_by(),
					'created_date'					=> $membersubscription->get_created_date(),
					'last_modified' 				=> date('Y-m-d H:i:s')
	
			);
	
			if ($this->getMembersubscription($member_subscription_id) ) {
				$this->membersubscription_tableGateway->update($data, array('feedback_id' => $member_subscription_id));
				return $member_subscription_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}
}

?>