<?php

namespace Index\Model;


use Zend\Db\TableGateway\TableGateway;

class MemberTable {
	protected $member_tableGateway;
	protected $property_tableGateway;
	protected $incomeitem_tableGateway;
	protected $expenseitem_tableGateway;
	protected $buyingitem_tableGateway;
	protected $sellingitem_tableGateway;
	protected $defaultbuyingcostitem_tableGateway;
	protected $defaultsellingcostitem_tableGateway;
	protected $adapter;

	public function __construct(TableGateway $member_tableGateway,$property_tableGateway,$incomeitem_tableGateway,$expenseitem_tableGateway,$buyingitem_tableGateway,$sellingitem_tableGateway,$defaultbuyingcostitem_tableGateway,$defaultsellingcostitem_tableGateway,$adapter)
	{
		$this->member_tableGateway   				= $member_tableGateway;
		$this->property_tableGateway 				= $property_tableGateway;
		$this->incomeitem_tableGateway  			= $incomeitem_tableGateway;
		$this->expenseitem_tableGateway 			= $expenseitem_tableGateway;
		$this->buyingitem_tableGateway  			= $buyingitem_tableGateway;
		$this->sellingitem_tableGateway 			= $sellingitem_tableGateway;
		$this->defaultbuyingcostitem_tableGateway	= $defaultbuyingcostitem_tableGateway;
		$this->defaultsellingcostitem_tableGateway  = $defaultsellingcostitem_tableGateway;
		$this->adapter 				 				= $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->member_tableGateway->select();
		return $resultSet;
	}

	public function getMember($id)
	{
		$id  = (int) $id;
		$rowset = $this->member_tableGateway->select(array('member_id' => $id));
		$member = $rowset->current();
		
		if (!$member ) {
			throw new \Exception("Could not find row $id");
		}

        // income items
		$statement = $this->adapter->query("select  income_id,
													income_name,
													member_id,
													sort_by,
													created_date,
													last_modified
											from member_income_items
											where member_id = ".$id);

		$incomeitems = $statement->execute();

		$i = 0;

		$incomeitemoutputs = array();

		foreach( $incomeitems as $incomeitem ){
			$rowset  = $this->incomeitem_tableGateway->select( array('income_id' => $incomeitem['income_id'] ) );
			$incomeitemoutputs[$i] = $rowset->current();
			$i++;
		}

		$member->set_income_items( $incomeitemoutputs );

		// expense items

		$statement = $this->adapter->query("select  expense_id,
													expense_name,
													member_id,
													sort_by,
													created_date,
													last_modified
											from member_expense_items
											where member_id = ".$id);

		$expenseitems = $statement->execute();

		$i = 0;

		$expenseitemoutputs = array();

		foreach( $expenseitems as $expenseitem ){
			$rowset  = $this->expenseitem_tableGateway->select( array('expense_id' => $expenseitem['expense_id'] ) );
			$expenseitemoutputs[$i] = $rowset->current();
			$i++;
		}

		$member->set_expense_items( $expenseitemoutputs );

		
		// buyingcost items

        $statement = $this->adapter->query("select buyingcost_id,
        										   buyingcost_name,
        										   member_id,
        										   sort_by,
        										   created_date,
        										   last_modified
											from member_buyingcost_items
											where member_id = ".$id);

        $buyingcostitems = $statement->execute();

        $i = 0;

        $buyingcostitemotuputs = array();

        foreach( $buyingcostitems as $buyingcostitem ){
        	$rowset = $this->buyingitem_tableGateway->select( array('buyingcost_id' => $buyingcostitem['buyingcost_id'] ) );
        	$buyingcostitemotuputs[$i] = $rowset->current();
        	$i++;
        }

        $member->set_buyingcost_items($buyingcostitemotuputs);
        
      
        
        

        // sellingcost items

        $statement = $this->adapter->query("select sellingcost_id,
        										   sellingcost_name,
        										   member_id,
        										   sort_by,
        										   created_date,
        										   last_modified
											from member_sellingcost_items
											where member_id = ".$id);

        $sellingcostitems = $statement->execute();

        $i = 0;

        $sellingcostitemotuputs = array();

        foreach( $sellingcostitems as $sellingcostitem){
        	$rowset = $this->sellingitem_tableGateway->select( array('sellingcost_id' => $sellingcostitem['sellingcost_id'] ) );
        	$sellingcostitemotuputs[$i] = $rowset->current();
        	$i++;
        }

        $member->set_sellingcost_items($sellingcostitemotuputs);
        

     

        // default buying cost items
        
        $statement = $this->adapter->query("select member_default_buyingcost_item_id,
	  											   mbi.buyingcost_id,
	   											   mbi.buyingcost_name,
	   											   member_default_buyingcost_item_amount
	   										from member_default_buyingcost_items mdbi
	   										inner join member_buyingcost_items mbi on mdbi.buyingcost_id = mbi.buyingcost_id
											where mdbi.member_id =".$id);
        
        $defaultbuyingcostitems = $statement->execute();
        
        $i = 0;
        
        $outputs = array();
     
        foreach( $defaultbuyingcostitems as $defaultbuyingcostitem ){
        	
        	$outputs[$i]['member_default_buyingcost_item_id']     = $defaultbuyingcostitem['member_default_buyingcost_item_id'];
        	$outputs[$i]['buyingcost_id'] 			   	   		  = $defaultbuyingcostitem['buyingcost_id'];
        	$outputs[$i]['buyingcost_name']  			   		  = $defaultbuyingcostitem['buyingcost_name'];
        	$outputs[$i]['member_default_buyingcost_item_amount'] = $defaultbuyingcostitem['member_default_buyingcost_item_amount'];
        	$i++;
        }
       
       $member->set_default_buyingcost_items( $outputs );
       
       // default selling cost items
       
        $statement = $this->adapter->query("select member_default_sellingcost_item_id,
	  											   mbi.sellingcost_id,
	   											   mbi.sellingcost_name,
	   											   member_default_sellingcost_item_amount
	   										from member_default_sellingcost_items mdbi
	   										inner join member_sellingcost_items mbi on mdbi.sellingcost_id = mbi.sellingcost_id
											where mdbi.member_id =".$id);
        
        $defaultsellingcostitems = $statement->execute();
        
        $i = 0;
        
        $outputs = array();
        
        foreach( $defaultsellingcostitems as $defaultsellingcostitem ){
        	$outputs[$i]['member_default_sellingcost_item_id']     = $defaultsellingcostitem['member_default_sellingcost_item_id'];
        	$outputs[$i]['sellingcost_id'] 			   	   		 = $defaultsellingcostitem['sellingcost_id'];
        	$outputs[$i]['sellingcost_name']  			   		 = $defaultsellingcostitem['sellingcost_name'];
        	$outputs[$i]['member_default_sellingcost_item_amount'] = $defaultsellingcostitem['member_default_sellingcost_item_amount'];
        	$i++;
        }
        
       $member->set_default_sellingcost_items( $outputs );
       
      
		//////////////
		$statement = $this->adapter->query("select property_id
										   	from properties
											where member_id =".$id);

		$properties = $statement->execute();

		$i = 0;

		$propertyoutputs = array();

		foreach( $properties as $property){

			$rowset     = $this->property_tableGateway->select( array('property_id' => $property['property_id'] ) );
			$propertyoutputs[$i] = $rowset->current();
			$i++;

		}

		$member->set_properties( $propertyoutputs );

		return $member;
	}

	public function getId($email){

		$resultSet = $this->member_tableGateway->select(array('email' => $email));

		if( $resultSet->count() ){

			foreach($resultSet as $result){

				$output = $result->get_member_id();

			}

			return $output;

		}else{
			return 0;
		}



	}

	public function saveMember(Member $member)
	{

		

		$member_id = (int)$member->get_member_id();
		if ($member_id == 0) {

			$data = array(
					'email'  	    											=> $member->get_email(),
					'password'  												=> $member->get_password(),
					'hash'          											=> $member->get_hash(),
					'member_type_id'											=> $member->get_member_type_id(),
					'active'        											=> $member->is_active(),
					'expired_date'  											=> '2100-01-01 00:00:00',
					
					// for default value
					'property_default_mls'										=> $member->get_property_default_mls(),
					'property_default_style'									=> $member->get_property_default_style(),
					'property_default_squarefeet'								=> $member->get_property_default_squarefeet(),
					'property_default_lotsize'									=> $member->get_property_default_lotsize(),
					'property_default_yearbuilt'								=> $member->get_property_default_yearbuilt(),
					'property_default_lastremodel'								=> $member->get_property_default_lastremodel(),
					'property_default_parking'									=> $member->get_property_default_parking(),
					'property_default_listingprice'								=> $member->get_property_default_listingprice(),
					'property_default_initimprove'								=> $member->get_property_default_initimprove(),
					'property_default_purchaseprice'							=> $member->get_property_default_purchaseprice(),
					'property_default_first_loanamount'							=> $member->get_property_default_first_loanamount(),
					'property_default_first_loantype'							=> $member->get_property_default_first_loantype(),
					'property_default_first_interestrate'						=> $member->get_property_default_first_interestrate(),
					'property_default_first_payment_frequency'					=> $member->get_property_default_first_payment_frequency(),
					'property_default_first_term'								=> $member->get_property_default_first_term(),
					'property_default_first_interestonly_term'					=> $member->get_property_default_first_interestonly_term(),
					'property_default_second_loanamount'						=> $member->get_property_default_second_loanamount(),
					'property_default_second_loantype'							=> $member->get_property_default_second_loantype(),
					'property_default_second_interestrate'						=> $member->get_property_default_second_interestrate(),
					'property_default_second_payment_frequency'					=> $member->get_property_default_second_payment_frequency(),
					'property_default_second_term'								=> $member->get_property_default_second_term(),
					'property_default_second_interestonly_term'					=> $member->get_property_default_second_interestonly_term(),
					'property_default_vacancy_rate'								=> $member->get_property_default_vacancy_rate(),
					'property_default_rent_mode'								=> $member->get_property_default_rent_mode(),
					'property_default_singlevalue_rent'							=> $member->get_property_default_singlevalue_rent(),
					'property_default_singlevalue_rent_frequency'				=> $member->get_property_default_singlevalue_rent_frequency(),	
					'property_default_appreciation_rate'						=> $member->get_property_default_appreciation_rate(),
					'property_default_incomeinflation_rate'						=> $member->get_property_default_incomeinflation_rate(),
					'property_default_expenseinflation_rate'					=> $member->get_property_default_expenseinflation_rate(),
					'property_default_lvt_for_refinance'						=> $member->get_property_default_lvt_for_refinance(),
					'property_default_compounding_period'						=> $member->get_property_default_compounding_period(),
					
					'sort_by'													=> $member->get_sort_by(),
					'created_date'												=> date('Y-m-d H:i:s'),
					'last_modified' 											=> date('Y-m-d H:i:s')

			);

			$this->member_tableGateway->insert($data);
			return $this->member_tableGateway->lastInsertValue;
		} else {

			$data = array(
					'email'  	    => $member->get_email(),
					'password'  	=> $member->get_password(),
					'hash'          => $member->get_hash(),
					'member_type_id'=> $member->get_member_type_id(),
					'active'        => $member->is_active(),
					'expired_date'  => $member->get_expired_date(),
					
					// for default value
					'property_default_mls'										=> $member->get_property_default_mls(),
					'property_default_style'									=> $member->get_property_default_style(),
					'property_default_squarefeet'								=> $member->get_property_default_squarefeet(),
					'property_default_lotsize'									=> $member->get_property_default_lotsize(),
					'property_default_yearbuilt'								=> $member->get_property_default_yearbuilt(),
					'property_default_lastremodel'								=> $member->get_property_default_lastremodel(),
					'property_default_parking'									=> $member->get_property_default_parking(),
					'property_default_listingprice'								=> $member->get_property_default_listingprice(),
					'property_default_initimprove'								=> $member->get_property_default_initimprove(),
					'property_default_purchaseprice'							=> $member->get_property_default_purchaseprice(),
					'property_default_first_loanamount'							=> $member->get_property_default_first_loanamount(),
					'property_default_first_loantype'							=> $member->get_property_default_first_loantype(),
					'property_default_first_interestrate'						=> $member->get_property_default_first_interestrate(),
					'property_default_first_payment_frequency'					=> $member->get_property_default_first_payment_frequency(),
					'property_default_first_term'								=> $member->get_property_default_first_term(),
					'property_default_first_interestonly_term'					=> $member->get_property_default_first_interestonly_term(),
					'property_default_second_loanamount'						=> $member->get_property_default_second_loanamount(),
					'property_default_second_loantype'							=> $member->get_property_default_second_loantype(),
					'property_default_second_interestrate'						=> $member->get_property_default_second_interestrate(),
					'property_default_second_payment_frequency'					=> $member->get_property_default_second_payment_frequency(),
					'property_default_second_term'								=> $member->get_property_default_second_term(),
					'property_default_second_interestonly_term'					=> $member->get_property_default_second_interestonly_term(),
					'property_default_vacancy_rate'								=> $member->get_property_default_vacancy_rate(),
					'property_default_rent_mode'								=> $member->get_property_default_rent_mode(),
					'property_default_singlevalue_rent'							=> $member->get_property_default_singlevalue_rent(),
					'property_default_singlevalue_rent_frequency'				=> $member->get_property_default_singlevalue_rent_frequency(),
					'property_default_appreciation_rate'						=> $member->get_property_default_appreciation_rate(),
					'property_default_incomeinflation_rate'						=> $member->get_property_default_incomeinflation_rate(),
					'property_default_expenseinflation_rate'					=> $member->get_property_default_expenseinflation_rate(),
					'property_default_lvt_for_refinance'						=> $member->get_property_default_lvt_for_refinance(),
					'property_default_compounding_period'						=> $member->get_property_default_compounding_period(),
											
					'sort_by'		=> $member->get_sort_by(),
					'created_date'	=> $member->get_created_date(),
					'last_modified' => date('Y-m-d H:i:s')

			);

			if ($this->getMember($member_id)) {
				$this->member_tableGateway->update($data, array('member_id' => $member_id));
				return $member_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function isMemberExist($email,$hash){

		$rowset = $this->member_tableGateway->select(
												array(
														'email' => $email,
														'hash'  => $hash,
														'active'=> 0
													 )
											 );
		$row = $rowset->current();
		if (!$row) {
			return 0;
		}else{
			return 1;
		}

	}

}

?>





