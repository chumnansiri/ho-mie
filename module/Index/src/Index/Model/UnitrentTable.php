<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class UnitrentTable {
	protected $tableGateway;
	protected $adapter;

	public function __construct(TableGateway $tableGateway,$adapter)
	{
		$this->tableGateway = $tableGateway;
		$this->adapter      = $adapter;
	}

	public function fetchAll()
	{

		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getUnitrent($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('unit_rent_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}


	public function saveUnitrent(Unitrent $unitrent)
	{

		

		$unit_rent_id = (int)$unitrent->get_unit_rent_id();
		if ($unit_rent_id == 0) {

			$data = array(
					'unit_rent_id' 			=> $unitrent->get_unit_rent_id(),
					'unit_id'				=> $unitrent->get_unit_id(),
					'unit_rent_rent'		=> $unitrent->get_unit_rent_rent(),
					'unit_rent_frequency'	=> $unitrent->get_unit_rent_frequency(),
					'year'					=> $unitrent->get_year(),
					'sort_by'				=> $unitrent->get_sort_by(),
					'created_date'			=> date('Y-m-d H:i:s'),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'unit_rent_id' 			=> $unitrent->get_unit_rent_id(),
					'unit_id'				=> $unitrent->get_unit_id(),
					'unit_rent_rent'		=> $unitrent->get_unit_rent_rent(),
					'unit_rent_frequency'	=> $unitrent->get_unit_rent_frequency(),
					'year'					=> $unitrent->get_year(),
					'sort_by'				=> $unitrent->get_sort_by(),
					'created_date'			=> $unitrent->get_created_date(),
					'last_modified' 		=> date('Y-m-d H:i:s')

			);

			if ($this->getUnit($unit_rent_id)) {
				$this->tableGateway->update($data, array('unit_id' => $unit_rent_id));
				return $unit_rent_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteUnitrent($unit_rent_id)
	{
		$this->property_tableGateway->delete(array('unit_rent_id' => $unit_rent_id));
	}

	public function deleteUnitrents($unit_id,$years){

		$years = implode(",",$years);
		$this->tableGateway->delete( 'unit_id = '.$unit_id.' and year not in('.$years.')' );

	}
	public function update_unit_by_year($unit_id,$year,$rent,$frequency){
		date_default_timezone_set('America/Los_Angeles');
		$where = array(
						'unit_id' => $unit_id,
						'year'    => $year
					  );

		$data = array(
						'unit_rent_rent' 		=> $rent,
						'unit_rent_frequency'	=> $frequency,
						'last_modified'         => date('Y-m-d H:i:s')
					 );

		$this->tableGateway->update($data, $where);
	}

	public function isexist($unit_id,$year){
		$rowset = $this->tableGateway->select(
											array(
													'unit_id' => $unit_id,
													'year'    => $year,
												 )
											 );


		$row = $rowset->current();
		if (!$row) {
			return 0;
		}else{
			return 1;
		}
	}

}

?>

