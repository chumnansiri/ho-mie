<?php
namespace Index\Model;

// Add these import statements

class Fullpagephoto{

	private $fullpage_photo_id;
	private $property_id;
	private $fullpage_photo_path;
	private $sort_by;
	private $created_date;
	private $last_modified;

	public function __construct(Fullpagephoto $fullpagephoto=null){
		if( $fullpagephoto ){
			$this->unit_id  		   =        $fullpagephoto->get_unit_id();
			$this->property_id         = 	    $fullpagephoto->get_property_id();
			$this->fullpage_photo_path =        $fullpagephoto->get_fullpage_photo_path();
			$this->sort_by             =        $fullpagephoto->get_sort_by();
			$this->created_date        =        $fullpagephoto->get_created_date();
			$this->last_modified       =        $fullpagephoto->get_last_modified();
		}
	}

	public function exchangeArray($data)
    {
        $this->fullpage_photo_id     	=    (		isset(  $data['fullpage_photo_id']) 	) ? 	$data['fullpage_photo_id'] 	: 	null;
        $this->property_id     			=    (		isset(  $data['property_id']) 			) ? 	$data['property_id'] 		: 	null;
        $this->fullpage_photo_path      =    (		isset(  $data['fullpage_photo_path']) 	) ? 	$data['fullpage_photo_path']: 	null;
        $this->sort_by      			=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date      		=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified      		=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;

    }


    public function get_fullpage_photo_id(){
		return $this->fullpage_photo_id;
    }

    public function get_property_id(){
    	return $this->property_id;
    }

  	public function get_fullpage_photo_path(){
  		return $this->fullpage_photo_path;
  	}

    public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_fullpage_photo_id($fullpage_photo_id){
		$this->fullpage_photo_id = $fullpage_photo_id;
    }

    public function set_property_id($property_id){
    	$this->property_id = $property_id;
    }

    public function set_fullpage_photo_path($fullpage_photo_path){
    	$this->fullpage_photo_path = $fullpage_photo_path;
    }

    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }

}
