<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class MemberdefaultsellingcostitemTable {
	
	protected $tableGateway;
	
	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}
	
	public function fetchAll()
	{
	
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	
	public function getMember_default_sellingcost_item($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('member_default_sellingcost_item_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}
	

	
	public function getMember_default_sellingcost_item_by_member_id( $member_id )
	{
		
		$member_id = (int) $member_id;
		return $this->tableGateway->select(array('member_id' => $member_id));
		
	}
	
	public function getId($member_id,$sellingcost_id){
	
		$resultSet = $this->tableGateway->select(array('member_id' 				=> $member_id,
													   'sellingcost_id'			=> $sellingcost_id
		));
	
		if( $resultSet->count() ){
	
			foreach($resultSet as $result){
	
				$output = $result->get_member_default_sellingcost_item_id();
	
			}
	
			return $output;
	
		}else{
			return 0;
		}
	
	
	
	}
	
	public function saveMember_default_sellingcost_item(Memberdefaultsellingcostitem $member_default_sellingcost_item)
	{
	
		$member_default_sellingcost_item_id = (int)$member_default_sellingcost_item->get_member_default_sellingcost_item_id();
		if ($member_default_sellingcost_item_id == 0) {
	
	
			$data = array(
	
					'member_id'  	    						=> $member_default_sellingcost_item->get_member_id(),
					'sellingcost_id'  							=> $member_default_sellingcost_item->get_sellingcost_id(),
					'member_default_sellingcost_item_amount'	=> $member_default_sellingcost_item->get_member_default_sellingcost_item_amount(),
					'sort_by'									=> $member_default_sellingcost_item->get_sort_by(),
					'created_date'								=> date('Y-m-d H:i:s'),
					'last_modified' 							=> date('Y-m-d H:i:s')
	
			);
	
	
			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {
	
			if ($this->getMember_default_sellingcost_item($member_default_sellingcost_item_id)) {
				$data = array(
						'member_id'  	    			=> $member_default_sellingcost_item->get_member_id(),
						'sellingcost_id'  						=> $member_default_sellingcost_item->get_sellingcost_id(),
						'member_default_sellingcost_item_amount'=> $member_default_sellingcost_item->get_member_default_sellingcost_item_amount(),
						'sort_by'								=> $member_default_sellingcost_item->get_sort_by(),
						'created_date'							=> $member_default_sellingcost_item->get_created_date(),
						'last_modified' 						=> date('Y-m-d H:i:s')
				);
				$this->tableGateway->update($data, array('member_default_sellingcost_item_id' => $member_default_sellingcost_item_id));
				return $member_default_sellingcost_item_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}
	public function deleteMember_default_sellingcost_item($member_default_sellingcost_item_id){
	
		$this->tableGateway->delete(array(
				'member_default_sellingcost_item_id' 		=> $member_default_sellingcost_item_id,
		));
	
	}
	
	public function deleteMember_default_sellingcost_item_by_sellingcost_id( $sellingcost_id ){
	
		$this->tableGateway->delete(array(
				'sellingcost_id' 		=> $sellingcost_id,
		));
	
	}
	
		
}

?>