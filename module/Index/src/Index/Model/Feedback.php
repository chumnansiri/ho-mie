<?php
namespace Index\Model;

// Add these import statements

class Feedback{

	private $feedback_id;
	private $member_id;
	private $message;
	private $sort_by;
	private $created_date;
	private $last_modified;




	public function __construct(Feedback $feedback=null){
		if($feedback){

			$this->feedback_id      = $feedback->get_feedback_id();
			$this->member_id 	  	= $feedback->get_member_id();
			$this->message          = $feedback->get_message();
			$this->sort_by        	= $feedback->get_sort_by();
			$this->created_date   	= $feedback->get_created_date();
			$this->last_modified  	= $feedback->get_last_modified();


		}
	}

	public function exchangeArray($data)
    {
    	$this->feedback_id          =    (		isset(  $data['feedback_id']) 			) ? 	$data['feedback_id'] 		: 	null;
        $this->member_id     		=    (		isset(  $data['member_id']) 			) ? 	$data['member_id'] 			: 	null;
        $this->message              =    (		isset(  $data['message']) 			    ) ? 	$data['message'] 			: 	null;
        $this->sort_by      		=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date      	=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified      	=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;

    }

    public function get_feedback_id(){
    	return $this->feedback_id;
    }

    public function get_member_id(){
		return $this->member_id;
    }

    public function get_message(){
    	return $this->message;
    }

    public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }


    ////////////////////////////////

    public function set_feedback_id($feed_id){
        $this->feedback_id = $feed_id;
    }

    public function set_member_id($member_id){
    	$this->member_id = $member_id;
    }

	public function set_message($message){
		$this->message = $message;
	}

    public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }







}
