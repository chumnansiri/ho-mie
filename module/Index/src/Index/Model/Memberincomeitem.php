<?php
namespace Index\Model;

// Add these import statements

class Memberincomeitem{

	private $income_id;
	private $income_name;
	private $member_id;
	private $sort_by;
	private $created_date;
	private $last_modified;


	public function __construct(){

	}

	public function exchangeArray($data)
    {
        $this->income_id     		=    (		isset(  $data['income_id']) 			) ? 	$data['income_id'] 			: 	null;
        $this->income_name     		=    (		isset(  $data['income_name']) 			) ? 	$data['income_name'] 		: 	null;
        $this->member_id     		=    (		isset(  $data['member_id']) 			) ? 	$data['member_id'] 			: 	null;
        $this->sort_by     			=    (		isset(  $data['sort_by']) 				) ? 	$data['sort_by'] 			: 	null;
        $this->created_date     	=    (		isset(  $data['created_date']) 			) ? 	$data['created_date'] 		: 	null;
        $this->last_modified     	=    (		isset(  $data['last_modified']) 		) ? 	$data['last_modified'] 		: 	null;


    }

    public function get_income_id(){
    	return $this->income_id;
    }

    public function get_income_name(){
    	return $this->income_name;
    }

    public function get_member_id(){
		return $this->member_id;
    }

 	public function get_sort_by(){
		return $this->sort_by;
    }

    public function get_created_date(){
    	return $this->created_date;
    }

    public function get_last_modified(){
    	return $this->last_modified;
    }

    ////////////////////////////////

    public function set_income_id($income_id){
    	$this->income_id = $income_id;
    }

    public function set_income_name($income_name){
    	$this->income_name = $income_name;
    }

    public function set_member_id($member_id){
    	$this->member_id = $member_id;
    }

	public function set_sort_by($sort_by){
    	$this->sort_by = $sort_by;
    }

    public function set_created_date($created_date){
    	$this->created_date = $created_date;
    }

    public function set_last_modified($last_modified){
    	$this->last_modified = $last_modified;
    }


}
