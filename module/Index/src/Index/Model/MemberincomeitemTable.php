<?php


namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class MemberincomeitemTable {

	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{
		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}

	public function getMemberincomeitem($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('income_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getMemberincomeitems($member_id){

		$where = array(
				'member_id'	=> $member_id,

		);

		$resultSet = $this->tableGateway->select($where);
		return $resultSet;
	}

	public function saveMemberincomeitem(Memberincomeitem $memberincomeitem)
	{


		$income_id = (int)$memberincomeitem->get_income_id();
		if ($income_id == 0) {

			$data = array(
					'income_name'  	=> $memberincomeitem->get_income_name(),
					'member_id'  	=> $memberincomeitem->get_member_id(),
					'sort_by'		=> $memberincomeitem->get_sort_by(),
					'created_date'	=> date('Y-m-d H:i:s'),
					'last_modified' => date('Y-m-d H:i:s')
			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'income_name'  	=> $memberincomeitem->get_income_name(),
					'member_id'  	=> $memberincomeitem->get_member_id(),
					'sort_by'		=> $memberincomeitem->get_sort_by(),
					'created_date'	=> $memberincomeitem->get_created_date(),
					'last_modified' => date('Y-m-d H:i:s')
			);

			if ($this->getMemberincomeitem($income_id)) {
				$this->tableGateway->update($data, array('income_id' => $income_id));
				return $income_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteMemberincomeitem($income_id)
	{
		$this->tableGateway->delete(array('income_id' => $income_id));
	}

}