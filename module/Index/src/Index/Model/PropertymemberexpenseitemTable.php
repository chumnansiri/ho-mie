<?php

namespace Index\Model;

use Zend\Db\TableGateway\TableGateway;

class PropertymemberexpenseitemTable{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway)
	{
		$this->tableGateway = $tableGateway;
	}

	public function fetchAll()
	{

		$resultSet = $this->tableGateway->select();
		return $resultSet;
	}
	
	public function isUsed($expense_id){
	
		$resultSet = $this->tableGateway->select(array(
						'expense_id'			=> $expense_id
		));
	
		if( $resultSet->count() ){
			// expense_id is being used
			return 1;
		}else{
			return 0;
		}
	
	}

	public function getProperty_member_expense_item($id)
	{
		$id  = (int) $id;
		$rowset = $this->tableGateway->select(array('property_expense_id' => $id));
		$row = $rowset->current();
		if (!$row) {
			throw new \Exception("Could not find row $id");
		}
		return $row;
	}

	public function getId($property_id,$expense_id){

		$resultSet = $this->tableGateway->select(array('property_id' 		=> $property_id,
													   'expense_id'			=> $expense_id
		));

		if( $resultSet->count() ){

			foreach($resultSet as $result){

				$output = $result->get_property_expense_id();

			}

			return $output;

		}else{
			return 0;
		}
	}

	public function saveProperty_member_expense_item(Propertymemberexpenseitem $propertymemberexpenseitem)
	{

		

		$property_expense_id = (int)$propertymemberexpenseitem->get_property_expense_id();
		if ($property_expense_id == 0) {

			$data = array(
					'property_id'  	    => $propertymemberexpenseitem->get_property_id(),
					'expense_id'  		=> $propertymemberexpenseitem->get_expense_id(),
					'amount'			=> $propertymemberexpenseitem->get_amount(),
					'frequency'         => $propertymemberexpenseitem->get_frequency(),
					'sort_by'			=> $propertymemberexpenseitem->get_sort_by(),
					'created_date'		=> date('Y-m-d H:i:s'),
					'last_modified' 	=> date('Y-m-d H:i:s')
			);

			$this->tableGateway->insert($data);
			return $this->tableGateway->lastInsertValue;
		} else {

			$data = array(
					'property_id'  	    => $propertymemberexpenseitem->get_property_id(),
					'expense_id'  		=> $propertymemberexpenseitem->get_expense_id(),
					'amount'			=> $propertymemberexpenseitem->get_amount(),
					'frequency'         => $propertymemberexpenseitem->get_frequency(),
					'sort_by'			=> $propertymemberexpenseitem->get_sort_by(),
					'created_date'		=> $propertymemberexpenseitem->get_created_date(),
					'last_modified' 	=> date('Y-m-d H:i:s')

			);

			if ($this->getProperty_member_expense_item($property_expense_id)) {
				$this->tableGateway->update($data, array('property_expense_id' => $property_expense_id));
				return $property_expense_id;
			} else {
				throw new \Exception('Form id does not exist');
			}
		}
	}

	public function deleteProperty_member_expense_item($property_expense_id){

		$this->tableGateway->delete(array(
				'property_expense_id' 		=> $property_expense_id,
		));

	}
	
	public function deleteProperty_member_expense_item_by_property_id($property_id){
	
		$this->tableGateway->delete(array(
				'property_id' 		=> $property_id,
		));
	
	}

}

?>

