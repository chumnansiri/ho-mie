<?php

namespace Index\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;

use Zend\Validator\Db\RecordExists;


use Index\Form\Validator\Forgotpassword\emailValidator;

class ForgotpasswordForm extends Form{


	public function __construct($name = null,$dbAdapter)
	{

		parent::__construct($name);
		$this->setAttribute('method', 'post');

		$email = new Element('email');
		$email->setAttributes(array(
				'name'		  =>'email',
				'type'		  =>'email',
				'placeholder' =>'Email',
				'autocomplete'=>'off'
		));



		$submit   = new Element('submit');
		$submit->setAttributes(array(
				'name'		 =>'submit',
				'type'		 =>'submit',
				'value'		 =>'Send Password',

		));

		$this->add($email);
		$this->add($submit);


		$emailInput = new Input('email');

		$emailValidator = new emailValidator(array(
						'field'	  => 'username',
				        'schema'  => 'zf2',
        				'adapter' => $dbAdapter
        		));



		$emailInput->getValidatorChain()->addValidator($emailValidator);



		$inputFilter = new InputFilter();

		$inputFilter->add($emailInput);

		$this->setInputFilter($inputFilter);

	}
}

?>