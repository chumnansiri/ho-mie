<?php

namespace Index\Form\Validator\Forgotpassword;

use Zend\Validator\Db\AbstractDb;

class emailValidator extends AbstractDb{


	const INVALID_FORMAT ='INVALID_FORMAT';
	const NOT_EXIST      ='NOT_EXIST';


	protected $messageTemplates = array(
		self::INVALID_FORMAT => "'%value%' has invalid email formats",
		self::NOT_EXIST      => "'%value%' does not exist in our system"
	);


	public function isValid($value){

		$this->setValue($value);//to insert tested value to the failure message


		$db = $this->getAdapter();

		$exist_query="select *
					  from members
					  where email='".$value."'";

		$result     = $db->query($exist_query,$db::QUERY_MODE_EXECUTE);
		$exist      = $result->count();

		if(!$exist){
			$this->error(self::NOT_EXIST);
			return false;
		}



		if( !preg_match( '/^[^\W][a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/',$value) )
		{
			$this->error(self::INVALID_FORMAT);
			return false;
		}

		return true;

	}

}

?>