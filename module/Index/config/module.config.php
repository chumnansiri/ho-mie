<?php


if( getenv('APPLICATION_ENV') == 'development' ){

	$db_config = array(
			
			'driver'         => 'Pdo',
			'dsn'            => 'mysql:dbname=zf2;host=mysql;port=3306',
			'username' 		 => 'ho-mie.com',
			'password'	     => 'ho-mie.com',
						

	);

}else if(  getenv('APPLICATION_ENV') == 'production' ){

	$db_config = array(
			'driver'         => 'Pdo',
			'dsn'            => 'mysql:dbname=zf2;host=mysql;port=3306',
			'username' 		 => 'ho-mie.com',
			'password'	     => 'ho-mie.com',

	);

}else{

	$db_config = array(
				
			'driver'         => 'Pdo',
			'dsn'            => 'mysql:dbname=zf2;host=mysql;port=3306',
			'username' 		 => 'ho-mie.com',
			'password'	     => 'ho-mie.com',
	
	
	);
}


return array(
	'controllers' => array(
			'invokables' => array(
					'Index\Controller\Index'    					=> 'Index\Controller\IndexController',
					'Index\Controller\Member'   					=> 'Index\Controller\MemberController',
					'Index\Controller\Memberdefaultbuyingcost' 	    => 'Index\Controller\MemberdefaultbuyingcostController',
					'Index\Controller\Memberdefaultsellingcost' 	=> 'Index\Controller\MemberdefaultsellingcostController',
					'Index\Controller\Property' 					=> 'Index\Controller\PropertyController',
					'Index\Controller\Propertybuyingcost' 			=> 'Index\Controller\PropertybuyingcostController',					
					'Index\Controller\Propertysellingcost' 			=> 'Index\Controller\PropertysellingcostController',
					'Index\Controller\Propertymortgagecost'			=> 'Index\Controller\PropertymortgagecostController',
					'Index\Controller\Otherincome'	 				=> 'Index\Controller\OtherincomeController',
					'Index\Controller\Otherexpense'	 				=> 'Index\Controller\OtherexpenseController',
					'Index\Controller\Slidebarinput'	 			=> 'Index\Controller\SlidebarinputController',
					'Index\Controller\Unit'	 						=> 'Index\Controller\UnitController',
					'Index\Controller\Unitforms'	 				=> 'Index\Controller\UnitformsController',
					'Index\Controller\Utility'	 		    		=> 'Index\Controller\UtilityController',

			),
	),





	// The following section is new and should be added to your file
		'router' => array(
				'routes' => array(
						// index route
						'index' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/index[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\Index',
												'action'     => 'index',
										),
								),
						),
						// member route
						'member' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/member[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\Member',
												'action'     => 'property',
										),
								),
						),
						// member default buying cost route
						'memberdefaultbuyingcost' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/memberdefaultbuyingcost[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\Memberdefaultbuyingcost',
												'action'     => 'index',
										),
								),
						),
						
						// member default selling cost route
						'memberdefaultsellingcost' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/memberdefaultsellingcost[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\Memberdefaultsellingcost',
												'action'     => 'index',
										),
								),
						),
						
						// property route
						'property' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/property[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\property',
												'action'     => 'index',
										),
								),
						),
						'propertybuyingcost' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/propertybuyingcost[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\propertybuyingcost',
												'action'     => 'index',
										),
								),
						),
						'propertysellingcost' => array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/propertysellingcost[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\propertysellingcost',
												'action'     => 'index',
										),
								),
						),
						'propertymortgagecost'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/propertymortgagecost[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\propertymortgagecost',
												'action'     => 'index',
										),
								),
						),
						'otherincome'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/otherincome[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\otherincome',
												'action'     => 'index',
										),
								),
						),
						'otherexpense'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/otherexpense[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\otherexpense',
												'action'     => 'index',
										),
								),
						),
						'slidebarinput'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/slidebarinput[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\slidebarinput',
												'action'     => 'index',
										),
								),
						),
						'unit'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/unit[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\unit',
												'action'     => 'index',
										),
								),
						),
						'unitforms'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/unitforms[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\unitforms',
												'action'     => 'index',
										),
								),
						),
						'utility'=> array(
								'type'    => 'segment',
								'options' => array(
										'route'    => '/utility[/][:action][/:id]',
										'constraints' => array(
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
												'id'     => '[0-9]+',
										),
										'defaults' => array(
												'controller' => 'Index\Controller\utility',
												'action'     => 'index',
										),
								),
						),
				),
		),

	 'view_manager' => array(
	 	'template_map' => array(
	 		'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
	 		'error/404'               => __DIR__ . '/../view/error/404.phtml',
	 		'error/index'             => __DIR__ . '/../view/error/index.phtml',
	 	),
        'template_path_stack' => array(
            'index' => __DIR__ . '/../view',
        ),
	 	'strategies' => array(
	 		'ViewJsonStrategy',
	 	),
    ),

		'index_db_config'=> $db_config,

	/* for amazon
		'index_db_config'=> array(
				'driver'         => 'Pdo',
				'dsn'            => 'mysql:dbname=zf2;host=test.ceaduklyw9ff.us-west-2.rds.amazonaws.com',
				'username' 		 => 'zf2_test',
				'password'	     => '6630Sunset!!!',

		)
	*/
	/* for rackspace
	'index_db_config'=> array(
            					'driver'         => 'Pdo',
            					'dsn'            => 'mysql:dbname=zf2;host=localhost',
            					'username' 		 => 'zf1',
            					'password'	     => '6630Sunset!!!',

            			)
	*/


);