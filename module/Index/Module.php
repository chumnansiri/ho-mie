<?php
namespace Index;
use Zend\EventManager\EventInterface as Event;


use Zend\Db\Adapter\Adapter;

use Index\Model\Utility\Trafficlog;

use Index\Model\ItemTable;

use Index\Model\Member;
use Index\Model\MemberTable;

use Index\Model\Property;
use Index\Model\PropertyTable;

use Index\Model\Memberdefaultbuyingcostitem;
use Index\Model\MemberdefaultbuyingcostitemTable;

use Index\Model\Memberdefaultsellingcostitem;
use Index\Model\MemberdefaultsellingcostitemTable;

use Index\Model\Memberincomeitem;
use Index\Model\MemberincomeitemTable;

use Index\Model\Propertymemberincomeitem;
use Index\Model\PropertymemberincomeitemTable;

use Index\Model\Memberexpenseitem;
use Index\Model\MemberexpenseitemTable;

use Index\Model\Propertymemberexpenseitem;
use Index\Model\PropertymemberexpenseitemTable;

use Index\Model\Memberbuyingcostitem;
use Index\Model\MemberbuyingcostitemTable;

use Index\Model\Propertybuyingcostitem;
use Index\Model\PropertybuyingcostitemTable;

use Index\Model\Membersellingcostitem;
use Index\Model\MembersellingcostitemTable;

use Index\Model\Propertysellingcostitem;
use Index\Model\PropertysellingcostitemTable;

use Index\Model\Unit;
use Index\Model\UnitTable;

use Index\Model\Unitrent;
use Index\Model\UnitrentTable;

use Index\Model\Fullpagephoto;
use Index\Model\FullpagephotoTable;

use Index\Model\Propertyvideo;
use Index\Model\PropertyvideoTable;

use Index\Model\Feedback;
use Index\Model\FeedbackTable;

use Index\Model\Membershiptype;
use Index\Model\MembershiptypeTable;

use Index\Model\Membersubscription;
use Index\Model\MembersubscriptionTable;

use Index\Model\Membermonthlytoken;
use Index\Model\MembermonthlytokenTable;

use Index\Model\Surway;
use Index\Model\SurwayTable;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;


//use Zend\Cache\Storage\Event;





Class module{
	public function onBootstrap(Event $e){
		date_default_timezone_set('America/Los_Angeles');
		
		
		
	}
	
	public function getAutoloaderConfig(){
		return array(
			'Zend\Loader\ClassMapAutoloader' => array(
					__DIR__.'/autoload_classmap.php',
			),
			'Zend\Loader\StandardAutoloader' => array(
					'namespaces' => array(
					__NAMESPACE__ => __DIR__.'/src/'.__NAMESPACE__,
					//'TCPDF'				=> __DIR__.'/libraries/tcpdf/',
					//'MyNamespace' => __DIR__.'/libraries/tcpdf',
					),
			),
		);
	}

	public function getConfig(){
		return include __DIR__.'/config/module.config.php';
	}


	public function getServiceConfig()
	{

		return array(
            'factories' => array(
            		// just in cass it's used in controller
            		'Trafficlog' => function( $sm ){
            			$config  = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );
            			$tableGateway = new TableGateway('traffic_logs', $adapter, null);
            			$table = new Trafficlog( $tableGateway );
            			
            			return $table;
            		},
            		'my_index_db_adapter' => function ($sm) {
						$config  = $sm->get('config');
						$adapter = new Adapter( $config['index_db_config'] );
						return $adapter;
            		},
            		'Index\Model\ItemTable'   =>  function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$tableGateway = new TableGateway('items', $adapter, null);
            			$table = new ItemTable($tableGateway);
            			return $table;

            		},
            		'Index\Model\MemberTable' =>  function($sm) {
						// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );


            			$resultSetPrototype 					= new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Member());
            			$tableGateway 							= new TableGateway('members', $adapter, null, $resultSetPrototype);

            			$PropertyresultSetPrototype 			= new ResultSet();
            			$PropertyresultSetPrototype->setArrayObjectPrototype( new Property() );
            			$PropertytableGateway 					= new TableGateway('properties', $adapter, null, $PropertyresultSetPrototype);

            			$IncomeitemresultSetPrototype           = new ResultSet();
            			$IncomeitemresultSetPrototype->setArrayObjectPrototype( new Memberincomeitem() );
            			$IncomeitemtableGateway                 = new TableGateway('member_income_items',$adapter, null, $IncomeitemresultSetPrototype);

            			$ExpenseitemresultSetPrototype          = new ResultSet();
            			$ExpenseitemresultSetPrototype->setArrayObjectPrototype( new Memberexpenseitem() );
            			$ExpenseitemtableGateway                = new TableGateway('member_expense_items',$adapter, null, $ExpenseitemresultSetPrototype);

            			$BuyingitemresultSetPrototype           = new ResultSet();
            			$BuyingitemresultSetPrototype->setArrayObjectPrototype( new Memberbuyingcostitem() );
            			$BuyingitemtableGateway                 = new TableGateway('member_buyingcost_items',$adapter,null, $BuyingitemresultSetPrototype);

            			$SellingitemresultSetPrototype          = new ResultSet();
            			$SellingitemresultSetPrototype->setArrayObjectPrototype( new Membersellingcostitem() );
            			$SellingitemtableGateway                = new TableGateway('member_sellingcost_items',$adapter,null, $SellingitemresultSetPrototype);
            			
            			$DefaultbuyingcostitemresultSetPrototype= new ResultSet();
            			$DefaultbuyingcostitemresultSetPrototype->setArrayObjectPrototype( new Memberdefaultbuyingcostitem() );
            			$DefaultbuyingcostitemtableGateway      = new TableGateway('member_default_buyingcost_items', $adapter,null, $DefaultbuyingcostitemresultSetPrototype);
            			
            			$DefaultsellingcostitemresultSetPrototype= new ResultSet();
            			$DefaultsellingcostitemresultSetPrototype->setArrayObjectPrototype( new Memberdefaultsellingcostitem() );
            			$DefaultsellingcostitemtableGateway      = new TableGateway('member_default_sellingcost_items', $adapter,null, $DefaultsellingcostitemresultSetPrototype);

            			$table = new MemberTable($tableGateway,
            									 $PropertytableGateway,
            									 $IncomeitemtableGateway,
            									 $ExpenseitemtableGateway,
            									 $BuyingitemtableGateway,
            									 $SellingitemtableGateway,
            									 $DefaultbuyingcostitemtableGateway,
            									 $DefaultsellingcostitemtableGateway,
            									 $adapter);


            			return $table;
            		},
            		'Index\Model\Member_default_buyingcost_itemTable' => function($sm){
            			
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );
            			
            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Memberdefaultbuyingcostitem() );

            			$tableGateway = new TableGateway('member_default_buyingcost_items', $adapter, null, $resultSetPrototype);
            			$table = new MemberdefaultbuyingcostitemTable($tableGateway);
            			return $table;
            			            			
            		},
            		'Index\Model\Member_default_sellingcost_itemTable' => function($sm){
            			 
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );
            			 
            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Memberdefaultsellingcostitem() );
            		
            			$tableGateway = new TableGateway('member_default_sellingcost_items', $adapter, null, $resultSetPrototype);
            			$table = new MemberdefaultsellingcostitemTable($tableGateway);
            			return $table;
            		
            		},            		
            		'Index\Model\PropertyTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$PropertyresultSetPrototype 			= new ResultSet();
            			$PropertyresultSetPrototype->setArrayObjectPrototype( new Property() );
            			$PropertytableGateway 					= new TableGateway('properties', $adapter, null, $PropertyresultSetPrototype);

            			$UnitresultSetPrototype                 = new ResultSet();
            			$UnitresultSetPrototype->setArrayObjectPrototype( new Unit() );
            			$UnittableGateway 						= new TableGateway('units', $adapter, null, $UnitresultSetPrototype);

            			$UnitrentresultSetPrototype             = new ResultSet();
            			$UnitrentresultSetPrototype->setArrayObjectPrototype( new Unitrent() );
            			$UnitrenttableGateway 					= new TableGateway('unit_rents', $adapter, null, $UnitrentresultSetPrototype );

            			$Fullpage_photoresultSetPrototype       = new ResultSet();
            			$Fullpage_photoresultSetPrototype->setArrayObjectPrototype( new Fullpagephoto() );
            			$FullpagephototableGateway 				= new TableGateway('fullpage_photos', $adapter, null , $Fullpage_photoresultSetPrototype );

            			$PropertyvideoresultSetPrototype 		= new ResultSet();
            			$PropertyvideoresultSetPrototype ->setArrayObjectPrototype( new Propertyvideo() );
            			$PropertyvideotableGateway              = new TableGateway('property_videos',$adapter, null , $PropertyvideoresultSetPrototype );


            			$table = new PropertyTable($PropertytableGateway,$UnittableGateway,$UnitrenttableGateway,$FullpagephototableGateway,$PropertyvideotableGateway,$adapter);
            			return $table;
            		},

            		'Index\Model\Member_income_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Memberincomeitem());

            			$tableGateway = new TableGateway('member_income_items', $adapter, null, $resultSetPrototype);
            			$table = new MemberincomeitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\Property_member_income_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Propertymemberincomeitem());

            			$tableGateway = new TableGateway('property_member_income_items', $adapter, null, $resultSetPrototype);
            			$table = new PropertymemberincomeitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\Member_expense_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Memberexpenseitem());

            			$tableGateway = new TableGateway('member_expense_items', $adapter, null, $resultSetPrototype);
            			$table = new MemberexpenseitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\Property_member_expense_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Propertymemberexpenseitem());

            			$tableGateway = new TableGateway('property_member_expense_items', $adapter, null, $resultSetPrototype);
            			$table = new PropertymemberexpenseitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\Member_buyingcost_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Memberbuyingcostitem());

            			$tableGateway = new TableGateway('member_buyingcost_items', $adapter, null, $resultSetPrototype);
            			$table = new MemberbuyingcostitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\Property_buyingcost_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Propertybuyingcostitem());

            			$tableGateway = new TableGateway('property_buyingcost_items', $adapter, null, $resultSetPrototype);
            			$table = new PropertybuyingcostitemTable($tableGateway);
            			return $table;
            		},
               		'Index\Model\Member_sellingcost_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Membersellingcostitem());

            			$tableGateway = new TableGateway('member_sellingcost_items', $adapter, null, $resultSetPrototype);
            			$table = new MembersellingcostitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\Property_sellingcost_itemTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Propertysellingcostitem());

            			$tableGateway = new TableGateway('property_sellingcost_items', $adapter, null, $resultSetPrototype);
            			$table = new PropertysellingcostitemTable($tableGateway);
            			return $table;
            		},
            		'Index\Model\UnitTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Unit());
            			$UnittableGateway = new TableGateway('units', $adapter, null, $resultSetPrototype);

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype(new Unitrent());
            			$UnitrenttableGateway = new TableGateway('unit_rents', $adapter, null, $resultSetPrototype);


            			$table = new UnitTable($UnittableGateway,$UnitrenttableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\UnitrentTable' =>  function($sm) {
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config'] );

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Unitrent() );

            			$tableGateway = new TableGateway('unit_rents', $adapter, null, $resultSetPrototype);
            			$table = new UnitrentTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\FullpagephotoTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Fullpagephoto() );

            			$tableGateway = new TableGateway('fullpage_photos', $adapter, null, $resultSetPrototype);
            			$table = new FullpagephotoTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\PropertyvideoTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Propertyvideo() );

            			$tableGateway = new TableGateway('property_videos', $adapter, null, $resultSetPrototype);
            			$table = new PropertyvideoTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\FeedbackTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);

            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Feedback() );

            			$tableGateway = new TableGateway('feedbacks', $adapter, null, $resultSetPrototype);
            			$table = new FeedbackTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\Membership_typeTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);
            			
            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Membershiptype() );
            			
            			$tableGateway = new TableGateway('membership_types', $adapter,null,$resultSetPrototype);
            			$table = new MembershiptypeTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\Member_subscriptionTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);
            			
            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Membersubscription() );
            			
            			$tableGateway = new TableGateway('member_subscriptions', $adapter,null,$resultSetPrototype);
            			$table = new MembersubscriptionTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\Member_monthly_tokenTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);
            			
            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Membermonthlytoken() );
            			
            			$tableGateway = new TableGateway('member_monthly_tokens', $adapter,null,$resultSetPrototype);
            			$table = new MembermonthlytokenTable($tableGateway,$adapter);
            			return $table;
            		},
            		'Index\Model\SurwayTable' => function($sm){
            			// get adapter
            			$config = $sm->get('config');
            			$adapter = new Adapter( $config['index_db_config']);
            		
            			$resultSetPrototype = new ResultSet();
            			$resultSetPrototype->setArrayObjectPrototype( new Surway() );
            			
            			 
            			
            			$tableGateway = new TableGateway('surway', $adapter,null,$resultSetPrototype);
            			$table = new SurwayTable($tableGateway,$adapter);
            			return $table;
            			
            		},


            )
        );
	}

}
