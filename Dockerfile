FROM octavianparalescu/docker-zf2
RUN apt-get update
RUN apt-get -qqy install vim
RUN apt-get -qqy install tree 
RUN apt-get -qqy install git
WORKDIR /zf2-app
COPY . .
RUN php composer.phar self-update
RUN php composer.phar update
RUN php composer.phar install
COPY zf2-app.conf /etc/apache2/sites-available/zf2-app.conf
COPY zf2-app.conf /etc/apache2/sites-enabled/zf2-app.conf

CMD ["/etc/init.d/apache2","reload"]
RUN mkdir -p public/img/members/member_id_1
RUN chmod 777 public/img/members/member_id_1
RUN mkdir -p module/Index/libraries/MPDF/temp
RUN chmod 777 module/Index/libraries/MPDF/temp
RUN chmod 777 public/pdf
RUN echo 'output_buffering=On' >> /usr/local/etc/php/php.ini



