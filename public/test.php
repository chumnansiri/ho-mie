<?php


$servername = "mysql";
$username = "ho-mie.com";
$password = "ho-mie.com";

try {
  $conn = new PDO("mysql:host=$servername;dbname=zf2", $username, $password);
  // set the PDO error mode to exception
  $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  echo "Connected successfully"; 
} catch(PDOException $e) {
  echo "Connection failed: " . $e->getMessage();
}